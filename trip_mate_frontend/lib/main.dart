import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:trip_mate_frontend/controllers/bloc/admin/admin_bloc.dart';
import 'package:trip_mate_frontend/controllers/bloc/popular_places/popular_places_bloc.dart';
import 'package:trip_mate_frontend/controllers/bloc/recommendation/recommendation_bloc.dart';
import 'package:trip_mate_frontend/screens/splash_screen.dart';
import 'controllers/bloc/auth/auth_bloc.dart';
import 'controllers/bloc/destination/destinations_bloc.dart';
import 'controllers/bloc/user/user_bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => AuthBloc()),
        BlocProvider(create: (context) => UserBloc()),
        BlocProvider(create: (context) => DestinationsBloc()),
        BlocProvider(create: (context) => AdminBloc()),
        BlocProvider(create: (context) => RecommendationBloc()),
        BlocProvider(create: (context) => PopularPlacesBloc()),
      ],
      child: GetMaterialApp(
          title: 'tripMate',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
            scaffoldBackgroundColor: Colors.white,
            fontFamily: 'Poppins',
          ),
          home: const SplashScreen()),
    );
  }
}
