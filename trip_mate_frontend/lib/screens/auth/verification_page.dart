// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trip_mate_frontend/controllers/bloc/auth/auth_bloc.dart';

import '../../Utils/constants.dart';
import '../../components/custom_button.dart';
import '../../components/verification_box.dart';

// ignore: must_be_immutable
class VerificationPage extends StatefulWidget {
  String? userId;
  String? email;
  bool? isFromRegister;
  VerificationPage(
      {super.key, this.userId, this.email, this.isFromRegister = false});

  @override
  State<VerificationPage> createState() => _VerificationPageState();
}

class _VerificationPageState extends State<VerificationPage> {
  TextEditingController code1_controller = TextEditingController();
  TextEditingController code2_controller = TextEditingController();
  TextEditingController code3_controller = TextEditingController();
  TextEditingController code4_controller = TextEditingController();
  bool isCheckBoxOn = false;

  @override
  void initState() {
    BlocProvider.of<AuthBloc>(context)
        .add(SendOtpEvent(email: widget.email, userId: widget.userId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        surfaceTintColor: Colors.grey,
        bottom: const PreferredSize(
          preferredSize: Size.fromHeight(1.0),
          child: Divider(
            color: Colors.grey,
            thickness: 0.6,
            height: 1,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 0, 20, 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: const BoxDecoration(),
                      child: const Image(
                        image: AssetImage(
                          'assets/images/logo.png',
                        ),
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ),
                  const SizedBox(height: 5),
                  Center(
                    child: Column(
                      children: [
                        const Text(
                          'VERIFY EMAIL',
                          style: TextStyle(
                            color: kDarkColor,
                            fontSize: 35,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Text(
                          '''Enter the Verification code sent to you at:
               ${widget.email}''',
                          style: const TextStyle(
                            color: kDarkColor,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                  VerificationCodeBox(
                    code1_controller: code1_controller,
                    code2_controller: code2_controller,
                    code3_controller: code3_controller,
                    code4_controller: code4_controller,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Didn’t receive OTP?',
                        style: TextStyle(
                          color: kDarkColor,
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      TextButton(
                        onPressed: () async {
                          BlocProvider.of<AuthBloc>(context).add(SendOtpEvent(
                              email: widget.email, userId: widget.userId));
                        },
                        child: const Text('RESEND OTP',
                            style: TextStyle(
                              color: Colors.blue,
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                            )),
                      )
                    ],
                  ),
                  const SizedBox(height: 20),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: CustomButton(
                name: 'SUBMIT',
                handleClicked: () async {
                  BlocProvider.of<AuthBloc>(context).add(VerifyOtpEvent(
                    otp:
                        '${code1_controller.text}${code2_controller.text}${code3_controller.text}${code4_controller.text}',
                    userId: widget.userId,
                    isFromRegister: widget.isFromRegister!,
                  ));
                  // ignore: avoid_print
                  print(code1_controller.text);
                  code1_controller.clear();
                  code2_controller.clear();
                  code3_controller.clear();
                  code4_controller.clear();
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
