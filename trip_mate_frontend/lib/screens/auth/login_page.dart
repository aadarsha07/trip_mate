import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:trip_mate_frontend/screens/auth/register_page.dart';
import '../../components/text_field.dart';
import '../../controllers/bloc/auth/auth_bloc.dart';
import '../../models/user.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final user = User();
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  String name = "";
  bool obscure = true;
  bool changeButton = false;
  ScrollController _scrollController = ScrollController();
  final _formkey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  RegExp emailRegX = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
  String? emailValidation(String? value) {
    if (value == null || value.isEmpty || value.trim() == '') {
      return 'Email is required';
    } else if (!emailRegX.hasMatch(value)) {
      return 'Invalid Email';
    }
    return null;
  }

  String? passwordValidation(String? value) {
    if (value == null || value.isEmpty || value.trim() == '') {
      return 'Password is required';
    }
    //  else if (!passwordRegX.hasMatch(value)) {
    //   return 'Invalid Password';
    // }
    return null;
  }

  @override
  dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          controller: _scrollController,
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          child: SafeArea(
            child: Form(
              key: _formkey,
              child: Column(
                children: [
                  Image.asset(
                    "assets/images/login.png",
                    fit: BoxFit.cover,
                    height: 300,
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    "Welcome $name",
                    style: const TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 16.0, horizontal: 32.0),
                    child: Column(
                      children: [
                        TextFieldComponent(
                          handleTap: () {
                            const double scrollToPosition =
                                60.0 * (5 - 1); // Adjust this value as needed
                            _scrollController.animateTo(
                              scrollToPosition,
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.easeInOut,
                            );
                          },
                          controller: _emailController,
                          fieldName: 'Email',
                          handleValidation: emailValidation,
                          hintText: "Enter email",
                        ),
                        TextFieldComponent(
                          handleTap: () {
                            const double scrollToPosition =
                                60.0 * (5 - 1); // Adjust this value as needed
                            _scrollController.animateTo(
                              scrollToPosition,
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.easeInOut,
                            );
                          },
                          controller: _passwordController,
                          fieldName: "Password",
                          obscure: obscure,
                          maxLines: obscure ? 1 : null,
                          hintText: "Enter Password",
                          handleValidation: passwordValidation,
                          prefixIcon: const Icon(Icons.lock_open_outlined),
                          suffixIcon: IconButton(
                            icon: obscure
                                ? const Icon(Icons.visibility_off)
                                : const Icon(Icons.visibility),
                            onPressed: () {
                              setState(() {
                                obscure = !obscure;
                              });
                            },
                            color: Colors.black,
                          ),
                        ),
                        const SizedBox(
                          height: 20.0,
                        ),
                        InkWell(
                          onTap: () async {
                            setState(() {
                              changeButton = true;
                            });
                            if (_formkey.currentState!.validate()) {
                              BlocProvider.of<AuthBloc>(context).add(
                                  LoginSubmitEvent(
                                      email: _emailController.text,
                                      password: _passwordController.text));
                            }
                            Future.delayed(const Duration(seconds: 2), () {
                              setState(() {
                                changeButton = false;
                              });
                            });
                          },
                          child: AnimatedContainer(
                            duration: const Duration(seconds: 1),
                            width: changeButton ? 50 : 150,
                            height: 50,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Colors.deepPurple,
                                // shape: changeButton
                                //     ? BoxShape.circle
                                //     : BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(
                                    changeButton ? 50 : 8)),
                            child: changeButton
                                ? const Icon(
                                    Icons.done,
                                    color: Colors.white,
                                  )
                                : const Text(
                                    "Login",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("Don't have an account ?"),
                      TextButton(
                          onPressed: (() {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: ((context) {
                              return const RegisterPage();
                            })));
                          }),
                          child: const Text("Register"))
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
