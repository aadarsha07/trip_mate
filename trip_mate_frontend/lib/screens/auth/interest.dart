// ignore_for_file: use_key_in_widget_constructors, library_private_types_in_public_api

import 'package:flutter/material.dart';

import '../../models/interest.dart';

class InterestSelectionScreen extends StatefulWidget {
  @override
  _InterestSelectionScreenState createState() =>
      _InterestSelectionScreenState();
}

class _InterestSelectionScreenState extends State<InterestSelectionScreen> {
  List<Interest> interests = [
    Interest(name: 'Music', icon: Icons.music_note),
    Interest(name: 'Sports', icon: Icons.sports),
    Interest(name: 'Art', icon: Icons.palette),
    Interest(name: 'Reading', icon: Icons.book),
    Interest(name: 'Travel', icon: Icons.flight),
    Interest(name: 'Cooking', icon: Icons.restaurant),
    Interest(name: 'Photography', icon: Icons.camera),
    Interest(name: 'Gaming', icon: Icons.videogame_asset),
  ];

  List<Interest> selectedInterests = [];

  void toggleInterest(Interest interest) {
    setState(() {
      if (selectedInterests.contains(interest)) {
        selectedInterests.remove(interest);
      } else {
        if (selectedInterests.length < 5) {
          selectedInterests.add(interest);
        }
      }
    });
  }

  Widget buildInterestItem(Interest interest) {
    final isSelected = selectedInterests.contains(interest);

    return GestureDetector(
      onTap: () => toggleInterest(interest),
      child: Stack(
        children: [
          Container(
            width: 100,
            height: 100,
            margin: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              color: isSelected ? Colors.blue : Colors.transparent,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Colors.blue),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(interest.icon, size: 30),
                const SizedBox(height: 10),
                Text(
                  interest.name,
                  style: TextStyle(
                    fontWeight:
                        isSelected ? FontWeight.bold : FontWeight.normal,
                  ),
                ),
              ],
            ),
          ),
          if (isSelected)
            Positioned.fill(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.blue.withOpacity(0.3),
                ),
              ),
            ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Interest Selection'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Wrap(
          spacing: 16,
          runSpacing: 16,
          children:
              interests.map((interest) => buildInterestItem(interest)).toList(),
        ),
      ),
    );
  }
}
