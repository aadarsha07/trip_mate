import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:trip_mate_frontend/components/custom_button.dart';
import 'package:trip_mate_frontend/components/custom_dropdown.dart';
import 'package:trip_mate_frontend/controllers/bloc/auth/auth_bloc.dart';
import 'package:trip_mate_frontend/controllers/repositories/register_repository.dart';
import '../../components/text_field.dart';
import '../../utils/constants.dart';
import 'login_page.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formkey = GlobalKey<FormState>();
  // ignore: unused_field, prefer_final_fields
  String? hint;
  var count = 1;

  XFile? pickedImage;
  final ImagePicker _picker = ImagePicker();

  DateTime birthDate = DateTime.now().toLocal();
  final TextEditingController dateController = TextEditingController();
  bool obscure = true;
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _heightController = TextEditingController();
  final TextEditingController _weightController = TextEditingController();
  final pageController = PageController();
  final ScrollController _scrollController = ScrollController();

  RegExp emailRegX = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
  RegExp passwordRegX =
      RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{6,}$');
  // handlers
  String? firstNameValidation(String? value) {
    if (value == null || value.isEmpty || value.trim() == '') {
      return 'First Name is required';
    }
    return null;
  }

  String? lastNameValidation(String? value) {
    if (value == null || value.isEmpty || value.trim() == '') {
      return 'Last Name is required';
    }
    return null;
  }

  String? emailValidation(String? value) {
    if (value == null || value.isEmpty || value.trim() == '') {
      return 'Email is required';
    } else if (!emailRegX.hasMatch(value)) {
      return 'Invalid Email';
    }
    return null;
  }

  String? passwordValidation(String? value) {
    if (value == null || value.isEmpty || value.trim() == '') {
      return 'Password is required';
    } else if (!passwordRegX.hasMatch(value)) {
      return 'Invalid Password';
    }
    return null;
  }

  String? genderValidation(String? value) {
    if (value == null || value.isEmpty || value.trim() == '') {
      return 'Gender is required';
    }
    return null;
  }

  String? cityValidation(String? value) {
    if (value == null || value.isEmpty || value.trim() == '') {
      return 'City Name is required';
    }
    return null;
  }

  String? heightValidation(double? value) {
    if (value == null || value.isNaN || value <= 0) {
      return 'Height is required';
    }
    return null;
  }

  String? weightValidation(double? value) {
    if (value == null || value.isNaN || value <= 0) {
      return 'Weight is required';
    }
    return null;
  }

  void _selectDate() async {
    final DateTime? selectedDate = await showDatePicker(
        initialEntryMode: DatePickerEntryMode.calendar,
        initialDatePickerMode: DatePickerMode.year,
        context: context,
        initialDate: birthDate,
        firstDate: DateTime(1900),
        lastDate: DateTime.now().toLocal());
    if (selectedDate != null) {
      setState(() {
        birthDate = selectedDate;
        dateController.text = dateFormatter.format(birthDate);
      });
    }
  }

  @override
  void initState() {
    super.initState();
    dateController.text = dateFormatter.format(birthDate);
  }

  final List<String> gender = ['Male', 'Female', 'Others'];
  List<String> states = [
    'Bagmati',
    'Gandaki',
    'Karnali',
    'Lumbini',
    'Province No. 1',
    'Province No. 2'
  ];
  final List<String> qualification = [
    'Bachelors',
    'Masters',
    'Doctorate',
    'High school',
    'Diploma',
    'Undergraduate',
    'PhD',
    'Postdoctoral',
  ];

  final List<String> maritalStatus = [
    'Single',
    'Married',
    'Divorced',
    'Widowed',
    'Separated',
  ];

  late String selectedMaritalStatus = maritalStatus[0];
  late String selectedGender = gender[0];
  late String selectedState = states[0];
  late String selectedQualification = qualification[0];
  List<String> selectedInterests = [];

  void toggleInterest(String interest) {
    setState(() {
      if (selectedInterests.contains(interest)) {
        selectedInterests.remove(interest);
      } else {
        selectedInterests.add(interest);
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          // toolbarHeight: 130,
          backgroundColor: Colors.white,
          foregroundColor: Colors.grey,
          elevation: 0,
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(left: 22, right: 22),
            child: Form(
              key: _formkey,
              child: PageView(
                physics: const NeverScrollableScrollPhysics(),
                controller: pageController,
                children: [
                  SingleChildScrollView(
                    controller: _scrollController,
                    keyboardDismissBehavior:
                        ScrollViewKeyboardDismissBehavior.onDrag,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          "Sign Up",
                          style: TextStyle(
                              fontSize: 40, fontWeight: FontWeight.bold),
                        ),
                        TextFieldComponent(
                          handleTap: () {
                            final double scrollToPosition =
                                60.0 * (1 - 1); // Adjust this value as needed
                            _scrollController.animateTo(
                              scrollToPosition,
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.easeInOut,
                            );
                          },
                          controller: _firstNameController,
                          fieldName: "First Name",
                          handleValidation: firstNameValidation,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Can't be empty";
                            }
                            return null;
                          },
                          hintText: "Enter First Name",
                        ),
                        TextFieldComponent(
                          handleTap: () {
                            final double scrollToPosition =
                                60.0 * (3 - 1); // Adjust this value as needed
                            _scrollController.animateTo(
                              scrollToPosition,
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.easeInOut,
                            );
                          },
                          controller: _lastNameController,
                          handleValidation: lastNameValidation,
                          fieldName: "Last Name",
                          hintText: "Enter Last Name",
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Can't be empty";
                            }
                            return null;
                          },
                        ),
                        TextFieldComponent(
                          handleTap: () {
                            final double scrollToPosition =
                                60.0 * (4 - 1); // Adjust this value as needed
                            _scrollController.animateTo(
                              scrollToPosition,
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.easeInOut,
                            );
                          },
                          controller: _emailController,
                          fieldName: "Email",
                          hintText: "Enter Email",
                          handleValidation: emailValidation,
                        ),
                        TextFieldComponent(
                          handleTap: () {
                            final double scrollToPosition =
                                60.0 * (5 - 1); // Adjust this value as needed
                            _scrollController.animateTo(
                              scrollToPosition,
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.easeInOut,
                            );
                          },
                          controller: _passwordController,
                          handleValidation: passwordValidation,
                          obscure: obscure,
                          maxLines: obscure ? 1 : null,
                          fieldName: "Password",
                          hintText: "Enter Password",
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "can't be empty";
                            }
                            if (value.length < 8) {
                              return "Must be greater than 8 char";
                            }
                            if (!passwordRegX.hasMatch(value)) {
                              return 'Invalid Password';
                            }
                            return null;
                          },
                          prefixIcon: const Icon(Icons.lock_open_outlined),
                          suffixIcon: IconButton(
                            icon: obscure
                                ? const Icon(Icons.visibility_off)
                                : const Icon(Icons.visibility),
                            onPressed: () {
                              setState(() {
                                obscure = !obscure;
                              });
                            },
                            color: Colors.black,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        CustomDropDown(
                          helperText: 'Gender',
                          optionList: gender,
                          selectedOption: selectedGender,
                          onChanged: (p) => setState(() {
                            selectedGender = p!;
                          }),
                        ),
                        TextFieldComponent(
                          fieldName: "Date of Birth",
                          readOnly: true,
                          controller: dateController,
                          handleTap: _selectDate,
                          hintText: DateTime.now().toString().split(' ')[0],
                        ),
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    controller: _scrollController,
                    child: Column(
                      children: [
                        CustomDropDown(
                          helperText: 'Select State',
                          optionList: states,
                          selectedOption: selectedState,
                          onChanged: (p) => setState(() {
                            selectedState = p!;
                          }),
                        ),
                        TextFieldComponent(
                          controller: _cityController,
                          fieldName: "City",
                          hintText: "Enter City",
                          handleValidation: cityValidation,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        CustomDropDown(
                          optionList: qualification,
                          selectedOption: selectedQualification,
                          helperText: 'Qualification',
                          onChanged: (p) => setState(
                            () {
                              selectedQualification = p!;
                            },
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        CustomDropDown(
                          optionList: maritalStatus,
                          selectedOption: selectedMaritalStatus,
                          helperText: 'Marital Status',
                          onChanged: (p) => setState(
                            () {
                              selectedMaritalStatus = p!;
                            },
                          ),
                        ),
                        TextFieldComponent(
                          handleTap: () {
                            final double scrollToPosition =
                                60.0 * (7 - 1); // Adjust this value as needed
                            _scrollController.animateTo(
                              scrollToPosition,
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.easeInOut,
                            );
                          },
                          fieldName: 'Height (cm)',
                          keyboardType: TextInputType.number,
                          validator: (p0) {
                            if (p0!.isEmpty) {
                              return 'Please enter your height';
                            }
                            if (int.tryParse(p0) == null) {
                              return 'Please enter a valid height';
                            }
                            return null;
                          },
                          hintText: 'Your height in cm',
                          controller: _heightController,
                        ),
                        TextFieldComponent(
                          handleTap: () {
                            final double scrollToPosition =
                                60.0 * (8 - 1); // Adjust this value as needed
                            _scrollController.animateTo(
                              scrollToPosition,
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.easeInOut,
                            );
                          },
                          fieldName: 'Weight (Kg)',
                          keyboardType: TextInputType.number,
                          validator: (p0) {
                            if (p0!.isEmpty) {
                              return 'Please enter your weight';
                            }
                            if (int.tryParse(p0) == null) {
                              return 'Please enter a valid weight';
                            }
                            return null;
                          },
                          hintText: 'Your weight in kg',
                          controller: _weightController,
                        ),
                        const SizedBox(
                          height: 60,
                        )
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          ' Select Your Interests',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        const Text('Please select your interests below:'),
                        const SizedBox(height: 16),
                        GridView.count(
                          shrinkWrap: true,
                          crossAxisCount: 2,
                          childAspectRatio: 1.5,
                          children: [
                            buildInterestItem('Music', Icons.music_note),
                            buildInterestItem('Sports', Icons.sports),
                            buildInterestItem('Books', Icons.book),
                            buildInterestItem('Movies', Icons.movie),
                            buildInterestItem('Travel', Icons.flight),
                            buildInterestItem('Fitness', Icons.fitness_center),
                            buildInterestItem('Cooking', Icons.restaurant),
                            buildInterestItem('Photography', Icons.camera),
                            buildInterestItem('Art', Icons.palette),
                            buildInterestItem('Technology', Icons.computer),
                          ],
                        )
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        const Text(
                          "Upload Photo",
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Stack(
                              children: [
                                SizedBox(
                                  width: 200,
                                  height: 300,
                                  child: (pickedImage != null)
                                      ? Container(
                                          width: 150,
                                          height: 150,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image: FileImage(
                                                File(pickedImage!.path),
                                              ),
                                            ),
                                          ),
                                        )
                                      : Container(
                                          width: 150,
                                          height: 150,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image: AssetImage(
                                                  "assets/images/noimage.jpg"),
                                            ),
                                          ),
                                          child: const Center(
                                            child: Text("Choose an image"),
                                          ),
                                        ),
                                ),
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: GestureDetector(
                                    onTap: () async {
                                      pickedImage = await _picker.pickImage(
                                          source: ImageSource.camera);
                                      setState(() {});
                                    },
                                    child: Container(
                                      height: 36,
                                      width: 36,
                                      decoration: const BoxDecoration(
                                        color: Color(0xFF7D5EFF),
                                        shape: BoxShape.circle,
                                      ),
                                      child: Icon(
                                        Icons.add,
                                        size: 30,
                                        color: pickedImage != null
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 100.0),
                          child: Column(
                            children: [
                              Text.rich(
                                TextSpan(
                                  text: 'By signing up you agree to our ',
                                  style: const TextStyle(
                                    fontSize: 15,
                                    color: Colors.black,
                                  ),
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: 'Terms & Conditions',
                                      style: const TextStyle(
                                        fontSize: 15,
                                        color: Colors.blue,
                                      ),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          // code to open / launch terms of service link here
                                        },
                                    ),
                                    TextSpan(
                                      text: ' and ',
                                      style: const TextStyle(
                                          fontSize: 15, color: Colors.black),
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: 'Privacy Policy.*',
                                            style: const TextStyle(
                                              fontSize: 15,
                                              color: Colors.blue,
                                            ),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                // code to open / launch privacy policy link here
                                              })
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Center(
                                child: CustomButton(
                                    name: "Register",
                                    handleClicked: () async {
                                      if (pickedImage == null) {
                                        showDialog(
                                          context: context,
                                          builder: (context) => AlertDialog(
                                              title: const Text(
                                                'Please set Image',
                                              ),
                                              actions: [
                                                ElevatedButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    child: const Text('Ok'))
                                              ]),
                                        );
                                      } else if (_formkey.currentState!
                                          .validate()) {
                                        BlocProvider.of<AuthBloc>(context).add(
                                            RegisterSubmitEvent(
                                                email: _emailController.text,
                                                password:
                                                    _passwordController.text,
                                                firstName:
                                                    _firstNameController.text,
                                                lastName:
                                                    _lastNameController.text,
                                                role: 'user',
                                                gender: selectedGender,
                                                dateOfBirth:
                                                    dateController.text,
                                                pickedImage: pickedImage!,
                                                interests: selectedInterests,
                                                state: selectedState,
                                                city: _cityController.text,
                                                qualification:
                                                    selectedQualification,
                                                maritalStatus:
                                                    selectedMaritalStatus,
                                                height: _heightController.text,
                                                weight:
                                                    _weightController.text));
                                      } else {
                                        showDialog(
                                          context: context,
                                          builder: (context) => AlertDialog(
                                              title: const Text(
                                                'Some Fields are Empty',
                                              ),
                                              actions: [
                                                ElevatedButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    child: const Text('Ok'))
                                              ]),
                                        );
                                      }
                                    }),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                // crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  const Text(
                                    "Already Signed Up?",
                                  ),
                                  TextButton(
                                      onPressed: (() {
                                        Navigator.of(context).push(
                                          MaterialPageRoute(
                                            builder: ((context) {
                                              return const LoginPage();
                                            }),
                                          ),
                                        );
                                      }),
                                      child: const Text(
                                        "Log In",
                                        style: TextStyle(
                                            color: Color.fromARGB(
                                              255,
                                              15,
                                              140,
                                              243,
                                            ),
                                            fontWeight: FontWeight.bold),
                                      ))
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        bottomSheet: Padding(
          padding: const EdgeInsets.only(bottom: 15.0),
          child: SizedBox(
            height: 50,
            child: Padding(
              padding: const EdgeInsets.only(left: 15.0, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  count == 2 || count == 3 || count == 4
                      ? TextButton(
                          onPressed: () {
                            setState(() {
                              count = count - 1;
                            });
                            pageController.previousPage(
                                duration: const Duration(milliseconds: 500),
                                curve: Curves.easeInOut);
                          },
                          child: const Text(
                            'Back',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                        )
                      : const Text('  '),
                  SmoothPageIndicator(
                    controller: pageController,
                    count: 4,
                    effect: WormEffect(
                        spacing: 16,
                        dotColor: Colors.black26,
                        activeDotColor: Colors.teal.shade700),
                  ),
                  count == 1 || count == 2 || count == 3
                      ? TextButton(
                          onPressed: () {
                            setState(() {
                              count = count + 1;
                            });
                            pageController.nextPage(
                                duration: const Duration(milliseconds: 500),
                                curve: Curves.easeInOut);
                          },
                          child: const Text(
                            'NEXT',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                        )
                      : const Text('  '),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildInterestItem(String name, IconData icon) {
    final bool isSelected = selectedInterests.contains(name);
    return InkWell(
      onTap: () => toggleInterest(name),
      child: Container(
        decoration: BoxDecoration(
          color: isSelected ? Colors.blue.withOpacity(0.2) : null,
          border: Border.all(
            color: isSelected ? Colors.blue : Colors.grey,
            width: 2.0,
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: isSelected ? Colors.blue : Colors.grey,
              size: 32,
            ),
            const SizedBox(height: 8.0),
            Text(
              name,
              style: TextStyle(
                fontWeight: isSelected ? FontWeight.bold : FontWeight.normal,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
