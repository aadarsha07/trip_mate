// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:trip_mate_frontend/screens/global/explore_screen.dart';
import 'package:trip_mate_frontend/screens/global/home_screen.dart';
import 'package:trip_mate_frontend/utils/functions.dart';

import '../controllers/bloc/user/user_bloc.dart';
import '../utils/constants.dart';
import 'global/message_screen.dart';

class HomePage extends StatefulWidget {
  int? currentIndex;
  HomePage({super.key, this.currentIndex = 0});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final screens = [
    const HomeScreen(),
    const ExploreScreen(),
    const MessageScreen(),
  ];

  @override
  void initState() {
    super.initState();
    token = getToken();
    userId = getId();
    BlocProvider.of<UserBloc>(context).add(UserInitialEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // body: IndexedStack(
      //   index: widget.currentIndex,
      //   children: screens,
      // ),
      body: screens[widget.currentIndex!],
      bottomNavigationBar: BottomNavigationBar(
          onTap: (index) {
            widget.currentIndex = index;
            setState(() {});
          },
          iconSize: 35,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          currentIndex: widget.currentIndex!,
          selectedItemColor: const Color(0xff8f294f),
          unselectedItemColor: Colors.grey,
          items: const [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                ),
                label: "Home"),
            BottomNavigationBarItem(
                icon: Icon(Icons.travel_explore), label: "Explore"),
            BottomNavigationBarItem(
                icon: Icon(Icons.message), label: "Messages")
          ]),
    );
  }
}
