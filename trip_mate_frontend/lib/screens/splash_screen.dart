// ignore_for_file: use_build_context_synchronously, avoid_print
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:trip_mate_frontend/screens/admin/admin_home.dart';
import 'package:trip_mate_frontend/utils/functions.dart';
import 'auth/login_page.dart';
import 'home_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    checkSavedData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: const Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(image: AssetImage('assets/images/logo.png')),
              CupertinoActivityIndicator(),
              SizedBox(height: 20.0),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> checkSavedData() async {
    String? token = await getToken();

    String? role = await getRole();
    bool? isEmailVerified = await getEmail();

    await Future.delayed(const Duration(milliseconds: 2000), () {});

    if (token == null) {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => const LoginPage()));
    } else {
      if (isEmailVerified == false) {
        Get.offAll(() => const LoginPage());
      } else {
        if (role == 'admin') {
          Get.offAll(() => const AdminHome());
        } else if (role == 'user') {
          Get.offAll(() => HomePage());
        }
      }
    }
  }
}
