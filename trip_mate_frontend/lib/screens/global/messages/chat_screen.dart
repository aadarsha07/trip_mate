import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:trip_mate_frontend/utils/constants.dart';
import 'package:trip_mate_frontend/utils/functions.dart';

import '../../../components/message_bubble.dart';

import '../../../controllers/repositories/chat_controller.dart';
import '../../../models/chat.dart';
import '../../../models/message.dart';
import '../../../models/user.dart';

class ChatScreen extends StatefulWidget {
  final String? chatId;
  final User participant;
  final bool isNewChat;

  const ChatScreen({
    super.key,
    this.chatId,
    required this.participant,
    this.isNewChat = false,
  });

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  initState() {
    super.initState();
    getSavedData();
  }

  void getSavedData() async {
    token = await getToken();
    userId = await getId();
  }

  final ChatController chatController = ChatController();
  final TextEditingController messageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                Get.back();
                // BlocProvider.of<UserBloc>(context).add(UserInitialEvent());
              },
              child: const Icon(
                Icons.arrow_back_ios_new,
                color: Colors.black,
              ),
            ),
            const SizedBox(
              width: 30,
            ),
            // Image.network(widget.participant.profileImage![0]),
            widget.participant.profileImage != null
                ? CircleAvatar(
                    backgroundImage: NetworkImage(
                        '$baseURL/${widget.participant.profileImage}'),
                  )
                : const CircleAvatar(
                    backgroundImage: AssetImage('assets/images/noimage.jpg'),
                  ),
            const SizedBox(
              width: 10,
            ),
            Text(
              widget.participant.firstName!,
              style: const TextStyle(color: Colors.black),
            )
          ],
        ),
        centerTitle: false,
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: Padding(
        padding:
            const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 30),
        child: Column(
          children: [
            Builder(
              builder: (context) {
                if (widget.isNewChat) {
                  return const Flexible(
                    flex: 1,
                    fit: FlexFit.tight,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SizedBox(height: 20),
                      ],
                    ),
                  );
                } else {
                  return StreamBuilder(
                      stream: FirebaseFirestore.instance
                          .collection('chat')
                          .doc(widget.chatId)
                          .snapshots()
                          .map((event) {
                        return Chat.fromJson(event.data()!);
                      }),
                      builder: (context, snapshot) {
                        if (snapshot.hasError) {
                          return const Center(
                            child: Text('Something went wrong'),
                          );
                        }
                        if (snapshot.hasData) {
                          final messageList = snapshot.data!.messages;
                          return Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: ListView.builder(
                              physics: const BouncingScrollPhysics(),
                              itemCount: messageList.length,
                              itemBuilder: (context, index) {
                                final message = messageList[index];
                                if (messageList.isEmpty) {
                                  return const Center(
                                    child: Text('No Messages'),
                                  );
                                } else {
                                  return MessageBubble(
                                    isMe:
                                        messageList.elementAt(index).senderId !=
                                            userId,
                                    text: message.text,
                                  );
                                }
                              },
                            ),
                          );
                        } else {
                          return const CircularProgressIndicator();
                        }
                      });
                }
              },
            ),
            Container(
              decoration: const BoxDecoration(
                color: Colors.white,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Icon(Icons.add),
                  const SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: TextFormField(
                      controller: messageController,
                      maxLines: null,
                      onFieldSubmitted: (value) {
                        if (widget.isNewChat) {
                          chatController.startConversation(
                            widget.participant.id!,
                            Message(
                              senderId: userId,
                              text: value,
                              timestamp: DateTime.now(),
                            ),
                            context,
                            widget.participant,
                          );
                        } else {
                          chatController.sendMessage(
                            value,
                            widget.chatId!,
                            userId,
                          );
                          messageController.clear();
                        }
                      },
                      keyboardType: TextInputType.multiline,
                      decoration: const InputDecoration(
                        hintText: 'Type a message...',
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                  IconButton(
                    icon: const Icon(Icons.send),
                    onPressed: () {
                      if (widget.isNewChat) {
                        chatController.startConversation(
                          widget.participant.id!,
                          Message(
                            senderId: userId,
                            text: messageController.text,
                            timestamp: DateTime.now(),
                          ),
                          context,
                          widget.participant,
                        );
                        messageController.clear();
                      } else {
                        chatController.sendMessage(
                          messageController.text,
                          widget.chatId!,
                          userId,
                        );
                        messageController.clear();
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
