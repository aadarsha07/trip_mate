import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:trip_mate_frontend/components/destination_detail.dart';
import 'package:trip_mate_frontend/models/destinations.dart';
import 'package:trip_mate_frontend/screens/global/destinations/add_destination.dart';
import 'package:trip_mate_frontend/utils/constants.dart';
import '../../controllers/bloc/destination/destinations_bloc.dart';

class ExploreScreen extends StatefulWidget {
  const ExploreScreen({Key? key}) : super(key: key);

  @override
  State<ExploreScreen> createState() => _ExploreScreenState();
}

class _ExploreScreenState extends State<ExploreScreen> {
  TextEditingController searchController = TextEditingController();
  String searchText = '';
  DateTime? startDateFilter;
  DateTime? endDateFilter;
  List<Destination>? destinations;
  List<Destination>?
      filteredDestinations; // Separate list for filtered destinations
  bool filtersApplied = false; // Flag to track whether filters are applied

  Future<void> _showDestinationDetails(Destination destination) async {
    await showModalBottomSheet(
      useSafeArea: true,
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return SingleChildScrollView(
          child: DestinationDetails(destination: destination),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    BlocProvider.of<DestinationsBloc>(context).add(DestinationsInitialEvent());
  }

  void _showDateRangeFilterDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Filter by Date Range'),
          content: SizedBox(
            width: double.infinity,
            height: 100,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  children: [
                    const Text('Start Date:'),
                    DatePicker(
                      selectedDate: startDateFilter,
                      onChanged: (date) {
                        setState(() {
                          startDateFilter = date;
                        });
                      },
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                Row(
                  children: [
                    const Text('End Date:  '),
                    DatePicker(
                      selectedDate: endDateFilter,
                      onChanged: (date) {
                        setState(() {
                          endDateFilter = date;
                        });
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Clear Filters'),
              onPressed: () {
                setState(() {
                  startDateFilter = null;
                  endDateFilter = null;
                  filtersApplied = false; // Clear the filters
                });
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('Apply Filters'),
              onPressed: () {
                _applyFilters(); // Apply the filters when the button is pressed
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _applyFilters() {
    setState(() {
      filtersApplied = true; // Mark filters as applied
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // Hide the keyboard when the user taps outside the text field
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Explore",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
              IconButton(
                icon: const Icon(
                  Icons.filter_alt_outlined,
                  size: 40,
                  color: Colors.black,
                ),
                onPressed: () {
                  _showDateRangeFilterDialog(); // Show the date range filter dialog
                },
              )
            ],
          ),
        ),
        body: RefreshIndicator.adaptive(
          onRefresh: () async {},
          child: BlocBuilder<DestinationsBloc, DestinationsState>(
            builder: (context, state) {
              if (state is DestinationsLoading) {
                return const Center(
                    child: CircularProgressIndicator.adaptive());
              } else if (state is DestinationsLoaded) {
                if (state.destinations.isNotEmpty) {
                  destinations = state.destinations;
                  DateTime currentDate = DateTime.now();

                  // Filter destinations by start date
                  destinations!.removeWhere((destination) =>
                      destination.startDate!.isBefore(currentDate));

                  destinations!
                      .sort((a, b) => a.startDate!.compareTo(b.startDate!));

                  // Apply date range filter if selected and filters are applied
                  if (filtersApplied &&
                      (startDateFilter != null || endDateFilter != null)) {
                    filteredDestinations = destinations!
                        .where((destination) =>
                            (startDateFilter == null ||
                                destination.startDate!
                                    .isAfter(startDateFilter!)) &&
                            (endDateFilter == null ||
                                destination.startDate!
                                    .isBefore(endDateFilter!)))
                        .toList();
                  } else {
                    // If no filters applied, use the entire list
                    filteredDestinations = destinations;
                  }

                  return Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextField(
                          controller: searchController,
                          onChanged: (value) {
                            setState(() {
                              searchText = value;
                            });
                          },
                          onSubmitted: (value) {
                            setState(() {
                              searchText = value;
                            });
                          },
                          decoration: const InputDecoration(
                            hintText: 'Search destinations...',
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                            ),
                            contentPadding: EdgeInsets.all(10),
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: filteredDestinations!.length,
                          itemBuilder: (context, index) {
                            if (searchText.isNotEmpty &&
                                !filteredDestinations![index]
                                    .name!
                                    .toLowerCase()
                                    .contains(searchText.toLowerCase())) {
                              return Container(); // Return an empty container for non-matching items
                            }

                            return InkWell(
                              onTap: () async => await _showDestinationDetails(
                                  filteredDestinations![index]),
                              child: Container(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    filteredDestinations![index]
                                                .addedBy!
                                                .userProfileImage !=
                                            null
                                        ? ClipOval(
                                            child: Image.network(
                                              '$baseURL/${filteredDestinations![index].addedBy!.userProfileImage}',
                                              width: 100,
                                              height: 100,
                                              fit: BoxFit.cover,
                                            ),
                                          )
                                        : ClipOval(
                                            child: Image.asset(
                                              'assets/images/noimage.jpg',
                                              width: 100,
                                              height: 100,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                    const SizedBox(width: 10),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Text(
                                                filteredDestinations![index]
                                                    .addedBy!
                                                    .userName!,
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                style: const TextStyle(
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              filteredDestinations![index]
                                                      .addedBy!
                                                      .isVerified!
                                                  ? const Icon(
                                                      Icons.verified,
                                                      color: Colors.blue,
                                                      size: 14,
                                                    )
                                                  : const SizedBox(),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              const Icon(
                                                Icons.location_pin,
                                                size: 16,
                                                color: Colors.orange,
                                              ),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              Flexible(
                                                child: Text(
                                                  filteredDestinations![index]
                                                      .name!,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  style: const TextStyle(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          const SizedBox(height: 5),
                                          Row(
                                            children: [
                                              const Icon(
                                                Icons.description,
                                                size: 16,
                                                color: Colors.grey,
                                              ),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              Text(
                                                filteredDestinations![index]
                                                    .description!,
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(width: 10),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              DateFormat.d().format(
                                                  filteredDestinations![index]
                                                      .startDate!),
                                              style:
                                                  const TextStyle(fontSize: 16),
                                            ),
                                            Text(
                                              DateFormat.MMM().format(
                                                  filteredDestinations![index]
                                                      .startDate!),
                                              style:
                                                  const TextStyle(fontSize: 16),
                                            ),
                                          ],
                                        ),
                                        const SizedBox(width: 10),
                                        const SizedBox(
                                          height: 30,
                                          child: VerticalDivider(
                                            width: 1,
                                            color: Colors.black,
                                          ),
                                        ),
                                        const SizedBox(width: 10),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              DateFormat.d().format(
                                                  filteredDestinations![index]
                                                      .endDate!),
                                              style:
                                                  const TextStyle(fontSize: 16),
                                            ),
                                            Text(
                                              DateFormat.MMM().format(
                                                  filteredDestinations![index]
                                                      .endDate!),
                                              style:
                                                  const TextStyle(fontSize: 16),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  );
                } else {
                  return const Center(
                    child: Text('No destinations found'),
                  );
                }
              } else {
                return const CircularProgressIndicator.adaptive();
              }
            },
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => AddDestination()));
          },
          child: const Icon(
            Icons.add,
            size: 40,
          ),
        ),
      ),
    );
  }
}

class DatePicker extends StatelessWidget {
  final DateTime? selectedDate;
  final ValueChanged<DateTime?>? onChanged;

  DatePicker({Key? key, this.selectedDate, this.onChanged}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        final DateTime? picked = await showDatePicker(
          context: context,
          initialDate: selectedDate ?? DateTime.now(),
          firstDate: DateTime(2000),
          lastDate: DateTime(2101),
        );
        if (picked != null && picked != selectedDate) {
          onChanged?.call(picked);
        }
      },
      child: Row(
        children: <Widget>[
          const Icon(Icons.calendar_today),
          Text(
            selectedDate != null
                ? DateFormat('MM/dd/yyyy').format(selectedDate!)
                : '',
          ),
        ],
      ),
    );
  }
}
