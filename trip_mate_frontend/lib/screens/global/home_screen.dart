import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trip_mate_frontend/controllers/bloc/user/user_bloc.dart';
import 'package:trip_mate_frontend/screens/global/profile/profile_screen.dart';
import '../../components/places_to_go.dart';
import '../../components/recommended_places.dart';
import '../../controllers/bloc/destination/destinations_bloc.dart';
import '../../models/user.dart';
import '../../utils/constants.dart';
import 'notifications.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  initState() {
    super.initState();

    // BlocProvider.of<RecommendationBloc>(context)
    //     .add(RecommendationInitialEvent());

    BlocProvider.of<DestinationsBloc>(context).add(FetchPendingEvent());
  }

  User? user;
  String? id;

  @override
  Widget build(BuildContext context) {
    // return Consumer<UserProvider>(builder: (context, userProvider, child) {
    //   final notificationCountProvider =
    //       Provider.of<NotificationCountProvider>(context);

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ProfileScreen(
                          user: user!,
                        )));
              },
              child: BlocBuilder<UserBloc, UserState>(
                builder: (context, state) {
                  if (state is UserInitial) {
                    BlocProvider.of<UserBloc>(context).add(UserInitialEvent());
                    BlocProvider.of<DestinationsBloc>(context)
                        .add(FetchPendingEvent());
                  }
                  if (state is UserLoaded) {
                    id = state.user.id;
                    user = state.user;
                    if (state.user.profileImage != null) {
                      return ClipRRect(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(20),
                          ),
                          child: CachedNetworkImage(
                            imageUrl: '$baseURL/${user!.profileImage}',
                            width: 60,
                            height: 60,
                          ));
                    } else {
                      return ClipRRect(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(20),
                          ),
                          child: Image.asset(
                            'assets/images/noimage.jpg',
                            width: 60,
                            height: 60,
                          ));
                    }
                  }
                  return ClipRRect(
                      borderRadius: const BorderRadius.all(
                        Radius.circular(20),
                      ),
                      child: Image.asset(
                        'assets/images/noimage.jpg',
                        width: 60,
                        height: 60,
                      ));
                },
              ),
            ),
            BlocBuilder<DestinationsBloc, DestinationsState>(
              builder: (context, state) {
                int? count = 0;
                if (state is FetchPending) {
                  for (var i in state.destinations) {
                    count = i.members!.length;
                  }

                  return Stack(alignment: Alignment.topRight, children: [
                    IconButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => const NotificationPage(),
                          ),
                        );
                      },
                      icon: const Icon(
                        Icons.notifications,
                        color: Color.fromARGB(255, 230, 146, 21),
                        size: 40,
                      ),
                    ),
                    if (count != 0)
                      Container(
                        padding: const EdgeInsets.all(4),
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child: Text(
                          count.toString(),
                          style: const TextStyle(
                            color: Colors.red,
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                  ]);
                }
                return Stack(alignment: Alignment.topRight, children: [
                  IconButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const NotificationPage(),
                        ),
                      );
                    },
                    icon: const Icon(
                      Icons.notifications,
                      color: Color.fromARGB(255, 230, 146, 21),
                      size: 40,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(4),
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: const Text(
                      '',
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ]);
              },
            )
          ],
        ),
        centerTitle: false,
      ),
      body: ListView(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.all(14),
        children: [
          // LOCATION CARD
          RichText(
            text: TextSpan(
                text: 'Discover',
                style: const TextStyle(
                    color: Color(0xff8f294f), fontSize: 32, height: 1.3),
                children: <TextSpan>[
                  TextSpan(
                      text: ' the Best Places to Travel',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black.withOpacity(.8)))
                ]),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Recommendation",
                style: Theme.of(context).textTheme.titleLarge,
              ),
            ],
          ),

          // ElevatedButton(
          //     onPressed: () {
          //       BlocProvider.of<UserBloc>(context).add(UserLogoutEvent());
          //     },
          //     child: Text("Click me")),

          RecommendedPlaces(
            id: id,
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Places to go",
                style: Theme.of(context).textTheme.titleLarge,
              ),
            ],
          ),
          const SizedBox(height: 10),
          const NearbyPlaces(),
        ],
      ),
    );
  }
}
