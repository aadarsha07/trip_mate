import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trip_mate_frontend/components/shimmers/message_shimmer.dart';
import 'package:trip_mate_frontend/controllers/bloc/user/user_bloc.dart';
import 'package:trip_mate_frontend/screens/global/messages/chat_screen.dart';

import '../../models/chat.dart';
import '../../models/user.dart';

import 'package:http/http.dart' as http;

import '../../utils/constants.dart';
import '../../utils/functions.dart';

class MessageScreen extends StatefulWidget {
  const MessageScreen({super.key});

  @override
  State<MessageScreen> createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  Future<User> getParticipant(String id) async {
    String? token = await getToken();

    var headersList = {
      'Accept': '*/*',
      'User-Agent': 'Thunder Client (https://www.thunderclient.com)',
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json'
    };
    var url = Uri.parse('$baseURL/api/users/$id');

    var req = http.Request('GET', url);
    req.headers.addAll(headersList);

    var res = await req.send();
    final resBody = await res.stream.bytesToString();
    return User.fromJson(jsonDecode(resBody));
  }

  @override
  void initState() {
    super.initState();
    BlocProvider.of<UserBloc>(context).add(UserInitialEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        elevation: 0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {},
              child: const Text(
                "Messages",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
      body: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) {
          if (state is UserLoaded) {
            final user = state.user;

            return Container(
              margin: const EdgeInsets.all(8),
              decoration: const BoxDecoration(
                  // border: Border.all(
                  //   color: const Color.fromARGB(255, 223, 220, 220),
                  //   width: 1,
                  // ),
                  // boxShadow: const [
                  //   BoxShadow(
                  //     color: Color.fromARGB(255, 251, 250, 250),
                  //     spreadRadius: 5,
                  //     blurRadius: 5,
                  //   ),
                  // ],
                  ),
              child: StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection("chat")
                      .where("participantsId", arrayContains: user.id)
                      .snapshots()
                      .map(
                        (snapshot) => snapshot.docs
                            .map((e) => Chat.fromJson(e.data()))
                            .toList()
                          ..sort(
                              (a, b) => b.lastSentAt!.compareTo(a.lastSentAt!)),
                      ),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Scaffold(
                          body: SingleChildScrollView(
                              child:
                                  MessageShimmer())); // Center(child: CircularProgressIndicator(
                    }
                    if (snapshot.hasData) {
                      final chats = snapshot.data ?? [];
                      return RefreshIndicator.adaptive(
                        onRefresh: () async {
                          setState(() {});
                        },
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: chats.length,
                            itemBuilder: (context, index) {
                              final chat = chats[index];
                              // print(chat.toJson());
                              final participantId = chat.participantsId
                                  .where(
                                    (element) => element != user.id,
                                  )
                                  .first;
                              return Column(
                                children: [
                                  FutureBuilder<User>(
                                    future: getParticipant(participantId),
                                    builder: (context, snapshot) {
                                      if (snapshot.connectionState ==
                                          ConnectionState.waiting) {
                                        return const MessageShimmer();
                                      } else if (snapshot.hasData) {
                                        final participant = snapshot.data;
                                        print(participant!.toJson());
                                        return InkWell(
                                          onTap: () {
                                            Navigator.of(context).push(
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    ChatScreen(
                                                  chatId: chat.id,
                                                  participant: participant,
                                                ),
                                              ),
                                            );
                                          },
                                          child: ListTile(
                                            contentPadding:
                                                const EdgeInsets.only(
                                              top: 2,
                                              bottom: 2,
                                              left: 2,
                                              right: 5,
                                            ),
                                            leading: Container(
                                              width: 60,
                                              alignment: Alignment.centerLeft,
                                              child: user.profileImage !=
                                                          null &&
                                                      user.profileImage != ''
                                                  ? CircleAvatar(
                                                      backgroundImage: NetworkImage(
                                                          '$baseURL/${participant.profileImage}'),
                                                      radius: 40,
                                                    )
                                                  : const CircleAvatar(
                                                      backgroundImage: AssetImage(
                                                          'assets/images/noimage.jpg'),
                                                      radius: 40,
                                                    ),
                                            ),
                                            title: Text(
                                              '${participant.firstName} ${participant.lastName}',
                                              style: const TextStyle(
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                            subtitle: Text(
                                              chat.lastMessage ?? '',
                                              style: const TextStyle(
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                            trailing: Column(
                                              children: [
                                                Text(
                                                  '${chat.lastSentAt!.hour.toString()}:${chat.lastSentAt!.minute.toString()}',
                                                  style: TextStyle(
                                                    color: chat.isSeen!
                                                        ? Colors.blue
                                                        : Colors.black,
                                                  ),
                                                ),
                                                Container(
                                                  height: 10,
                                                  width: 10,
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: chat.isSeen!
                                                        ? Colors.blue
                                                        : Colors.white,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        );
                                      } else if (snapshot.hasError) {
                                        return Center(
                                          child:
                                              Text(snapshot.error.toString()),
                                        );
                                      } else {
                                        return const CircularProgressIndicator();
                                      }
                                    },
                                  ),
                                  const Divider(
                                    thickness: 1,
                                    color: Color.fromARGB(255, 218, 217, 217),
                                    indent: 20,
                                    endIndent: 20,
                                  ),
                                ],
                              );
                            }),
                      );
                    } else {
                      return const MessageShimmer();
                    }
                  }),
            );
          }
          return const MessageShimmer();
        },
      ),
    );
  }
}
