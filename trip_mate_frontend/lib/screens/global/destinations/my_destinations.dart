// ignore_for_file: unnecessary_import, must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/instance_manager.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';
import 'package:trip_mate_frontend/components/my_trips_details.dart';
import 'package:trip_mate_frontend/controllers/bloc/destination/destinations_bloc.dart';
import '../../../models/destinations.dart';
import '../../../utils/constants.dart';
import 'add_destination.dart';

class MyDestinations extends StatefulWidget {
  String userId;
  MyDestinations({super.key, required this.userId});

  @override
  State<MyDestinations> createState() => _MyDestinationsState();
}

class _MyDestinationsState extends State<MyDestinations> {
  Future<void> _showDestinationDetails(Destination destination) async {
    await showModalBottomSheet(
      useSafeArea: true,
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return SingleChildScrollView(
            child: MyTripsDetail(
          destination: destination,
        ));
      },
    );
  }

  @override
  initState() {
    super.initState();
    BlocProvider.of<DestinationsBloc>(context).add(ShowMyTripsEvent(
      userId: widget.userId,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            InkWell(
              onTap: () {
                Get.back();
              },
              child: const ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  child: Icon(
                    Icons.arrow_back_ios_new,
                    color: Colors.black,
                  )),
            ),
            const SizedBox(
              width: 130,
            ),
            const Text(
              "My trips",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 25),
            )
          ],
        ),
        centerTitle: false,
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: BlocBuilder<DestinationsBloc, DestinationsState>(
        builder: (context, state) {
          if (state is ShowMyTripsLoading) {
            return const Center(child: CircularProgressIndicator.adaptive());
          } else if (state is ShowMyTrips) {
            final destinations = state.destinations;
            //remove if destination.addedby not equal to current user
            // destinations.removeWhere(
            //     (element) => element.addedBy!.userId != widget.userId);
            return ListView.builder(
              shrinkWrap: true,
              itemCount: destinations.length,
              itemBuilder: (context, index) {
                final destination = destinations[index];

                return InkWell(
                  onTap: () async => await _showDestinationDetails(destination),
                  child: Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        destination.addedBy!.userProfileImage == null
                            ? ClipOval(
                                child: Image.asset(
                                  'assets/images/noimage.jpg',
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.cover,
                                ),
                              )
                            : ClipOval(
                                child: Image.network(
                                  '$baseURL/${destination.addedBy!.userProfileImage}',
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.cover,
                                ),
                              ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    destination.addedBy!.userName!,
                                    style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  destination.addedBy!.isVerified!
                                      ? const Icon(
                                          Icons.verified,
                                          color: Colors.blue,
                                          size: 14,
                                        )
                                      : const SizedBox(),
                                ],
                              ),
                              Text(
                                destination.name!,
                                style: const TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              const SizedBox(height: 5),
                              Text(
                                destination.description!,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 10),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  DateFormat.d().format(destination.startDate!),
                                  style: const TextStyle(fontSize: 16),
                                ),
                                Text(
                                  DateFormat.MMM()
                                      .format(destination.startDate!),
                                  style: const TextStyle(fontSize: 16),
                                ),
                              ],
                            ),
                            const SizedBox(width: 10),
                            const SizedBox(
                              height: 30,
                              child: VerticalDivider(
                                width: 1,
                                color: Colors.black,
                              ),
                            ),
                            const SizedBox(width: 10),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  DateFormat.d().format(destination.endDate!),
                                  style: const TextStyle(fontSize: 16),
                                ),
                                Text(
                                  DateFormat.MMM().format(destination.endDate!),
                                  style: const TextStyle(fontSize: 16),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          }
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => AddDestination()));
        },
        child: const Icon(
          Icons.add,
          size: 40,
        ),
      ),
    );
  }
}
