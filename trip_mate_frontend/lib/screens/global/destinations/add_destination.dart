import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:trip_mate_frontend/components/custom_button.dart';
import 'package:trip_mate_frontend/components/custom_dropdown.dart';
import 'package:trip_mate_frontend/components/text_field.dart';

import '../../../controllers/bloc/destination/destinations_bloc.dart';
import '../../../utils/constants.dart';

// ignore: must_be_immutable
class AddDestination extends StatefulWidget {
  String? name;
  AddDestination({Key? key, this.name}) : super(key: key);

  @override
  State<AddDestination> createState() => _AddDestinationState();
}

class _AddDestinationState extends State<AddDestination> {
  late final _formkey = GlobalKey<FormState>();
  DateTime startDate = DateTime.now().toLocal();
  DateTime endDate = DateTime.now().toLocal().add(const Duration(days: 1));
  // final ImagePicker _picker = ImagePicker();
  // XFile? pickedImage;
  TextEditingController destinationController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController purposeController = TextEditingController();
  TextEditingController startDateController = TextEditingController();
  TextEditingController endDateController = TextEditingController();

  List<String> gender = ['Male', 'Female', 'Others'];
  late String _selectedGender = gender[0];

  String? genderValidation(String? value) {
    if (value == null || value.isEmpty || value.trim() == '') {
      return 'Gender is required';
    }
    return null;
  }

  void _selectStartDate() async {
    final DateTime? selectedDate = await showDatePicker(
        context: context,
        initialDate: startDate,
        firstDate: DateTime.now(),
        lastDate: DateTime.now().add(const Duration(days: 365)));
    if (selectedDate != null) {
      setState(() {
        startDate = selectedDate;
        startDateController.text = dateFormatter.format(startDate);
        endDateController.text =
            dateFormatter.format(startDate.add(const Duration(days: 1)));
      });
    }
  }

  void _selectEndDate() async {
    final DateTime? selectedDate = await showDatePicker(
        context: context,
        initialDate: startDate.add(const Duration(days: 1)),
        firstDate: startDate.add(const Duration(days: 1)),
        lastDate: startDate.add(const Duration(days: 365)));
    if (selectedDate != null) {
      setState(() {
        endDate = selectedDate;
        endDateController.text = dateFormatter.format(endDate);
      });
    }
  }

  @override
  void initState() {
    super.initState();
    destinationController.text = widget.name ?? '';

    startDateController.text = dateFormatter.format(startDate);
    endDateController.text =
        dateFormatter.format(startDate.add(const Duration(days: 1)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: const ClipRRect(
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
                child: Icon(
                  Icons.arrow_back_ios_new,
                  color: Colors.black,
                ),
              ),
            ),
          ],
        ),
        centerTitle: false,
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _formkey,
            child: Column(children: [
              TextFieldComponent(
                handleValidation: (value) {
                  if (value == null || value.isEmpty || value.trim() == '') {
                    return 'Destination is required';
                  }
                  return null;
                },
                controller: destinationController,
                fieldName: 'Destination',
                hintText: 'Where you wanna go',
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Text(
                      "Who are you looking for",
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                    CustomDropDown(
                      optionList: gender,
                      selectedOption: _selectedGender,
                      onChanged: (value) {
                        setState(() {
                          _selectedGender = value!;
                        });
                      },
                    ),
                  ],
                ),
              ),
              TextFieldComponent(
                maxLines: 3,
                handleValidation: (value) {
                  if (value == null || value.isEmpty || value.trim() == '') {
                    return 'Description is required';
                  }
                  return null;
                },
                controller: descriptionController,
                fieldName: 'Description',
                hintText: '''Tell more about your trip,
          How you are planning to go, 
          What you will be doing there,
                            ''',
              ),
            
              TextFieldComponent(
                fieldName: "From",
                readOnly: true,
                controller: startDateController,
                handleTap: _selectStartDate,
                hintText: dateFormatter.format(startDate),
              ),
              TextFieldComponent(
                  fieldName: "To",
                  readOnly: true,
                  controller: endDateController,
                  handleTap: _selectEndDate,
                  hintText: dateFormatter
                      .format(startDate.add(const Duration(days: 1)))),
              CustomButton(
                name: 'Submit',
                handleClicked: () {
                  if (_formkey.currentState!.validate()) {
                    BlocProvider.of<DestinationsBloc>(context).add(
                      AddDestinationEvent(
                        name: destinationController.text,
                        description: descriptionController.text,
                        peopleLooking: _selectedGender,
                        startDate: startDateController.text,
                        endDate: endDateController.text,
                        // pickedImage: pickedImage!,
                      ),
                    );
                  }
                },
              )
            ]),
          ),
        ),
      ),
    );
  }
}
