import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:trip_mate_frontend/components/custom_button.dart';
import 'package:trip_mate_frontend/components/custom_dropdown.dart';
import 'package:trip_mate_frontend/components/text_field.dart';
import '../../../controllers/bloc/destination/destinations_bloc.dart';
import '../../../utils/constants.dart';
import 'package:trip_mate_frontend/models/destinations.dart';

// ignore: must_be_immutable
class EditDestinations extends StatefulWidget {
  Destination destination;

  EditDestinations({Key? key, required this.destination}) : super(key: key);

  @override
  State<EditDestinations> createState() => _EditDestinationsState();
}

class _EditDestinationsState extends State<EditDestinations> {
  late final _formkey = GlobalKey<FormState>();
  late DateTime startDate = widget.destination.startDate!;
  late DateTime endDate = widget.destination.endDate!;
  final ImagePicker _picker = ImagePicker();
  XFile? pickedImage;
  TextEditingController destinationController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController purposeController = TextEditingController();
  TextEditingController startDateController = TextEditingController();
  TextEditingController endDateController = TextEditingController();
  // late String _selectedGender = widget.destination.peopleLooking;

  String? genderValidation(String? value) {
    if (value == null || value.isEmpty || value.trim() == '') {
      return 'Gender is required';
    }
    return null;
  }

  chooseStartTime() async {
    final DateTime? pickDate = await showDatePicker(
      context: context,
      initialDate: startDate,
      firstDate: DateTime.now().subtract(const Duration(days: 365)),
      lastDate: DateTime.now().add(const Duration(days: 365)),
    );
    if (pickDate != null) {
      setState(() {
        startDate = DateTime(pickDate.year, pickDate.month, pickDate.day);
        startDateController.text = dateFormatter.format(startDate);
      });
    }
  }

  chooseEndTime() async {
    final DateTime? pickDate = await showDatePicker(
      context: context,
      initialDate: endDate,
      firstDate: DateTime.now().subtract(const Duration(days: 365)),
      lastDate: DateTime.now().add(const Duration(days: 365)),
    );
    if (pickDate != null) {
      setState(() {
        endDate = DateTime(pickDate.year, pickDate.month, pickDate.day);
        endDateController.text = dateFormatter.format(endDate);
      });
    }
  }

  List<String> genders = ['Male', 'Female', 'Others'];

  late String? selectedOption = genders[0];

  @override
  void initState() {
    super.initState();
    startDateController.text = dateFormatter.format(startDate);
    endDateController.text = dateFormatter.format(endDate);
    destinationController.text = widget.destination.name!;
    descriptionController.text = widget.destination.description!;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: const ClipRRect(
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
                child: Icon(
                  Icons.arrow_back_ios_new,
                  color: Colors.black,
                ),
              ),
            ),
          ],
        ),
        centerTitle: false,
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _formkey,
            child: Column(children: [
              TextFieldComponent(
                controller: destinationController,
                fieldName: 'Destination',
                hintText: 'Where you wanna go',
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const Text(
                      "Who are you looking for",
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                    CustomDropDown(
                      optionList: genders,
                      selectedOption: selectedOption,
                      onChanged: (p) => setState(
                        () {
                          selectedOption = p!;
                        },
                      ),
                    )
                  ],
                ),
              ),
              TextFieldComponent(
                maxLines: 3,
                controller: descriptionController,
                fieldName: 'Description',
                hintText: '''Tell more about your trip,
          How you are planning to go, 
          What you will be doing there,
                            ''',
              ),
              const Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Add image of the destination",
                    style: TextStyle(fontSize: 18),
                  )),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black,
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child: TextButton(
                      onPressed: () async {
                        pickedImage = await _picker.pickImage(
                            source: ImageSource.gallery);
                        setState(() {});
                      },
                      child: const Align(
                        alignment: Alignment.bottomLeft,
                        child: Text("Change Image"),
                      ),
                    ),
                  ),
                  (pickedImage == null)
                      ? Container(
                          height: 200,
                          width: 200,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black,
                              width: 1.0,
                            ),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: widget.destination.placeImage != null
                              ? Image(
                                  image: NetworkImage(
                                      '$baseURL/${widget.destination.placeImage}'))
                              : const Image(
                                  image:
                                      AssetImage('assets/images/noimage.jpg'),
                                ))
                      : Container(
                          height: 200,
                          width: 200,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black,
                              width: 1.0,
                            ),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child:
                              Image(image: FileImage(File(pickedImage!.path))),
                        ),
                ],
              ),
              TextFieldComponent(
                fieldName: "From",
                readOnly: true,
                controller: startDateController,
                handleTap: chooseStartTime,
                hintText: DateTime.now().toString().split(' ')[0],
              ),
              TextFieldComponent(
                fieldName: "To",
                readOnly: true,
                controller: endDateController,
                handleTap: chooseEndTime,
                hintText: DateTime.now().toString().split(' ')[0],
              ),
              CustomButton(
                  name: 'Submit',
                  handleClicked: () {
                    if (_formkey.currentState!.validate()) {
                      BlocProvider.of<DestinationsBloc>(context).add(
                        EditDestinationEvent(
                          destinationId: widget.destination.id!,
                          name: destinationController.text,
                          description: descriptionController.text,
                          peopleLooking: selectedOption!,
                          startDate: startDateController.text,
                          endDate: endDateController.text,
                          pickedImage: pickedImage,
                        ),
                      );
                    }
                  })
            ]),
          ),
        ),
      ),
    );
  }
}
