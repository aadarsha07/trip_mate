import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trip_mate_frontend/controllers/bloc/destination/destinations_bloc.dart';
import 'package:trip_mate_frontend/screens/global/profile/traveler_profile_page.dart';
import 'package:trip_mate_frontend/utils/functions.dart';
import '../../models/destinations.dart';
import '../../utils/constants.dart';
import 'package:http/http.dart' as http;

class NotificationPage extends StatefulWidget {
  const NotificationPage({
    Key? key,
  }) : super(key: key);

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<DestinationsBloc>(context).add(FetchPendingEvent());
  }

  @override
  Widget build(BuildContext context) {
    // final List<Destination> destinations = snapshot.data!;
    // destinations.sort((a, b) => a.startDate!.compareTo(b.startDate!));
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: const ClipRRect(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  child: Icon(
                    Icons.arrow_back_ios_new,
                    color: Colors.black,
                  )),
            ),
          ],
        ),
        centerTitle: false,
        backgroundColor: Colors.white,
        elevation: 0.0,
      ),
      body: BlocBuilder<DestinationsBloc, DestinationsState>(
        builder: (context, state) {
          if (state is FetchPendingLoading) {
            return const Center(child: CircularProgressIndicator());
          } else if (state is FetchPending) {
            final List<Destination> destinations = state.destinations;
            if (destinations.isEmpty || destinations.length == 0) {
              return const Center(
                child: Text(
                  "No pending requests",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              );
            } else {
              return ListView.builder(
                itemCount: destinations.length,
                itemBuilder: (context, index) {
                  final destination = destinations[index];
                  final List<Member>? membersData = destination.members;
                  // Filter members with a "pending" status
                  final List<Member> pendingMembers = membersData!
                      .where((member) => member.status == 'Pending')
                      .toList();

                  final List<Widget> pendingMemberTiles =
                      pendingMembers.map((member) {
                    return InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) =>
                              TravelerProfilePage(id: member.userId),
                        ));
                      },
                      child: ListTile(
                        title: Text(
                          member.userName,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                        subtitle: Text(
                          "Wants to join trip ${destination.name}",
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ElevatedButton(
                              onPressed: () async {
                                String? token = await getToken();
                                // Handle accept button click

                                var headersList = {
                                  'Accept': '*/*',
                                  'User-Agent':
                                      'Thunder Client (https://www.thunderclient.com)',
                                  'Authorization': 'Bearer $token'
                                };
                                var url = Uri.parse(
                                    '$baseURL/api/destinations/${destination.id}/accept/${member.userId}');

                                var req = http.Request('PUT', url);
                                req.headers.addAll(headersList);
                                var res = await req.send();
                                final resBody =
                                    await res.stream.bytesToString();
                                var jsonDecoded = jsonDecode(resBody);
                                if (res.statusCode >= 200 &&
                                    res.statusCode < 300) {
                                  print(resBody);
                                  Navigator.of(context)
                                      .pushReplacement(MaterialPageRoute(
                                    builder: (context) =>
                                        const NotificationPage(),
                                  ));
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: const Text("Accepted request"),
                                      backgroundColor: Colors.green.shade300,
                                      behavior: SnackBarBehavior.floating,
                                      duration:
                                          const Duration(milliseconds: 2500),
                                      action: SnackBarAction(
                                        label: 'Dismiss',
                                        disabledTextColor: kLightColor,
                                        textColor: kLightColor,
                                        onPressed: () {
                                          ScaffoldMessenger.of(context)
                                              .removeCurrentSnackBar();
                                        },
                                      ),
                                    ),
                                  );
                                } else {
                                  print(res.reasonPhrase);
                                  if (jsonDecoded.containsKey('message')) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(jsonDecoded['message']),
                                        backgroundColor: Colors.red.shade300,
                                        behavior: SnackBarBehavior.floating,
                                        duration:
                                            const Duration(milliseconds: 2500),
                                        action: SnackBarAction(
                                          label: 'Dismiss',
                                          disabledTextColor: kLightColor,
                                          textColor: kLightColor,
                                          onPressed: () {
                                            ScaffoldMessenger.of(context)
                                                .removeCurrentSnackBar();
                                          },
                                        ),
                                      ),
                                    );
                                  }
                                }
                              },
                              child: const Text('Accept'),
                            ),
                            const SizedBox(width: 8),
                            ElevatedButton(
                              onPressed: () async {
                                // Handle delete button click
                                String? token = await getToken();

                                var headersList = {
                                  'Accept': '*/*',
                                  'User-Agent':
                                      'Thunder Client (https://www.thunderclient.com)',
                                  'Authorization': 'Bearer $token'
                                };
                                var url = Uri.parse(
                                    '$baseURL/api/destinations/${destination.id}/member/${member.userId}');

                                var res = await http.delete(
                                  url,
                                  headers: headersList,
                                );

                                var jsonDecoded = jsonDecode(res.body);

                                if (res.statusCode >= 200 &&
                                    res.statusCode < 300) {
                                  // Successfully deleted member
                                  Navigator.of(context)
                                      .pushReplacement(MaterialPageRoute(
                                    builder: (context) =>
                                        const NotificationPage(),
                                  ));
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: const Text("Declined request"),
                                      backgroundColor: Colors.red.shade300,
                                      behavior: SnackBarBehavior.floating,
                                      duration:
                                          const Duration(milliseconds: 2500),
                                      action: SnackBarAction(
                                        label: 'Dismiss',
                                        disabledTextColor: kLightColor,
                                        textColor: kLightColor,
                                        onPressed: () {
                                          ScaffoldMessenger.of(context)
                                              .removeCurrentSnackBar();
                                        },
                                      ),
                                    ),
                                  );
                                } else {
                                  print(res.reasonPhrase);
                                  if (jsonDecoded.containsKey('message')) {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(jsonDecoded['message']),
                                        backgroundColor: Colors.red.shade300,
                                        behavior: SnackBarBehavior.floating,
                                        duration:
                                            const Duration(milliseconds: 2500),
                                        action: SnackBarAction(
                                          label: 'Dismiss',
                                          disabledTextColor: kLightColor,
                                          textColor: kLightColor,
                                          onPressed: () {
                                            ScaffoldMessenger.of(context)
                                                .removeCurrentSnackBar();
                                          },
                                        ),
                                      ),
                                    );
                                  }
                                }
                              },
                              child: const Text('Decline'),
                            ),
                          ],
                        ),
                      ),
                    );
                  }).toList();
                  return Column(
                    children: pendingMemberTiles,
                  );
                },
              );
            }
          }
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        },
      ),
    );
  }
}
