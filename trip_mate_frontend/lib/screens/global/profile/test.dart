// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';

class ScrollableDropdownMenu extends StatefulWidget {
  const ScrollableDropdownMenu({super.key});

  @override
  _ScrollableDropdownMenuState createState() => _ScrollableDropdownMenuState();
}

class _ScrollableDropdownMenuState extends State<ScrollableDropdownMenu> {
  List<String> options = [
    'Option 1',
    'Option 2',
    'Option 3',
    'Option 4',
    'Option 5',
    'Option 6',
    'Option 7',
    'Option 8',
    'Option 9',
    'Option 10',
    'Option 11',
    'Option 12',
    'Option 13',
    'Option 14',
    'Option 15',
    'Option 16',
    'Option 17',
    'Option 18',
    'Option 19',
    'Option 20',
  ];

  String? selectedOption;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Scrollable Dropdown Menu'),
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: SingleChildScrollView(
            child: DropdownButton<String>(
              isExpanded: true,
              hint: const Text('Select an option'),
              value: selectedOption,
              onChanged: (String? newValue) {
                setState(() {
                  selectedOption = newValue;
                });
              },
              items:
                  options.take(5).map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          ),
        ),
      ),
    );
  }
}
