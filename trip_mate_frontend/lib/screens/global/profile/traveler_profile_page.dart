// ignore_for_file: unnecessary_null_comparison

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:trip_mate_frontend/controllers/bloc/destination/destinations_bloc.dart';

import 'package:trip_mate_frontend/models/user.dart';
import 'package:trip_mate_frontend/utils/constants.dart';
import 'package:trip_mate_frontend/utils/functions.dart';
import 'package:trip_mate_frontend/utils/size_config.dart';

import '../../../controllers/bloc/user/user_bloc.dart';
import '../messages/chat_screen.dart';

// ignore: must_be_immutable
class TravelerProfilePage extends StatefulWidget {
  String id;
  TravelerProfilePage({Key? key, required this.id}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _TravelerProfilePageState createState() => _TravelerProfilePageState();
}

class _TravelerProfilePageState extends State<TravelerProfilePage> {
  // late Map<String, dynamic> travelerDetail = {};

  // Widget buildImageSlider(List profileImage) {
  //   if (profileImage != null) {
  //     List<String> profileImages = List<String>.from(profileImage);

  //     if (profileImages.isEmpty) {
  //       return Image.asset(
  //         'assets/images/noimage.jpg',
  //         fit: BoxFit.cover,
  //       );
  //     } else {
  //       return Swiper(
  //         itemBuilder: (BuildContext context, int index) {
  //           return Image.network(
  //             '$baseURL/${profileImages[index]}',
  //             fit: BoxFit.cover,
  //           );
  //         },
  //         itemCount: profileImages.length,
  //         pagination: const SwiperPagination(),
  //         // control: SwiperControl(),
  //       );
  //     }
  //   } else {
  //     return Image.asset(
  //       'assets/images/noimage.jpg',
  //       fit: BoxFit.cover,
  //     );
  //   }
  // }
  String? userId;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<UserBloc>(context).add(GetTravelerEvent(id: widget.id));
    BlocProvider.of<DestinationsBloc>(context)
        .add(ShowMyTripsEvent(userId: widget.id));
  }

  getUser() async {
    userId = (await getId())!;
    setState(() {});
  }

  String calculateAge(String? userDateOfBirth) {
    if (userDateOfBirth == null) {
      return 'N/A';
    }

    DateTime today = DateTime.now();
    DateTime dob = DateFormat("yyyy-MM-dd").parse(userDateOfBirth);

    int age = today.year - dob.year;
    if (today.month < dob.month ||
        (today.month == dob.month && today.day < dob.day)) {
      age--;
    }

    return age.toString();
  }

  bool isFavorite = false;
  void toggleFavorite() {
    setState(() {
      isFavorite = !isFavorite;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        if (state is TravellerLoading) {
          return const Scaffold(
            body: Center(
              child: CircularProgressIndicator.adaptive(),
            ),
          );
        } else if (state is TravellerLoaded) {
          User user = state.user;

          return DefaultTabController(
            length: 2,
            child: Scaffold(
              body: SingleChildScrollView(
                child: Column(
                  children: [
                    Stack(children: [
                      user.profileImage == null
                          ? Image.asset(
                              'assets/images/noimage.jpg',
                              fit: BoxFit.cover,
                              height: 400,
                              width: double.infinity,
                            )
                          : SizedBox(
                              height: 400,
                              width: double
                                  .infinity, // Adjust the height as needed
                              child: Image.network(
                                  '$baseURL/${user.profileImage}',
                                  fit: BoxFit.cover),
                            ),
                      Positioned(
                        top: 40,
                        left: 10,
                        child: GestureDetector(
                          onTap: () {
                            BlocProvider.of<DestinationsBloc>(context)
                                .add(DestinationsInitialEvent());
                            BlocProvider.of<UserBloc>(context)
                                .add(UserInitialEvent());
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                              border: Border.all(width: 2, color: Colors.white),
                            ),
                            child: const Center(
                              child: Icon(
                                Icons.arrow_back,
                                size: 30,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ]),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                '${user.firstName ?? ''} ${user.lastName ?? ''},',
                                style: const TextStyle(
                                    fontSize: 24, fontWeight: FontWeight.bold),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Text(
                                calculateAge(user.dateOfBirth.toString()),
                                style: const TextStyle(
                                    fontSize: 24, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.location_on_outlined,
                                size: 30,
                                color: Colors.grey.shade600,
                              ),
                              Text(
                                '${user.city}, ${user.state}',
                                style: TextStyle(
                                    fontSize: 20, color: Colors.grey.shade600),
                              ),
                            ],
                          ),
                          const SizedBox(height: 5),
                          Row(
                            children: [
                              Icon(Icons.translate,
                                  size: 30, color: Colors.grey.shade600),
                              Text(
                                user.languages!.isEmpty
                                    ? 'Nepali'
                                    : user.languages!.join(', '),
                                style: TextStyle(
                                    fontSize: 20, color: Colors.grey.shade600),
                              ),
                            ],
                          ),
                          const SizedBox(height: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'About Me: ',
                                style: TextStyle(
                                    fontSize: 16, color: Colors.grey.shade600),
                              ),
                              const SizedBox(height: 5),
                              Text(
                                user.aboutMe ?? 'N/A',
                                style: TextStyle(
                                    fontSize: 16, color: Colors.grey.shade600),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(children: [
                              Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: const Color(0xFFE5E4E4),
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    width: 0.5,
                                  ),
                                ),
                                child: IconButton(
                                  icon: Icon(
                                    isFavorite
                                        ? Icons.favorite
                                        : Icons.favorite_border_rounded,
                                    color: isFavorite
                                        ? Colors.red
                                        : const Color.fromARGB(255, 70, 69, 69),
                                    size: 30,
                                  ),
                                  onPressed: () {
                                    toggleFavorite();
                                  },
                                ),
                              ),
                              const SizedBox(width: 10),
                              Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: const Color(0xFFE5E4E4),
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    width: 0.5,
                                  ),
                                ),
                                child: IconButton(
                                  icon: const Icon(
                                      Icons.messenger_outline_rounded,
                                      color: Color.fromARGB(255, 70, 69, 69),
                                      size: 30),
                                  onPressed: () {
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (context) => ChatScreen(
                                          participant: user,
                                          isNewChat: true,
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              )
                            ]),
                          ),
                          const SizedBox(height: 10),
                          const TabBar(
                            tabs: [
                              Tab(
                                child: Text(
                                  'More Info',
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.black),
                                ),
                              ),
                              Tab(
                                child: Text(
                                  'Trips',
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.black),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                                height: 350,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                    gradient: const LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [
                                        Color(0xFFD9D9D9),
                                        Color(0x00D9D9D9),
                                        Color(0x00D9D9D9),
                                        Color(0x00D9D9D9),
                                        Color(0x00D9D9D9),
                                        Color(0xFFD9D9D9),
                                      ],
                                      stops: [
                                        0,
                                        0.3229,
                                        0.5937,
                                        0.7812,
                                        0.8385,
                                        1
                                      ],
                                    ),
                                    border: Border.all(
                                      width: 0.5,
                                      color: Colors.grey,
                                    ),
                                    borderRadius: BorderRadius.circular(15)),
                                child: TabBarView(children: [
                                  SingleChildScrollView(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        children: [
                                          const Text(
                                            'Just Few More Things About Me',
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Container(
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                border: Border.all(
                                                  width: 0.5,
                                                  color: Colors.grey,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(15)),
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 20.0,
                                                  top: 10,
                                                  bottom: 5),
                                              child: Column(
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      const Expanded(
                                                        flex: 7,
                                                        child: Text(
                                                          'Qualification',
                                                          style: TextStyle(
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        flex: 5,
                                                        child: Text(
                                                          user.qualification ??
                                                              'N/A',
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const Divider(
                                                    thickness: 1,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      const Expanded(
                                                        flex: 7,
                                                        child: Text(
                                                          'Relationship Status',
                                                          style: TextStyle(
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        flex: 5,
                                                        child: Text(
                                                          user.maritalStatus ??
                                                              'N/A',
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const Divider(
                                                    thickness: 1,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      const Expanded(
                                                        flex: 7,
                                                        child: Text(
                                                          "I'm Looking For",
                                                          style: TextStyle(
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        flex: 5,
                                                        child: Text(
                                                          user.lookingFor ??
                                                              'N/A',
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const Divider(
                                                    thickness: 1,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      const Expanded(
                                                        flex: 7,
                                                        child: Text(
                                                          'Drinking Habit',
                                                          style: TextStyle(
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        flex: 5,
                                                        child: Text(
                                                          user.drinker ?? 'N/A',
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const Divider(
                                                    thickness: 1,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: [
                                                      const Expanded(
                                                        flex: 7,
                                                        child: Text(
                                                          'Smoker',
                                                          style: TextStyle(
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        flex: 5,
                                                        child: Text(
                                                          user.smoker ?? 'N/A',
                                                          style:
                                                              const TextStyle(
                                                            fontSize: 16,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  SingleChildScrollView(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: BlocBuilder<DestinationsBloc,
                                          DestinationsState>(
                                        builder: (context, state) {
                                          if (state is ShowMyTripsLoading) {
                                            return const Center(
                                              child: CircularProgressIndicator
                                                  .adaptive(),
                                            );
                                          } else if (state is ShowMyTrips) {
                                            return Column(children: [
                                              for (int i = 0;
                                                  i < state.destinations.length;
                                                  i++)
                                                Row(
                                                  children: [
                                                    Container(
                                                      width: SizeConfig(context)
                                                              .deviceWidth() *
                                                          0.75,
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      decoration: BoxDecoration(
                                                          color: Colors.white,
                                                          border: Border.all(
                                                            width: 0.5,
                                                            color: Colors.grey,
                                                          ),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      15)),
                                                      child: Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Row(
                                                                children: [
                                                                  const Icon(
                                                                    Icons
                                                                        .location_on,
                                                                    size: 15,
                                                                    color: Colors
                                                                        .orange,
                                                                  ),
                                                                  const SizedBox(
                                                                    width: 5,
                                                                  ),
                                                                  Text(
                                                                    state
                                                                        .destinations[
                                                                            i]
                                                                        .name!,
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                    maxLines: 1,
                                                                    style: const TextStyle(
                                                                        fontSize:
                                                                            18,
                                                                        fontWeight:
                                                                            FontWeight.bold),
                                                                  ),
                                                                ],
                                                              ),
                                                              Row(
                                                                children: [
                                                                  const Icon(
                                                                    Icons
                                                                        .date_range,
                                                                    size: 15,
                                                                  ),
                                                                  const SizedBox(
                                                                    width: 5,
                                                                  ),
                                                                  Text(
                                                                    DateFormat.d().format(state
                                                                        .destinations[
                                                                            i]
                                                                        .startDate!),
                                                                    style: const TextStyle(
                                                                        fontSize:
                                                                            14),
                                                                  ),
                                                                  Text(
                                                                    DateFormat.MMM().format(state
                                                                        .destinations[
                                                                            i]
                                                                        .startDate!),
                                                                    style: const TextStyle(
                                                                        fontSize:
                                                                            14),
                                                                  ),
                                                                  const Text(
                                                                      '-'),
                                                                  Text(
                                                                    DateFormat.d().format(state
                                                                        .destinations[
                                                                            i]
                                                                        .endDate!),
                                                                    style: const TextStyle(
                                                                        fontSize:
                                                                            14),
                                                                  ),
                                                                  Text(
                                                                    DateFormat.MMM().format(state
                                                                        .destinations[
                                                                            i]
                                                                        .endDate!),
                                                                    style: const TextStyle(
                                                                        fontSize:
                                                                            14),
                                                                  ),
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                          const SizedBox(
                                                              width: 10),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                            ]);
                                          }
                                          return const Center(
                                            child: CircularProgressIndicator
                                                .adaptive(),
                                          );
                                        },
                                      ),
                                    ),
                                  )
                                ])),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        }
        return const Center(
          child: CircularProgressIndicator.adaptive(),
        );
      },
    );
  }
}
