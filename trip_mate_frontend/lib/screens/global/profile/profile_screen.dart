import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';
import 'package:trip_mate_frontend/screens/global/profile/profile_info.dart';
import 'package:trip_mate_frontend/utils/constants.dart';
import '../../../controllers/bloc/user/user_bloc.dart';
import '../../../models/user.dart';
import '../change_password.dart';
import '../destinations/my_destinations.dart';

class ProfileScreen extends StatefulWidget {
  final User? user;

  const ProfileScreen({super.key, this.user});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  Get.back();
                },
                child: const ClipRRect(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20),
                    ),
                    child: Icon(
                      Icons.arrow_back_ios_new,
                      color: Colors.black,
                    )),
              ),
            ],
          ),
          centerTitle: false,
          backgroundColor: Colors.white,
          elevation: 0.0,
        ),
        body: SizedBox(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Stack(
                      children: [
                        SizedBox(
                          width: 130,
                          height: 130,
                          child: widget.user!.profileImage != null
                              ? Container(
                                  width: 100,
                                  height: 100,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: NetworkImage(
                                            '$baseURL/${widget.user!.profileImage}')),
                                  ),
                                )
                              : Container(
                                  width: 100,
                                  height: 100,
                                  decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: AssetImage(
                                              "assets/images/noimage.jpg"))),
                                ),
                        ),
                      ],
                    ),
                    Text(
                      "${widget.user!.firstName} ${widget.user!.lastName}",
                      style: const TextStyle(
                          fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Text(widget.user!.email!),
                    const Icon(
                      Icons.check_box_rounded,
                      color: Colors.green,
                    ),
                  ],
                ),
                SizedBox(
                  height: 300,
                  width: 400,
                  child: Column(
                    children: [
                      ListTile(
                        leading: const Icon(
                          CupertinoIcons.info,
                          color: Colors.purple,
                        ),
                        title: const Text(
                          'My Information',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: const Text(
                          'View your personal details',
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => ProfileInfo(
                                    user: widget.user!,
                                  )));
                        },
                      ),
                      ListTile(
                        leading: const Icon(
                          CupertinoIcons.airplane,
                          color: Colors.purple,
                        ),
                        title: const Text(
                          'My Trips',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: const Text(
                          'View your upcoming trips',
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => MyDestinations(
                                    userId: widget.user!.id!,
                                  )));
                        },
                      ),
                      ListTile(
                        leading: const Icon(
                          CupertinoIcons.lock,
                          color: Colors.purple,
                        ),
                        title: const Text(
                          'Change Password',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: const Text(
                          'Set new password ',
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => const ChangePassword()));
                        },
                      ),
                      ListTile(
                          leading: const Icon(
                            Icons.exit_to_app_sharp,
                            color: Colors.purple,
                          ),
                          title: const Text('Logout',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          subtitle: const Text('Signout from tripMate'),
                          onTap: () {
                            BlocProvider.of<UserBloc>(context)
                                .add(UserLogoutEvent());
                          }),
                    ],
                  ),
                )
              ],
            )));
  }
}
