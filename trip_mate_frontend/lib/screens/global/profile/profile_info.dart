// ignore_for_file: must_be_immutable

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:image_picker/image_picker.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

import 'package:trip_mate_frontend/components/custom_dropdown.dart';
import 'package:trip_mate_frontend/utils/constants.dart';

import 'package:trip_mate_frontend/utils/size_config.dart';
import '../../../components/text_field.dart';
import '../../../controllers/bloc/user/user_bloc.dart';
import '../../../models/user.dart';

class ProfileInfo extends StatefulWidget {
  User user;

  ProfileInfo({Key? key, required this.user}) : super(key: key);

  @override
  State<ProfileInfo> createState() => _ProfileInfoState();
}

class _ProfileInfoState extends State<ProfileInfo> {
  final _formkey = GlobalKey<FormState>();
  XFile? pickedImage;
  final ImagePicker _picker = ImagePicker();
  final ScrollController _scrollController = ScrollController();

  final TextEditingController dateController = TextEditingController();
  final TextEditingController aboutMeController = TextEditingController();
  final TextEditingController lookingForController = TextEditingController();
  final TextEditingController smokerController = TextEditingController();
  final TextEditingController drinkerController = TextEditingController();
  final TextEditingController heightController = TextEditingController();
  final TextEditingController weightController = TextEditingController();
  // final TextEditingController languageController = TextEditingController();
  List<String> selectedLanguages = [];

  final List<String> languages = [
    'Nepali',
    'English',
    'Spanish',
    'French',
    'German',
    'Chinese',
    'Japanese',
    'Maithili',
    'Bhojpuri',
    'Tharu',
    'Tamang',
    'Newari (Nepal Bhasa)',
    'Magar',
    'Awadhi',
    'Rai',
    'Limbu',
    'Gurung',
    'Sherpa',
    'Urdu',
    'Tibetan',
    'Chepang',
    'Sunuwar',
    'Bajjika',
    'Doteli',
    'Kumal',
    'Danuwar',
    'Darchula',
  ];
  final List<String> lookingFor = [
    'Friendship',
    'Dating',
    'Relationship',
    'Travel Buddy',
    'Business Partner',
    'Others',
  ];
  final List<String> smoker = [
    'Yes',
    'No',
    'Occasionally',
  ];
  final List<String> drinker = [
    'Yes',
    'No',
    'Occasionally',
  ];
  late String selectedLookingFor = lookingFor[0];
  late String selectedSmoker = smoker[1];
  late String selectedDrinker = drinker[1];

  @override
  void initState() {
    super.initState();
    aboutMeController.text = widget.user.aboutMe ?? '';
    lookingForController.text = widget.user.lookingFor.toString();
    smokerController.text = widget.user.smoker.toString();
    drinkerController.text = widget.user.drinker.toString();
    selectedLanguages = widget.user.languages!;
    heightController.text = widget.user.height.toString();
    weightController.text = widget.user.weight.toString();
  }

  String? cityValidation(String? value) {
    if (value == null || value.isEmpty || value.trim() == '') {
      return 'City Name is required';
    }
    return null;
  }

  String? weightValidation(double? value) {
    if (value == null || value.isNaN || value <= 0) {
      return 'Weight is required';
    }
    return null;
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: const ClipRRect(
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
              child: Icon(
                Icons.arrow_back_ios_new,
                color: Colors.black,
              ),
            ),
          ),
          title: Text(
            "Edit Profile",
            style: TextStyle(
              fontSize: SizeConfig(context).nameSize(),
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 0.0,
        ),
        body: SingleChildScrollView(
          controller: _scrollController,
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: SizedBox(
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Change you Image:",
                          style: TextStyle(
                            fontSize: SizeConfig(context).nameSize(),
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Stack(
                              children: [
                                SizedBox(
                                  width: 200,
                                  height: 300,
                                  child: widget.user.profileImage != null
                                      ? (pickedImage != null)
                                          ? Container(
                                              width: 150,
                                              height: 150,
                                              decoration: BoxDecoration(
                                                shape: BoxShape.rectangle,
                                                image: DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: FileImage(
                                                    File(pickedImage!.path),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : Container(
                                              width: 150,
                                              height: 150,
                                              decoration: BoxDecoration(
                                                shape: BoxShape.rectangle,
                                                image: DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: NetworkImage(
                                                    '$baseURL/${widget.user.profileImage}',
                                                  ),
                                                ),
                                              ),
                                            )
                                      : Container(
                                          width: 150,
                                          height: 150,
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            image: DecorationImage(
                                              fit: BoxFit.cover,
                                              image: AssetImage(
                                                  "assets/images/noimage.jpg"),
                                            ),
                                          ),
                                          child: const Center(
                                            child: Text("Choose an image"),
                                          ),
                                        ),
                                ),
                                Positioned(
                                  top: 0,
                                  right: 0,
                                  child: GestureDetector(
                                    onTap: () async {
                                      pickedImage = await _picker.pickImage(
                                          source: ImageSource.camera);
                                      setState(() {});
                                    },
                                    child: Container(
                                      height: 36,
                                      width: 36,
                                      decoration: const BoxDecoration(
                                        color: Color(0xFF7D5EFF),
                                        shape: BoxShape.circle,
                                      ),
                                      child: Icon(
                                        Icons.add,
                                        size: 30,
                                        color: pickedImage != '' &&
                                                pickedImage != null
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    Form(
                      key: _formkey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextFieldComponent(
                            fieldName: "About Me",
                            maxLines: 3,
                            controller: aboutMeController,
                            hintText: "About Me",
                          ),
                          GestureDetector(
                            onTap: () async {
                              final selectedValues =
                                  await showDialog<List<String>>(
                                context: context,
                                builder: (BuildContext context) {
                                  return MultiSelectDialog(
                                    height:
                                        SizeConfig(context).deviceHeight() / 2,
                                    searchable: true,
                                    items: languages
                                        .map((language) =>
                                            MultiSelectItem<String>(
                                                language, language))
                                        .toList(),
                                    initialValue: selectedLanguages,
                                  );
                                },
                              );
                              if (selectedValues != null) {
                                setState(() {
                                  selectedLanguages = selectedValues;
                                });
                              }
                            },
                            child: AbsorbPointer(
                              child: TextFieldComponent(
                                fieldName: 'I Speak',
                                hintText: selectedLanguages.isEmpty
                                    ? 'Select Languages'
                                    : selectedLanguages.join(', '),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomDropDown(
                            optionList: lookingFor,
                            selectedOption: selectedLookingFor,
                            helperText: 'Looking For',
                            onChanged: (p) => setState(
                              () {
                                selectedLookingFor = p!;
                              },
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomDropDown(
                            optionList: smoker,
                            selectedOption: selectedSmoker,
                            helperText: 'Smoker',
                            onChanged: (p) => setState(
                              () {
                                selectedSmoker = p!;
                              },
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          CustomDropDown(
                            optionList: drinker,
                            selectedOption: selectedDrinker,
                            helperText: 'Drinking Habit',
                            onChanged: (p) => setState(
                              () {
                                selectedDrinker = p!;
                              },
                            ),
                          ),
                          TextFieldComponent(
                            handleTap: () {
                              const double scrollToPosition =
                                  60.0 * (6 - 1); // Adjust this value as needed
                              _scrollController.animateTo(
                                scrollToPosition,
                                duration: const Duration(milliseconds: 500),
                                curve: Curves.easeInOut,
                              );
                            },
                            fieldName: "Height (cm)",
                            keyboardType: TextInputType.number,
                            controller: heightController,
                          ),
                          TextFieldComponent(
                            handleTap: () {
                              const double scrollToPosition =
                                  60.0 * (8 - 1); // Adjust this value as needed
                              _scrollController.animateTo(
                                scrollToPosition,
                                duration: const Duration(milliseconds: 500),
                                curve: Curves.easeInOut,
                              );
                            },
                            fieldName: "Weight (kg)",
                            keyboardType: TextInputType.number,
                            controller: weightController,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 35),
                            child: Align(
                              child: ElevatedButton(
                                onPressed: () {
                                  if (pickedImage == null &&
                                      widget.user.profileImage == null) {
                                    showDialog(
                                      context: context,
                                      builder: (context) => AlertDialog(
                                          title: const Text(
                                            'Please set Image',
                                          ),
                                          actions: [
                                            ElevatedButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                child: const Text('Ok'))
                                          ]),
                                    );
                                  } else if (_formkey.currentState!
                                      .validate()) {
                                    print(pickedImage);
                                    BlocProvider.of<UserBloc>(context).add(
                                      UserUpdateEvent(
                                        aboutMe: aboutMeController.text,
                                        lookingFor: selectedLookingFor,
                                        smoker: selectedSmoker,
                                        drinker: selectedDrinker,
                                        height: heightController.text,
                                        weight: weightController.text,
                                        selectedLanguages: selectedLanguages,
                                        pickedImage: pickedImage,
                                      ),
                                    );
                                  }
                                },
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.deepPurple,
                                  fixedSize: const Size(150, 55),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                                child: const Text(
                                  "Update",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

// ignore: non_constant_identifier_names
  
