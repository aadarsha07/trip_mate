import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:trip_mate_frontend/models/all_users_model.dart';
import 'package:trip_mate_frontend/screens/admin/show_user.dart';

import '../../controllers/bloc/admin/admin_bloc.dart';
import '../../utils/constants.dart';

class UserInsightsScreen extends StatefulWidget {
  const UserInsightsScreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _UserInsightsScreenState createState() => _UserInsightsScreenState();
}

class _UserInsightsScreenState extends State<UserInsightsScreen> {
  String _selectedOption = 'All';
  List<AllUsers> users = [];
  String _searchQuery = '';

  List<AllUsers> _filteredUsers = [];

  @override
  void initState() {
    super.initState();
    _searchQuery = '';
    BlocProvider.of<AdminBloc>(context).add(AdminGetAllUsersEvent());
    _filteredUsers = List.from(users);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kLightColor,
        elevation: 1,
        title: const Text(
          'USER INSIGHTS',
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25),
        ),
        centerTitle: true,
      ),
      body: BlocBuilder<AdminBloc, AdminState>(
        builder: (context, state) {
          if (state is AdminLoading) {
            return const Center(child: CircularProgressIndicator());
          } else if (state is AdminGetAllUsers) {
            print('Number of users: ${users.length}');
            users = state.users;
            _applyFilter();

            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      DropdownFilter(
                        selectedOption: _selectedOption,
                        onOptionChanged: (newValue) {
                          setState(() {
                            _selectedOption = newValue!;
                          });
                          _applyFilter();
                        },
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: TextField(
                        onChanged: (searchQuery) {
                          print('Search Query: $searchQuery');
                          setState(() {
                            _searchQuery = searchQuery;
                            _applyFilter();
                          });
                        },
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                            borderSide: BorderSide.none,
                          ),
                          hintText: 'Search by name or email',
                          prefixIcon: Icon(
                            Icons.search,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount: _filteredUsers.length,
                    itemBuilder: (context, index) {
                      final user = _filteredUsers[index];
                      return ListTile(
                        title: Text('${user.firstName!} ${user.lastName!}'),
                        subtitle: Text(user.email!),
                        trailing: user.isVerified!
                            ? const Icon(Icons.check, color: Colors.green)
                            : const Icon(Icons.close, color: Colors.red),
                        onTap: () {
                          Get.to(() => ShowUser(userId: user.sId));
                        },
                      );
                    },
                  ),
                ),
              ],
            );
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  // void _applyFilter() {
  //   print('Filtered Users: ${_filteredUsers.length}');
  //   if (_selectedOption == 'All') {
  //     _filteredUsers = List.from(users);
  //   } else if (_selectedOption == 'Verified') {
  //     _filteredUsers = users.where((user) => user.isVerified!).toList();
  //   } else if (_selectedOption == 'Unverified') {
  //     _filteredUsers = users.where((user) => !user.isVerified!).toList();
  //   }
  // }
  void _applyFilter() {
    if (_selectedOption == 'All') {
      _filteredUsers = List.from(users);
    } else if (_selectedOption == 'Verified') {
      _filteredUsers = users.where((user) => user.isVerified!).toList();
    } else if (_selectedOption == 'Unverified') {
      _filteredUsers = users.where((user) => !user.isVerified!).toList();
    }
    // Apply the search query filter as well
    final lowerCaseQuery = _searchQuery.toLowerCase();
    _filteredUsers = _filteredUsers.where((user) {
      return user.firstName!.toLowerCase().contains(lowerCaseQuery) ||
          user.lastName!.toLowerCase().contains(lowerCaseQuery) ||
          user.email!.toLowerCase().contains(lowerCaseQuery);
    }).toList();
  }
}

class DropdownFilter extends StatelessWidget {
  final String selectedOption;
  final ValueChanged<String?> onOptionChanged;

  const DropdownFilter({
    super.key,
    required this.selectedOption,
    required this.onOptionChanged,
  });

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: selectedOption,
      onChanged: onOptionChanged,
      items: ['All', 'Verified', 'Unverified'].map((option) {
        return DropdownMenuItem<String>(
          value: option,
          child: Text(option),
        );
      }).toList(),
    );
  }
}

class User {
  final String username;
  final String email;
  final bool isVerified;

  User({required this.username, required this.email, this.isVerified = false});
}
