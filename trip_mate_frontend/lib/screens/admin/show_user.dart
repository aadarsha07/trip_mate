// ignore_for_file: must_be_immutable

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trip_mate_frontend/controllers/bloc/admin/admin_bloc.dart';
import 'package:trip_mate_frontend/controllers/bloc/user/user_bloc.dart';
import 'package:trip_mate_frontend/models/user.dart';

import '../../utils/constants.dart';

class ShowUser extends StatefulWidget {
  String? userId;
  ShowUser({super.key, this.userId});

  @override
  State<ShowUser> createState() => _ShowUserState();
}

class _ShowUserState extends State<ShowUser> {
  User? users;

  @override
  void initState() {
    BlocProvider.of<UserBloc>(context).add(GetTravelerEvent(id: widget.userId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: kLightColor,
        elevation: 1,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            padding: const EdgeInsets.all(10),
            child: const Icon(
              Icons.arrow_back_ios_new,
              color: Colors.black,
            ),
          ),
        ),
        title: const Text(
          'User Detail',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 25,
          ),
        ),
      ),
      body: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) {
          if (state is TravellerLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is TravellerLoaded) {
            users = state.user;
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 190,
                      width: 120,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: users!.profileImage != null &&
                              users!.profileImage!.isNotEmpty
                          ? Image.network(
                              '$baseURL/${users!.profileImage}',
                              fit: BoxFit.cover,
                            )
                          : Image.asset(
                              'assets/images/noimage.jpg',
                              fit: BoxFit.cover,
                            ),
                    ),
                    const SizedBox(height: 20),
                    const Text(
                      'User Information',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 10),
                    ListTile(
                      title: const Text(
                        'ID',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        widget.userId!,
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                    ListTile(
                      title: const Text(
                        'Is Email Verified',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        users?.isEmailVerified == true
                            ? 'Yes'
                            : users?.isEmailVerified == false
                                ? 'No'
                                : 'Not Available',
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                    ListTile(
                      title: const Text(
                        'Name',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        '${users?.firstName ?? 'Not Available'} ${users?.lastName ?? 'Not Available'}',
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                    ListTile(
                      title: const Text(
                        'Email',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        users?.email ?? 'Not Available',
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                    ListTile(
                      title: const Text(
                        'Date of Birth',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        users?.dateOfBirth?.toString().split(' ')[0] ??
                            'Not Available',
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                    ListTile(
                      title: const Text(
                        'City',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        users?.city ?? 'Not Available',
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                    ListTile(
                      title: const Text(
                        'State',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        users?.state ?? 'Not Available',
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                    ListTile(
                      title: const Text(
                        'Interests',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        users?.interests?.isNotEmpty == true
                            ? users!.interests!.join(', ')
                            : 'Not Available',
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                    ListTile(
                      title: const Text(
                        'Languages',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        users?.languages?.isNotEmpty == true
                            ? users!.languages!.first
                            : 'Not Available',
                        style: const TextStyle(fontSize: 16),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Center(
                      child: ElevatedButton(
                        onPressed: () {
                          users!.isVerified!
                              ? BlocProvider.of<AdminBloc>(context)
                                  .add(UnVerifyUserEvent(id: widget.userId!))
                              : BlocProvider.of<AdminBloc>(context)
                                  .add(VerifyUserEvent(id: widget.userId!));
                        },
                        style: ElevatedButton.styleFrom(
                          padding: const EdgeInsets.symmetric(horizontal: 30),
                        ),
                        child: Text(
                          users!.isVerified! ? 'Unverify User' : 'Verify User',
                          style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                  ],
                ),
              ),
            );
          }
          return const Center(
            child: CupertinoActivityIndicator(),
          );
        },
      ),
    );
  }
}
