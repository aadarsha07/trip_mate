import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';
import 'package:trip_mate_frontend/controllers/bloc/popular_places/popular_places_bloc.dart';
import 'package:trip_mate_frontend/screens/admin/edit_popular_places.dart';

import '../../utils/constants.dart';

class ShowPoularPlace extends StatefulWidget {
  const ShowPoularPlace({super.key});

  @override
  State<ShowPoularPlace> createState() => _ShowPoularPlaceState();
}

class _ShowPoularPlaceState extends State<ShowPoularPlace> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<PopularPlacesBloc>(context).add(
      PopularPlacesInitialEvent(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text('Popular Places',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 22,
            )),
        backgroundColor: kLightColor,
        elevation: 1,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            padding: const EdgeInsets.all(10),
            child: const Icon(
              Icons.arrow_back_ios_new,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: BlocBuilder<PopularPlacesBloc, PopularPlacesState>(
        builder: (context, state) {
          if (state is PopularPlacesInitial) {
            BlocProvider.of<PopularPlacesBloc>(context)
                .add(PopularPlacesInitialEvent());
          } else if (state is PopularPlacesLoaded) {
            return Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    itemCount: state.popularPlaces.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: SizedBox(
                          height: 135,
                          width: double.maxFinite,
                          child: Card(
                            elevation: 0.4,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: InkWell(
                              borderRadius: BorderRadius.circular(12),
                              onTap: () {},
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(12),
                                      child: FadeInImage.assetNetwork(
                                        placeholder:
                                            'assets/images/noimage.jpg',
                                        image:
                                            '$baseURL/${state.popularPlaces[index]['image']}',
                                        height: 120,
                                        width: 120,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            state.popularPlaces[index]['name'],
                                            style: const TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            state.popularPlaces[index]
                                                ['subTitle'],
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: const TextStyle(
                                              fontSize: 14,
                                              color: Colors.grey,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Column(
                                      children: [
                                        IconButton(
                                          onPressed: () {
                                            Get.to(() => EditPopularPlaces(
                                                popularPlaces: state
                                                    .popularPlaces[index]));
                                          },
                                          icon: const Icon(
                                            Icons.edit,
                                            color: Colors.blue,
                                          ),
                                        ),
                                        IconButton(
                                          onPressed: () {
                                            BlocProvider.of<PopularPlacesBloc>(
                                                    context)
                                                .add(
                                              PopularPlacesDeleteEvent(
                                                state.popularPlaces[index]
                                                    ['_id'],
                                              ),
                                            );
                                          },
                                          icon: const Icon(
                                            Icons.delete,
                                            color: Colors.red,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
