import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/route_manager.dart';
import 'package:trip_mate_frontend/controllers/bloc/user/user_bloc.dart';
import 'package:trip_mate_frontend/screens/admin/add_popular_places.dart';
import 'package:trip_mate_frontend/screens/admin/show_popular_places.dart';
import 'package:trip_mate_frontend/utils/constants.dart';

class MenuScreen extends StatefulWidget {
  const MenuScreen({super.key});

  @override
  State<MenuScreen> createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 20,
            ),
            // ElevatedButton(
            //     onPressed: () {}, child: const Text('Add Popular Places')),
            // ElevatedButton(
            //     onPressed: () async {
            //       BlocProvider.of<UserBloc>(context).add(UserLogoutEvent());
            //     },
            //     child: const Text('Logout')),
            _buildRowContent(context, 'Add Popular Places', Icons.add, () {
              Get.to(() => const AddPopularPlaces());
            }),
            _buildRowContent(context, 'Show Popular Places', Icons.add, () {
              Get.to(() => const ShowPoularPlace());
            }),
            _buildRowContent(context, 'Logout', Icons.logout, () async {
              BlocProvider.of<UserBloc>(context).add(UserLogoutEvent());
            }),
          ],
        ),
      ),
    );
  }

  Widget _buildRowContent(BuildContext context, String title, IconData icon,
      void Function()? onPressed) {
    return Column(
      children: [
        GestureDetector(
          onTap: onPressed,
          child: ListTile(
            title: Row(
              children: [
                Icon(
                  icon,
                  color: Colors.orange,
                ),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  title,
                  style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: kDarkColor,
                  ),
                ),
              ],
            ),
            trailing: const Icon(
              Icons.arrow_forward_ios,
              size: 15,
              color: kDarkColor,
            ),
          ),
        ),
        const Divider(
          color: Colors.grey,
          thickness: 0.3,
          height: 1,
        ),
      ],
    );
  }
}
