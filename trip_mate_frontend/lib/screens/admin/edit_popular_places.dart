// ignore_for_file: must_be_immutable

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:trip_mate_frontend/components/text_field.dart';
import 'package:trip_mate_frontend/utils/constants.dart';
import '../../controllers/bloc/popular_places/popular_places_bloc.dart';

class EditPopularPlaces extends StatefulWidget {
  dynamic popularPlaces;
  EditPopularPlaces({super.key, required this.popularPlaces});

  @override
  State<EditPopularPlaces> createState() => _EditPopularPlacesState();
}

class _EditPopularPlacesState extends State<EditPopularPlaces> {
  final ImagePicker _picker = ImagePicker();
  XFile? pickedImage;
  final _formkey = GlobalKey<FormState>();

  TextEditingController nameController = TextEditingController();
  TextEditingController subtitleController = TextEditingController();
  @override
  void initState() {
    super.initState();
    nameController.text = widget.popularPlaces['name'];
    subtitleController.text = widget.popularPlaces['subTitle'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text('Add Popular Places',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 22,
            )),
        backgroundColor: kLightColor,
        elevation: 1,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            padding: const EdgeInsets.all(10),
            child: const Icon(
              Icons.arrow_back_ios_new,
              color: Colors.black,
            ),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formkey,
          child: Column(
            children: [
              Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey,
                      width: 0.5,
                    ),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        TextFieldComponent(
                          controller: nameController,
                          handleValidation: (p0) {
                            if (p0 == null || p0.isEmpty || p0.trim() == '') {
                              return 'Place Name is required';
                            }
                            return null;
                          },
                          fieldName: 'Place Name',
                          hintText: 'Place Name',
                        ),
                        TextFieldComponent(
                          controller: subtitleController,
                          handleValidation: (p0) {
                            if (p0 == null || p0.isEmpty || p0.trim() == '') {
                              return 'Place subtitle is required';
                            }
                            return null;
                          },
                          fieldName: 'Subtitle',
                          hintText: 'Place Subtitle',
                        ),
                        const Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              "Add image of the destination",
                              style: TextStyle(fontSize: 18),
                            )),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey,
                                  width: 0.5,
                                ),
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: TextButton(
                                onPressed: () async {
                                  pickedImage = await _picker.pickImage(
                                      source: ImageSource.gallery);
                                  setState(() {});
                                },
                                child: Align(
                                  alignment: Alignment.bottomLeft,
                                  child: (pickedImage == null)
                                      ? const Text("Pick Image")
                                      : const Text("Change Image"),
                                ),
                              ),
                            ),
                            (widget.popularPlaces['image'] != null)
                                ? (pickedImage != null)
                                    ? Container(
                                        width: 150,
                                        height: 150,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: FileImage(
                                              File(pickedImage!.path),
                                            ),
                                          ),
                                        ),
                                      )
                                    : Image.network(
                                        '$baseURL/${widget.popularPlaces['image']}',
                                        height: 200,
                                        width: 200,
                                        fit: BoxFit.cover,
                                      )
                                : Container(
                                    height: 200,
                                    width: 200,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.grey,
                                        width: 0.5,
                                      ),
                                      borderRadius: BorderRadius.circular(5.0),
                                    ),
                                    child: Image(
                                        image:
                                            FileImage(File(pickedImage!.path))),
                                  )
                          ],
                        ),
                      ],
                    ),
                  )),
              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (pickedImage == null &&
                        widget.popularPlaces['image'] == null) {
                      showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                            title: const Text(
                              'Please set Image',
                            ),
                            actions: [
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text('Ok'))
                            ]),
                      );
                    }
                    if (_formkey.currentState!.validate()) {
                      BlocProvider.of<PopularPlacesBloc>(context).add(
                        PopularPlaceUpdateEvent(
                          widget.popularPlaces['_id'],
                          nameController.text,
                          subtitleController.text,
                          pickedImage,
                        ),
                      );
                    }
                  },
                  child: const Text('Update Place'))
            ],
          ),
        ),
      ),
    );
  }
}
