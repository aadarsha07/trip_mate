// ignore_for_file: file_names

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:trip_mate_frontend/controllers/bloc/admin/admin_bloc.dart';
import 'package:trip_mate_frontend/utils/constants.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<AdminBloc>(context).add(AdminGetAllUsersEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kLightColor,
        elevation: 0,
        title: const Text(
          'DASHBOARD',
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25),
        ),
        centerTitle: true,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: BlocBuilder<AdminBloc, AdminState>(
            builder: (context, state) {
              if (state is AdminLoading) {
                return const Center(child: CircularProgressIndicator());
              } else if (state is AdminGetAllUsers) {
                int verifiedUsers = 0;
                int unverifiedUsers = 0;
                for (var element in state.users) {
                  if (element.isVerified!) {
                    verifiedUsers++;
                  } else {
                    unverifiedUsers++;
                  }
                }
                return Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 200,
                      child: PieChart(
                        PieChartData(
                          sections: [
                            PieChartSectionData(
                              badgeWidget: const Text(
                                'Unverified Users',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                ),
                              ),
                              badgePositionPercentageOffset: 2.2,
                              value: unverifiedUsers.toDouble(),
                              color: Colors.red,
                              title: (unverifiedUsers.toDouble() /
                                      state.users.length.toDouble() *
                                      100)
                                  .toStringAsPrecision(3),
                            ),
                            PieChartSectionData(
                              badgeWidget: const Text(
                                'Verified Users',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                ),
                              ),
                              badgePositionPercentageOffset: 2.2,
                              value: verifiedUsers.toDouble(),
                              color: Colors.green,
                              title: (verifiedUsers.toDouble() /
                                      state.users.length.toDouble() *
                                      100)
                                  .toStringAsPrecision(3),
                            ),
                          ],
                          sectionsSpace: 0,
                          centerSpaceRadius: 70,
                          startDegreeOffset: -90,
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    const Text(
                      'User Insights',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 10),
                    Row(
                      children: [
                        Expanded(
                            child: InsightCard(
                                title: 'Total Users',
                                value: state.users.length.toString())),
                        Expanded(
                            child: InsightCard(
                                title: 'Verified Users',
                                value: verifiedUsers.toString())),
                        Expanded(
                            child: InsightCard(
                                title: 'Unverified Users',
                                value: unverifiedUsers.toString())),
                      ],
                    ),
                  ],
                );
              }
              return const Center(child: CupertinoActivityIndicator());
            },
          ),
        ),
      ),
    );
  }
}

class InsightCard extends StatelessWidget {
  final String title;
  final String value;

  const InsightCard({super.key, required this.title, required this.value});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 2,
            blurRadius: 5,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '$title\n',
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 10),
            Container(
              height: 1,
              color: Colors.grey.withOpacity(0.5), // Divider color
            ),
            const SizedBox(height: 10),
            Text(
              value,
              style: const TextStyle(fontSize: 14),
            ),
          ],
        ),
      ),
    );
  }
}
