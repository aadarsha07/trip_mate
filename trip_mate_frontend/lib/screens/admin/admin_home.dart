import 'package:flutter/material.dart';
import 'package:trip_mate_frontend/screens/admin/dashboard.dart';
import 'package:trip_mate_frontend/screens/admin/menu.dart';
import 'package:trip_mate_frontend/screens/admin/user_screen.dart';

class AdminHome extends StatefulWidget {
  const AdminHome({super.key});

  @override
  State<AdminHome> createState() => _AdminHomeState();
}

class _AdminHomeState extends State<AdminHome> {
  int currentIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  final screens = [
    const DashboardScreen(),
    const UserInsightsScreen(),
    const MenuScreen(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
          onTap: (index) {
            currentIndex = index;
            setState(() {});
          },
          iconSize: 35,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          currentIndex: currentIndex,
          selectedItemColor: const Color(0xff8f294f),
          unselectedItemColor: Colors.grey,
          items: const [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                ),
                label: "Home"),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: "Users"),
            BottomNavigationBarItem(icon: Icon(Icons.menu), label: "Menu")
          ]),
    );
  }
}
