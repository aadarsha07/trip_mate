import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

const kLightColor = Color(0xFFFFFFFF);
const kDarkColor = Color(0xFF000000);
const kPrimaryColor = Color(0xFF3674BD);
const kFailureColor = Color(0xFFF64E60);
const kSuccessColor = Color(0xFF50CD89);
const kNeutralColor = Color(0xFFF3F6F9);
const kIconColor = Color(0xFF667085);

// String baseURL = 'https://tripmate-server.onrender.com';
String baseURL = 'http://localhost:3000';

dynamic token;
dynamic userId;
dynamic role;

// String baseURL = 'http://192.168.1.75:3000';

DateFormat dateFormatter = DateFormat('dd MMM, yyyy');
