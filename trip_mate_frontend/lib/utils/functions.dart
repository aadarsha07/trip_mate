// ignore_for_file: avoid_print

import 'package:shared_preferences/shared_preferences.dart';

Future<void> saveToken(String token) async {
  var prefs = await SharedPreferences.getInstance();
  prefs.setString('accessToken', token);

  print("Token saved to device.");
}

Future<void> saveId(String id) async {
  var prefs = await SharedPreferences.getInstance();
  prefs.setString('accessId', id);

  print("Id saved to device.");
}

Future<void> saveRole(String role) async {
  var prefs = await SharedPreferences.getInstance();
  prefs.setString('role', role);

  print("role saved to device.");
}

Future<void> saveEmail(bool isEmailVerified) async {
  var prefs = await SharedPreferences.getInstance();
  prefs.setBool('isEmailVerified', isEmailVerified);

  print("isEmailVerified saved to device.");
}

Future<String?> getToken() async {
  var prefs = await SharedPreferences.getInstance();
  String? token = prefs.getString('accessToken');
  print(token);
  print("Token fetched from device.");
  return token;
}

Future<String?> getId() async {
  var prefs = await SharedPreferences.getInstance();
  String? id = prefs.getString('accessId');
  print(id);

  print("Id fetched from device.");
  return id;
}

Future<String?> getRole() async {
  var prefs = await SharedPreferences.getInstance();
  String? role = prefs.getString('role');
  print(role);

  print("role fetched from device.");
  return role;
}

Future<bool?> getEmail() async {
  var prefs = await SharedPreferences.getInstance();
  bool? isEmailVerified = prefs.getBool('isEmailVerified');
  print(isEmailVerified);

  print("isEmailVerified fetched from device.");
  return isEmailVerified;
}
