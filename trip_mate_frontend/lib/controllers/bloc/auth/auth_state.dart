part of 'auth_bloc.dart';

@immutable
class AuthState {}

class AuthInitial extends AuthState {}

class LoginLoading extends AuthState {}

class LoginLoaded extends AuthState {
  LoginLoaded();
}

class LoginError extends AuthState {
  final String message;

  LoginError({required this.message});
}

class RegisterLoading extends AuthState {}

class RegisterLoaded extends AuthState {
  RegisterLoaded();
}

class RegisterError extends AuthState {
  final String message;

  RegisterError({required this.message});
}

class SendOtpLoading extends AuthState {}

class SendOtpLoaded extends AuthState {
  final VerificationModel verificationModel;
  SendOtpLoaded({required this.verificationModel});
}

class SendOtpError extends AuthState {
  final String message;

  SendOtpError({required this.message});
}

class VerifyOtpLoading extends AuthState {}

class VerifyOtpLoaded extends AuthState {
  final VerificationModel verificationModel;
  VerifyOtpLoaded({required this.verificationModel});
}

class VerifyOtpError extends AuthState {
  final String message;

  VerifyOtpError({required this.message});
}
