// ignore_for_file: must_be_immutable

part of 'auth_bloc.dart';

@immutable
class AuthEvent {}

class LoginLoadingEvent extends AuthEvent {}

class LoginSubmitEvent extends AuthEvent {
  String? email;
  String? password;

  LoginSubmitEvent({required this.email, required this.password});
}

class LoginLoadedEvent extends AuthEvent {
  final User user;

  LoginLoadedEvent({required this.user});
}

class LoginErrorEvent extends AuthEvent {
  final String message;

  LoginErrorEvent({required this.message});
}

class RegisterLoadingEvent extends AuthEvent {}

class RegisterSubmitEvent extends AuthEvent {
  String email;
  String password;
  String firstName;
  String lastName;
  String role;
  String gender;
  String dateOfBirth;
  XFile pickedImage;
  List<String> interests;
  String state;
  String city;
  String qualification;
  String maritalStatus;
  String height;
  String weight;

  RegisterSubmitEvent(
      {required this.email,
      required this.password,
      required this.firstName,
      required this.lastName,
      required this.role,
      required this.gender,
      required this.dateOfBirth,
      required this.pickedImage,
      required this.interests,
      required this.state,
      required this.city,
      required this.qualification,
      required this.maritalStatus,
      required this.height,
      required this.weight});
}

class RegisterLoadedEvent extends AuthEvent {
  final User user;

  RegisterLoadedEvent({required this.user});
}

class RegisterErrorEvent extends AuthEvent {
  final String message;

  RegisterErrorEvent({required this.message});
}

class SendOtpEvent extends AuthEvent {
  String? email;
  String? userId;
  SendOtpEvent({required this.email, required this.userId});
}

class VerifyOtpEvent extends AuthEvent {
  String? otp;
  String? userId;
  bool isFromRegister;
  VerifyOtpEvent(
      {required this.otp, required this.userId, required this.isFromRegister});
}
