// ignore_for_file: unused_import, unnecessary_import, depend_on_referenced_packages

import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:trip_mate_frontend/controllers/repositories/login_repository.dart';
import 'package:trip_mate_frontend/controllers/repositories/register_repository.dart';
import 'package:trip_mate_frontend/controllers/repositories/send_otp.dart';
import 'package:trip_mate_frontend/controllers/repositories/verify_otp.dart';
import 'package:trip_mate_frontend/models/status_model.dart';
import 'package:trip_mate_frontend/models/verification_model.dart';
import 'package:trip_mate_frontend/screens/admin/admin_home.dart';
import 'package:trip_mate_frontend/screens/auth/login_page.dart';
import 'package:trip_mate_frontend/screens/auth/verification_page.dart';
import 'package:trip_mate_frontend/utils/constants.dart';
import 'package:trip_mate_frontend/utils/functions.dart';
import '../../../models/user.dart';
import '../../../screens/home_page.dart';
part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(AuthInitial()) {
    on<LoginSubmitEvent>((event, emit) async {
      emit(LoginLoading());
      LoginRepository loginRepository = LoginRepository();
      StatusModel status = await loginRepository.login(
          email: event.email!, password: event.password!);
      if (status.status == "success") {
        emit(LoginLoaded());
        saveId(status.id!);
        saveToken(status.token!);
        saveRole(status.role!);
        saveEmail(status.isEmailVerified!);
        if (status.role == 'admin') {
          Get.offAll(() => const AdminHome());
        } else {
          status.isEmailVerified == false
              ? Get.offAll(() => VerificationPage(
                    email: event.email,
                    userId: status.id,
                  ))
              : Get.offAll(() => HomePage());
        }
        Fluttertoast.showToast(
          msg: 'Login Successfull',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: kSuccessColor,
          textColor: kLightColor,
          fontSize: 13,
        );
      } else {
        emit(LoginError(message: status.message!));
        Fluttertoast.showToast(
          msg: status.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: kFailureColor,
          textColor: kLightColor,
          fontSize: 13,
        );
      }
    });
    on<SendOtpEvent>(
      (event, emit) async {
        SendOtpRepository sendOtpRepository = SendOtpRepository();
        emit(SendOtpLoading());
        VerificationModel verificationModel =
            await sendOtpRepository.sendOtp(event.email!, event.userId!);
        if (verificationModel.status == "success") {
          emit(SendOtpLoaded(verificationModel: verificationModel));
          Fluttertoast.showToast(
            msg: verificationModel.message!,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: kSuccessColor,
            textColor: kLightColor,
            fontSize: 13,
          );
        } else {
          emit(SendOtpError(message: verificationModel.message!));
          Fluttertoast.showToast(
            msg: verificationModel.message!,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: kFailureColor,
            textColor: kLightColor,
            fontSize: 13,
          );
        }
      },
    );
    on<VerifyOtpEvent>((event, emit) async {
      VerifiyOtpRepository verifiyOtpRepository = VerifiyOtpRepository();
      emit(VerifyOtpLoading());
      VerificationModel verificationModel =
          await verifiyOtpRepository.verifyOtp(event.otp!, event.userId!);

      if (verificationModel.status == "success") {
        emit(VerifyOtpLoaded(verificationModel: verificationModel));
        Fluttertoast.showToast(
          msg: verificationModel.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: kSuccessColor,
          textColor: kLightColor,
          fontSize: 13,
        );
        saveEmail(true);
        Get.offAll(() => HomePage());
      } else {
        emit(VerifyOtpError(message: verificationModel.message!));
        Fluttertoast.showToast(
          msg: verificationModel.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: kFailureColor,
          textColor: kLightColor,
          fontSize: 13,
        );
      }
    });

    on<RegisterSubmitEvent>((event, emit) async {
      RegisterRepository registerRepository = RegisterRepository();
      emit(RegisterLoading());
      StatusModel status = await registerRepository.register(
        event.email,
        event.password,
        event.firstName,
        event.lastName,
        event.role,
        event.gender,
        event.dateOfBirth,
        event.pickedImage,
        event.interests,
        event.state,
        event.city,
        event.qualification,
        event.maritalStatus,
        event.height,
        event.weight,
      );
      if (status.status == 'success') {
        emit(RegisterLoaded());
        Fluttertoast.showToast(
          msg: status.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: kSuccessColor,
          textColor: kLightColor,
          fontSize: 13.0,
        );
        saveId(status.id!);
        saveToken(status.token!);
        saveRole(status.role!);
        saveEmail(status.isEmailVerified!);

        Get.offAll(() => VerificationPage(
              email: status.email,
              userId: status.id,
              isFromRegister: false,
            ));
      } else {
        emit(RegisterError(message: status.message!));
        Fluttertoast.showToast(
          msg: status.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: kFailureColor,
          textColor: kLightColor,
          fontSize: 13.0,
        );
      }
    });
  }
}
