part of 'destinations_bloc.dart';

@immutable
class DestinationsState {}

class DestinationsInitial extends DestinationsState {}

class DestinationsLoading extends DestinationsState {}

class DestinationsLoaded extends DestinationsState {
  final List<Destination> destinations;

  DestinationsLoaded(this.destinations);
}

class AddDestinationLoading extends DestinationsState {}

class AddDestinationSuccess extends DestinationsState {}

class ShowMyTrips extends DestinationsState {
  final List<Destination> destinations;

  ShowMyTrips(this.destinations);
}

class ShowMyTripsLoading extends DestinationsState {}

class EditDestinationLoading extends DestinationsState {}

class EditDestinationSuccess extends DestinationsState {}

class DeleteDestinationLoading extends DestinationsState {}

class DeleteDestinationSuccess extends DestinationsState {}

class FetchPending extends DestinationsState {
  final List<Destination> destinations;

  FetchPending(this.destinations);
}

class FetchPendingLoading extends DestinationsState {}


