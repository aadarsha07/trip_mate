part of 'destinations_bloc.dart';

@immutable
class DestinationsEvent {
  const DestinationsEvent();
}

class DestinationsInitialEvent extends DestinationsEvent {}

class DestinationsLoadingEvent extends DestinationsEvent {}

class DestinationsLoadedEvent extends DestinationsEvent {}

class AddDestinationEvent extends DestinationsEvent {
  final String name;
  final String description;
  final String peopleLooking;
  final String startDate;
  final String endDate;
  final XFile? pickedImage;

  const AddDestinationEvent({
    required this.name,
    required this.description,
    required this.peopleLooking,
    required this.startDate,
    required this.endDate,
    this.pickedImage,
  });
}

class ShowMyTripsEvent extends DestinationsEvent {
  final String userId;
  const ShowMyTripsEvent({
    required this.userId,
  });
}

class EditDestinationEvent extends DestinationsEvent {
  final String destinationId;
  final String name;
  final String description;
  final String peopleLooking;
  final String startDate;
  final String endDate;
  final XFile? pickedImage;
  final String? userId;

  const EditDestinationEvent({
    required this.destinationId,
    required this.name,
    required this.description,
    required this.peopleLooking,
    required this.startDate,
    required this.endDate,
    this.pickedImage,
    this.userId,
  });
}

class DeleteDestinationEvent extends DestinationsEvent {
  final String destinationId;
  final String? userId;

  const DeleteDestinationEvent({required this.destinationId, this.userId});
}

class ShowDestinationEvent extends DestinationsEvent {
  final String destinationId;

  const ShowDestinationEvent({required this.destinationId});
}

class FetchPendingEvent extends DestinationsEvent {}
