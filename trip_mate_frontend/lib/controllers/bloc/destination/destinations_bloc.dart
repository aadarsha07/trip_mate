// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/route_manager.dart';
import 'package:image_picker/image_picker.dart';
import 'package:trip_mate_frontend/controllers/repositories/add_destination.dart';
import 'package:trip_mate_frontend/controllers/repositories/edit_destination.dart';
import 'package:trip_mate_frontend/models/status_model.dart';
import 'package:trip_mate_frontend/screens/home_page.dart';
import '../../../models/destinations.dart';
import '../../../utils/functions.dart';
import '../../repositories/all_destination.dart';
import '../../repositories/my_trips.dart';

part 'destinations_event.dart';
part 'destinations_state.dart';

class DestinationsBloc extends Bloc<DestinationsEvent, DestinationsState> {
  DestinationsBloc() : super(DestinationsInitial()) {
    on<DestinationsInitialEvent>((event, emit) async {
      String? userId = await getId();
      emit(DestinationsLoading());
      AllDestinationRepository allDestinationRepository =
          AllDestinationRepository();
      DestinationModel destinations =
          await allDestinationRepository.fetchDestinations();
      List<Destination> filteredDestination = destinations.destinations!
          .where((element) => element.addedBy!.userId != userId)
          .toList();
      emit(DestinationsLoaded(filteredDestination));
    });

    on<AddDestinationEvent>((event, emit) async {
      emit(AddDestinationLoading());
      AddDestinationRepository allDestinationRepository =
          AddDestinationRepository();
      StatusModel status = await allDestinationRepository.addDestination(
        name: event.name,
        description: event.description,
        peopleLooking: event.peopleLooking,
        startDate: event.startDate,
        endDate: event.endDate,
        // pickedImage: event.pickedImage,
      );
      if (status.status == 'success') {
        emit(AddDestinationSuccess());
        Fluttertoast.showToast(
          msg: 'Destination added successfully',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0,
        );
        Get.offAll(() => HomePage());
      } else {
        Fluttertoast.showToast(
          msg: 'Something went wrong',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0,
        );
      }
    });

    on<ShowMyTripsEvent>((event, emit) async {
      emit(ShowMyTripsLoading());
      MyTripsRepository myTripsRepository = MyTripsRepository();
      List<Destination> destination = await myTripsRepository.myTrips(
        event.userId,
      );
      emit(ShowMyTrips(destination));
    });

    on<EditDestinationEvent>((event, emit) async {
      emit(EditDestinationLoading());
      EditDestinationRepository editDestination = EditDestinationRepository();
      StatusModel status = await editDestination.editDestination(
        destinationId: event.destinationId,
        name: event.name,
        description: event.description,
        peopleLooking: event.peopleLooking,
        startDate: event.startDate,
        endDate: event.endDate,
        pickedImage: event.pickedImage,
      );
      if (status.status == 'success') {
        emit(EditDestinationSuccess());
        Fluttertoast.showToast(
          msg: 'Destination edited successfully',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0,
        );
        Get.back();
        BlocProvider.of<DestinationsBloc>(Get.context!).add(
          ShowMyTripsEvent(
            userId: event.userId!,
          ),
        );
      } else {
        Fluttertoast.showToast(
          msg: 'Something went wrong',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0,
        );
      }
    });
    on<DeleteDestinationEvent>((event, emit) async {
      emit(DeleteDestinationLoading());
      MyTripsRepository deleteDestinationRepository = MyTripsRepository();
      StatusModel status =
          await deleteDestinationRepository.deleteTrip(event.destinationId);
      if (status.status == 'success') {
        emit(DeleteDestinationSuccess());
        Get.back();
        BlocProvider.of<DestinationsBloc>(Get.context!).add(
          ShowMyTripsEvent(userId: event.userId!),
        );
        Fluttertoast.showToast(
          msg: 'Destination deleted successfully',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0,
        );
      } else {
        Fluttertoast.showToast(
          msg: 'Something went wrong',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0,
        );
      }
    });

    on<FetchPendingEvent>((event, emit) async {
      emit(FetchPendingLoading());
      AllDestinationRepository myTripsRepository = AllDestinationRepository();
      List<Destination> destination =
          await myTripsRepository.fetchPendingDestinations();
      emit(FetchPending(destination));
    });
  }
}
