// ignore_for_file: depend_on_referenced_packages, unnecessary_import

import 'package:bloc/bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/instance_manager.dart';
import 'package:get/route_manager.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:trip_mate_frontend/controllers/repositories/user_repository.dart';
import 'package:trip_mate_frontend/models/status_model.dart';
import 'package:trip_mate_frontend/screens/auth/login_page.dart';
import 'package:trip_mate_frontend/utils/constants.dart';

import '../../../models/user.dart';
import '../../../screens/home_page.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(UserInitial()) {
    on<UserInitialEvent>((event, emit) async {
      emit(UserLoading());
      UserRepository userRepository = UserRepository();
      User user = await userRepository.getUser(event.id);
      emit(UserLoaded(user: user));
    });
    on<UserLogoutEvent>(
      (event, emit) async {
        emit(UserLogoutLoading());
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.remove('accessToken');
        prefs.remove('accessId');
        prefs.remove('role');
        prefs.clear();
        Get.offAll(() => const LoginPage());
      },
    );
    on<UserUpdateEvent>((event, emit) async {
      emit(UserUpdateLoadingState());
      UserRepository userRepository = UserRepository();
      print('selected image ${event.pickedImage}');
      StatusModel status = await userRepository.updateUser(
        aboutMe: event.aboutMe!,
        drinker: event.drinker!,
        height: event.height!,
        lookingFor: event.lookingFor!,
        selectedLanguages: event.selectedLanguages!,
        smoker: event.smoker!,
        weight: event.weight!,
        pickedImage: event.pickedImage,
      );
      if (status.status == 'success') {
        emit(UserUpdateState());
        Fluttertoast.showToast(
          msg: status.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: kSuccessColor,
          textColor: kLightColor,
          fontSize: 13.0,
        );
        Get.offAll(() => HomePage());
      } else {
        Fluttertoast.showToast(
          msg: status.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: kFailureColor,
          textColor: kLightColor,
          fontSize: 13.0,
        );
      }
    });

    on<GetTravelerEvent>((event, emit) async {
      emit(TravellerLoading());
      UserRepository userRepository = UserRepository();
      await userRepository.getUserById(event.id).then((user) {
        emit(TravellerLoaded(user: user));
      });
    });
    on<ChnagePasswordEvent>((event, emit) async {
      emit(ChangePasswordLoadingState());
      UserRepository userRepository = UserRepository();
      StatusModel status = await userRepository.changePassword(
        newPassword: event.newPassword,
        confirmPassword: event.confirmPassword,
      );
      if (status.status == 'success') {
        Fluttertoast.showToast(
          msg: status.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: kSuccessColor,
          textColor: kLightColor,
          fontSize: 13.0,
        );
        Get.offAll(() => const LoginPage());
      } else {
        Fluttertoast.showToast(
          msg: status.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: kFailureColor,
          textColor: kLightColor,
          fontSize: 13.0,
        );
      }
    });
  }
}
