part of 'user_bloc.dart';

@immutable
class UserState {}

class UserInitial extends UserState {}

class UserLoading extends UserState {}

class UserLoaded extends UserState {
  final User user;

  UserLoaded({required this.user});
}

class TravellerLoaded extends UserState {
  final User user;

  TravellerLoaded({required this.user});
}

class TravellerLoading extends UserState {}

class UserLogoutLoading extends UserState {}

class UserUpdateState extends UserState {
  UserUpdateState();
}

class UserUpdateLoadingState extends UserState {}

class ChangePasswordState extends UserState {
  final StatusModel status;

  ChangePasswordState({required this.status});
}

class ChangePasswordLoadingState extends UserState {}
