// ignore_for_file: must_be_immutable

part of 'user_bloc.dart';

@immutable
class UserEvent {}

class UserInitialEvent extends UserEvent {
  final String? id;

  UserInitialEvent({this.id});
}

class UserLoadingEvent extends UserEvent {}

class UserLoadedEvent extends UserEvent {
  final User user;

  UserLoadedEvent({required this.user});
}

class UserLogoutEvent extends UserEvent {}

class GetTravelerEvent extends UserEvent {
  final String? id;

  GetTravelerEvent({this.id});
}

class UserUpdateEvent extends UserEvent {
  String? aboutMe;
  String? lookingFor;
  String? smoker;
  String? drinker;
  String? height;
  String? weight;
  List<String>? selectedLanguages;
  XFile? pickedImage;

  UserUpdateEvent(
      {this.aboutMe,
      this.lookingFor,
      this.smoker,
      this.drinker,
      this.height,
      this.weight,
      this.selectedLanguages,
      this.pickedImage});
}

class UserUpdateLoadingEvent extends UserEvent {}

class ChnagePasswordEvent extends UserEvent {
  final String newPassword;
  final String confirmPassword;

  ChnagePasswordEvent(
      {required this.newPassword, required this.confirmPassword});
}
