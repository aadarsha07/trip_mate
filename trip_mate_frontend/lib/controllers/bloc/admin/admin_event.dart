part of 'admin_bloc.dart';

@immutable
class AdminEvent {}

class AdminInitialEvent extends AdminEvent {}

class AdminGetUsersEvent extends AdminEvent {}

class AdminDeleteUserEvent extends AdminEvent {
  final String id;
  AdminDeleteUserEvent({required this.id});
}

class AdminGetAllUsersEvent extends AdminEvent {}

class AdminUpdateUserEvent extends AdminEvent {
  final String id;
  final String name;
  final String email;
  final String password;
  AdminUpdateUserEvent(
      {required this.id,
      required this.name,
      required this.email,
      required this.password});
}

class AdminAddUserEvent extends AdminEvent {
  final String name;
  final String email;
  final String password;
  AdminAddUserEvent(
      {required this.name, required this.email, required this.password});
}

class VerifyUserEvent extends AdminEvent {
  final String id;
  VerifyUserEvent({required this.id});
}

class UnVerifyUserEvent extends AdminEvent {
  final String id;
  UnVerifyUserEvent({required this.id});
}

class AddPopularPlacesEvent extends AdminEvent {
  final String name;
  final String description;
  final XFile image;

  AddPopularPlacesEvent({
    required this.name,
    required this.description,
    required this.image,
  });
}

class GetPopularPlacesEvent extends AdminEvent {}

