part of 'admin_bloc.dart';

@immutable
class AdminState {}

class AdminInitial extends AdminState {}

class AdminLoading extends AdminState {}

class AdminGetAllUsers extends AdminState {
  final List<AllUsers> users;
  AdminGetAllUsers({required this.users});
}

class AdminError extends AdminState {
  final String message;
  AdminError({required this.message});
}

class VeificationLoading extends AdminState {}

class AddPopularPlace extends AdminState {}

class AddPopularPlaceLoading extends AdminState {}

class GetPopularPlaces extends AdminState {
  final List popularPlaces;
  GetPopularPlaces(this.popularPlaces);
}

class GetPopularPlacesLoading extends AdminState {}
