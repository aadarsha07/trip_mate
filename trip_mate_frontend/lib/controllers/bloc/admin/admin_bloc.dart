// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/route_manager.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';
import 'package:trip_mate_frontend/controllers/repositories/get_all_users.dart';
import 'package:trip_mate_frontend/controllers/repositories/get_popular_places.dart';
import 'package:trip_mate_frontend/models/all_users_model.dart';

import '../../../models/status_model.dart';
import '../../repositories/add_popular_places.dart';
import '../../repositories/userVerifyByAdmin.dart';

part 'admin_event.dart';
part 'admin_state.dart';

class AdminBloc extends Bloc<AdminEvent, AdminState> {
  AdminBloc() : super(AdminInitial()) {
    on<AdminGetAllUsersEvent>((event, emit) async {
      GetAllUsersRepository getAllUsersRepository = GetAllUsersRepository();
      emit(AdminLoading());
      List<AllUsers> allUsers = await getAllUsersRepository.getAllUsers();

      if (allUsers.isEmpty) {
        emit(AdminError(message: 'No users found'));
        return;
      } else {
        emit(AdminGetAllUsers(users: allUsers));
      }
    });

    on<VerifyUserEvent>((event, emit) {
      emit(VeificationLoading());
      UserVerifyByAdmin userVerifyByAdmin = UserVerifyByAdmin();
      userVerifyByAdmin.verifyUser(event.id).then((value) {
        if (value.status == 'success') {
          Fluttertoast.showToast(
            msg: 'User verified successfully',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0,
          );
          Get.back();
          BlocProvider.of<AdminBloc>(Get.context!).add(AdminGetAllUsersEvent());
        } else {
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0,
          );
        }
      });
    });

    on<UnVerifyUserEvent>((event, emit) {
      emit(VeificationLoading());
      UserVerifyByAdmin userVerifyByAdmin = UserVerifyByAdmin();
      userVerifyByAdmin.unverifyUser(event.id).then((value) {
        if (value.status == 'success') {
          Fluttertoast.showToast(
            msg: 'User unverified',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0,
          );
          Get.back();
          BlocProvider.of<AdminBloc>(Get.context!).add(AdminGetAllUsersEvent());
        } else {
          Fluttertoast.showToast(
            msg: 'Something went wrong',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0,
          );
        }
      });
    });
    on<AddPopularPlacesEvent>((event, emit) async {
      emit(AddPopularPlaceLoading());
      AddPopularPlacesRepository addPopularPlacesRepository =
          AddPopularPlacesRepository();
      StatusModel status = await addPopularPlacesRepository.addPopularPlaces(
          event.name, event.description, event.image);
      if (status.status == 'success') {
        Fluttertoast.showToast(
          msg: 'Popular place added successfully',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0,
        );
        Get.back();
      } else {
        Fluttertoast.showToast(
          msg: 'Something went wrong',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0,
        );
      }
    });

    //get popular places
    on<GetPopularPlacesEvent>((event, emit) async {
      emit(GetPopularPlacesLoading());
      GetPopularPlacesRepository getAllUsersRepository =
          GetPopularPlacesRepository();
      List places = await getAllUsersRepository.getPopularPlaces();

      emit(GetPopularPlaces(places));
    });
  }
}
