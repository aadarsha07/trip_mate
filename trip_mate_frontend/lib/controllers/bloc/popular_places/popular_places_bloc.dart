import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/route_manager.dart';
import 'package:image_picker/image_picker.dart';
import 'package:meta/meta.dart';
import 'package:trip_mate_frontend/models/status_model.dart';
import '../../repositories/get_popular_places.dart';
part 'popular_places_event.dart';
part 'popular_places_state.dart';

class PopularPlacesBloc extends Bloc<PopularPlacesEvent, PopularPlacesState> {
  PopularPlacesBloc() : super(PopularPlacesInitial()) {
    on<PopularPlacesInitialEvent>((event, emit) async {
      emit(PopularPlacesLoading());
      GetPopularPlacesRepository popularPlaces = GetPopularPlacesRepository();
      await popularPlaces.getPopularPlaces().then((value) {
        emit(PopularPlacesLoaded(value));
      });
    });

    on<PopularPlacesDeleteEvent>((event, emit) async {
      emit(DeleteLoading());
      GetPopularPlacesRepository popularPlaces = GetPopularPlacesRepository();
      StatusModel status =
          await popularPlaces.deletePopularPlaces(event.placeId);
      if (status.status == 'success') {
        emit(PopularPlacesDelete(status));
        Fluttertoast.showToast(
          msg: status.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 14.0,
        );
        BlocProvider.of<PopularPlacesBloc>(Get.context!).add(
          PopularPlacesInitialEvent(),
        );
      } else {
        emit(PopularPlacesDeleteError(status.message!));
        Fluttertoast.showToast(
          msg: status.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 14.0,
        );
      }
    });

    on<PopularPlaceUpdateEvent>((event, emit) async {
      emit(UpdateLoading());
      GetPopularPlacesRepository popularPlaces = GetPopularPlacesRepository();
      StatusModel status = await popularPlaces.updatePopularPlaces(
          event.placeId, event.name, event.description, event.image);
      if (status.status == 'success') {
        emit(PopularPlacesDelete(status));
        Fluttertoast.showToast(
          msg: status.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 14.0,
        );
        Get.back();
        BlocProvider.of<PopularPlacesBloc>(Get.context!).add(
          PopularPlacesInitialEvent(),
        );
      } else {
        emit(PopularPlacesDeleteError(status.message!));
        Fluttertoast.showToast(
          msg: status.message!,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 14.0,
        );
      }
    });
  }
}
