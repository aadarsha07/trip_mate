part of 'popular_places_bloc.dart';

@immutable
class PopularPlacesEvent {}

class PopularPlacesInitialEvent extends PopularPlacesEvent {}

class PopularPlacesLoadingEvent extends PopularPlacesEvent {}

class PopularPlacesLoadedEvent extends PopularPlacesEvent {
  final List popularPlaces;
  PopularPlacesLoadedEvent(this.popularPlaces);
}

class PopularPlacesDeleteEvent extends PopularPlacesEvent {
  final String placeId;
  PopularPlacesDeleteEvent(this.placeId);
}

class PopularPlaceUpdateEvent extends PopularPlacesEvent {
  final String placeId;
  final String name;
  final String description;
  final XFile? image;
  PopularPlaceUpdateEvent(
      this.placeId, this.name, this.description, this.image);
}
