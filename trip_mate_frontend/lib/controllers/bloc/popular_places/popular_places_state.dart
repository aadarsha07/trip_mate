part of 'popular_places_bloc.dart';

@immutable
class PopularPlacesState {}

class PopularPlacesInitial extends PopularPlacesState {}

class PopularPlacesLoading extends PopularPlacesState {}

class PopularPlacesLoaded extends PopularPlacesState {
  final List popularPlaces;
  PopularPlacesLoaded(this.popularPlaces);
}

class PopularPlacesError extends PopularPlacesState {
  final String message;
  PopularPlacesError(this.message);
}

class PopularPlacesDelete extends PopularPlacesState {
  final StatusModel status;
  PopularPlacesDelete(this.status);
}

class PopularPlacesDeleteError extends PopularPlacesState {
  final String message;
  PopularPlacesDeleteError(this.message);
}

class DeleteLoading extends PopularPlacesState {}

class PopularPlacesUpdate extends PopularPlacesState {
  final StatusModel status;
  PopularPlacesUpdate(this.status);
}

class PopularPlacesUpdateError extends PopularPlacesState {
  final String message;
  PopularPlacesUpdateError(this.message);
}

class UpdateLoading extends PopularPlacesState {}
