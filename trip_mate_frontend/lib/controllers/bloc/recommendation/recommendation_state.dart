// ignore_for_file: must_be_immutable

part of 'recommendation_bloc.dart';

@immutable
class RecommendationState {}

class RecommendationInitial extends RecommendationState {}

class RecommendationLoading extends RecommendationState {}

class RecommendationLoaded extends RecommendationState {
  RecommendationModel recommendations;
  RecommendationLoaded(this.recommendations);
}
