// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:trip_mate_frontend/models/recommendationModel.dart';

import '../../repositories/recommendation.dart';

part 'recommendation_event.dart';
part 'recommendation_state.dart';

class RecommendationBloc
    extends Bloc<RecommendationEvent, RecommendationState> {
  RecommendationBloc() : super(RecommendationInitial()) {
    on<RecommendationInitialEvent>((event, emit) async {
      emit(RecommendationLoading());
      RecommendationRepository recommend = RecommendationRepository();
      RecommendationModel recommendations = await recommend.getRecommendation();
      emit(RecommendationLoaded(recommendations));
    });
  }
}
