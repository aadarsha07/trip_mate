part of 'recommendation_bloc.dart';

@immutable
 class RecommendationEvent {}

  class RecommendationInitialEvent extends RecommendationEvent {}

  class RecommendationLoadingEvent extends RecommendationEvent {}

  class RecommendationLoadedEvent extends RecommendationEvent {}

 

