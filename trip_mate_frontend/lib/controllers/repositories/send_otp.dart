import 'dart:convert';

import 'package:trip_mate_frontend/models/verification_model.dart';
import 'package:trip_mate_frontend/utils/constants.dart';
import 'package:http/http.dart' as http;

class SendOtpRepository {
  Future<VerificationModel> sendOtp(String email, String userId) async {
    try {
      var url = '$baseURL/api/users/$userId/sendEmail';

      var response = await http.post(
        Uri.parse(url),
      );
      print(response.body);
      var jsonDecoded = jsonDecode(response.body);
      return VerificationModel.fromJson(jsonDecoded);
    } catch (e) {
      rethrow;
    }
  }
}
