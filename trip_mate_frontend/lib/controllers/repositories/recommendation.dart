import 'dart:convert';

import 'package:trip_mate_frontend/models/recommendationModel.dart';
import 'package:http/http.dart' as http;

import '../../utils/constants.dart';
import '../../utils/functions.dart';

class RecommendationRepository {
  Future<RecommendationModel> getRecommendation() async {
    String? userId = await getId();
    print('user id is $userId');
    try {
      var url = '$baseURL/api/recommended-users/$userId';
      // var url =
      // 'https://tripmate-server.onrender.com/api/recommended-users/$userId';

      var response = await http.get(
        Uri.parse(url),
      );
      var jsonDecoded = jsonDecode(response.body);
      print(jsonDecoded);
      print('aayo');
      RecommendationModel recommendation =
          RecommendationModel.fromJson(jsonDecoded);
      return recommendation;
    } catch (e) {
      rethrow;
    }
  }
}
