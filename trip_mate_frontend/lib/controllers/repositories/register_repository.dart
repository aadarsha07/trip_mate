// ignore_for_file: unnecessary_null_comparison
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:trip_mate_frontend/models/status_model.dart';
import '../../utils/constants.dart';
import 'package:http/http.dart' as http;

class RegisterRepository {
  Future<StatusModel> register(
    String email,
    String password,
    String firstName,
    String lastName,
    String role,
    String gender,
    String dateOfBirth,
    XFile? pickedImage,
    List<String> interests,
    String state,
    String city,
    String qualification,
    String maritalStatus,
    String height,
    String weight,
  ) async {
    try {
      var url = Uri.parse('$baseURL/api/auth/register');
      var data = {
        'email': email,
        'password': password,
        'firstName': firstName,
        'lastName': lastName,
        'role': role,
        'gender': gender,
        'dateOfBirth': dateOfBirth.toString(),
        'state': state,
        'city': city,
        'qualification': qualification,
        'maritalStatus': maritalStatus,
        'height': height,
        'weight': weight,
      };
      var headersList = {'Accept': '*/*', 'Content-Type': 'application/json'};
      var req = http.MultipartRequest('POST', url);
      for (var i = 0; i < interests.length; i++) {
        req.fields['interests[$i]'] = interests[i];
      }
      req.headers.addAll(headersList);
      req.files.add(
          await http.MultipartFile.fromPath('profileImage', pickedImage!.path));
      req.fields.addAll(data);
      var res = await req.send();
      final resBody = await res.stream.bytesToString();
      final jsonDecoded = jsonDecode(resBody);
      StatusModel status = StatusModel.fromJson(jsonDecoded);
      return status;
    } catch (ex) {
      print(ex);
      rethrow;
    }
  }
}
