import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../components/toast.dart';
import '../../utils/constants.dart';

class JoinDestinationRepository {
  Future joinDestination(
      BuildContext context, String token, String destinationId) async {
    try {
      var apiUrl = "$baseURL/api/destinations/$destinationId/join";
      var response = await http.post(
        Uri.parse(apiUrl),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token'
        },
      );
      var jsonDecoded = jsonDecode(response.body);
      if (response.statusCode == 200) {
        print(jsonDecoded);
        showSuccessToast(jsonDecoded['message'], color: Colors.green);
      } else {
        showSuccessToast(
          jsonDecoded['message'],
          color: Colors.red,
        );
      }
    } catch (e) {
      print(e);
      showSnackBar('Something went wrong', context, false);
    }
  }
}
