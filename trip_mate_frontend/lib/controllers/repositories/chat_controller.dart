import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:trip_mate_frontend/models/chat.dart';
import 'package:trip_mate_frontend/models/message.dart';
import 'package:trip_mate_frontend/models/user.dart' as u;
import 'package:trip_mate_frontend/screens/global/messages/chat_screen.dart';

class ChatController {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  void sendMessage(String message, String chatId, String senderId) {
    _firestore.collection('chat').doc(chatId).set({
      "lastSentAt": DateTime.now(),
      "lastMessage": message,
      "messages": FieldValue.arrayUnion([
        Message(
          senderId: senderId,
          text: message,
          timestamp: DateTime.now(),
        ).toJson()
      ]),
    }, SetOptions(merge: true));
  }

  void startConversation(String recieverId, Message message,
      BuildContext context, u.User chatUser) async {
    _firestore
        .collection('chat')
        .add(
          Chat(
            id: '',
            participantsId: [message.senderId, recieverId],
            lastMessage: message.text,
            lastSentAt: DateTime.now(),
          ).toJson(),
        )
        .then(
      (value) {
        _firestore.collection('chat').doc(value.id).update({
          'id': value.id,
        });
        sendMessage(message.text, value.id, message.senderId);
        Get.off(() => ChatScreen(
              participant: chatUser,
              chatId: value.id,
              isNewChat: false,
            ));
      },
    );
  }
}
