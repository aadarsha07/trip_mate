// ignore_for_file: unnecessary_null_comparison

import 'dart:convert';

import 'package:image_picker/image_picker.dart';
import 'package:trip_mate_frontend/models/status_model.dart';

import 'package:trip_mate_frontend/utils/functions.dart';

import '../../models/user.dart';

import 'package:http/http.dart' as http;

import '../../utils/constants.dart';

class UserRepository {
  Future<User> getUser(String? id) async {
    String? token = await getToken();
    String? userId = await getId();
    try {
      var url = '$baseURL/api/users/$userId';
      var response = await http.get(
        Uri.parse(url),
        headers: {
          'Accept': '*/*',
          'User-Agent': 'Thunder Client (https://www.thunderclient.com)',
          'Authorization': 'Bearer $token',
        },
      );
      var jsonDecoded = jsonDecode(response.body);
      // print(jsonDecoded);
      User user = User.fromJson(jsonDecoded);
      return user;
    } catch (e) {
      rethrow;
    }
  }

  Future<StatusModel> updateUser(
      {required String aboutMe,
      required String lookingFor,
      required String smoker,
      required String drinker,
      required String height,
      required String weight,
      required List<String> selectedLanguages,
      XFile? pickedImage}) async {
    String? token = await getToken();
    String? userId = await getId();
    try {
      var url = '$baseURL/api/users/$userId';
      var headersList = {'Accept': '*/*', 'Authorization': 'Bearer $token'};

      var body = {
        "aboutMe": aboutMe,
        "lookingFor": lookingFor,
        "smoker": smoker,
        "drinker": drinker,
        "height": height,
        "weight": weight,
      };

      final req = http.MultipartRequest(
        'PUT',
        Uri.parse(url),
      );
      req.headers.addAll(headersList);
      for (var i = 0; i < selectedLanguages.length; i++) {
        req.fields['languages[$i]'] = selectedLanguages[i];
      }
      print(pickedImage);
      print('repository');
      if (pickedImage != null) {
        req.files.add(await http.MultipartFile.fromPath(
            'profileImage', pickedImage.path));
      }
      req.fields.addAll(body);
      final res = await req.send();
      final resBody = await res.stream.bytesToString();
      final jsonDecoded = jsonDecode(resBody);
      // print(jsonDecoded);

      StatusModel status = StatusModel.fromJson(jsonDecoded);
      return status;
    } catch (e) {
      rethrow;
    }
  }

  Future<User> getUserById(String? id) async {
    String? token = await getToken();
    try {
      var url = '$baseURL/api/users/$id';
      return http.get(
        Uri.parse(url),
        headers: {
          'Accept': '*/*',
          'User-Agent': 'Thunder Client (https://www.thunderclient.com)',
          'Authorization': "Bearer $token"
        },
      ).then((response) {
        var jsonDecoded = jsonDecode(response.body);
        User user = User.fromJson(jsonDecoded);
        return user;
      });
    } catch (e) {
      rethrow;
    }
  }

  Future<StatusModel> changePassword(
      {required String newPassword, required String confirmPassword}) async {
    String? token = await getToken();
    String? userId = await getId();

    try {
      var url = '$baseURL/api/users/$userId/changepassword';

      var headers = {
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'User-Agent': 'Thunder Client (https://www.thunderclient.com)',
        'Authorization': "Bearer $token"
      };

      var body = {
        "newPassword": newPassword,
        "confirmPassword": confirmPassword,
      };
      var data = jsonEncode(body);

      var response = await http.put(
        Uri.parse(url),
        headers: headers,
        body: data,
      );
      var jsonDecoded = jsonDecode(response.body);
      print(jsonDecoded);
      StatusModel status = StatusModel.fromJson(jsonDecoded);
      return status;
    } catch (e) {
      rethrow;
    }
  }
}
