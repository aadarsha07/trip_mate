import 'dart:convert';

import 'package:image_picker/image_picker.dart';
import 'package:trip_mate_frontend/models/status_model.dart';

import '../../utils/constants.dart';
import 'package:http/http.dart' as http;

import '../../utils/functions.dart';

class AddDestinationRepository {
  Future<StatusModel> addDestination({
    required String name,
    required String description,
    required String peopleLooking,
    required String startDate,
    required String endDate,
    XFile? pickedImage,
  }) async {
    String? userId = await getId();
    final token = await getToken();
    try {
      var url = Uri.parse('$baseURL/api/destinations/$userId');
      var headersList = {
        'Accept': '*/*',
        'User-Agent': 'Thunder Client (https://www.thunderclient.com)',
        'Authorization': 'Bearer $token'
      };
      var body = {
        'name': name,
        'description': description,
        'peopleLooking': peopleLooking,
        'startDate': startDate,
        'endDate': endDate,
      };
      var req = http.MultipartRequest('POST', url);
      req.headers.addAll(headersList);

      if (pickedImage != null) {
        req.files.add(
            await http.MultipartFile.fromPath('placeImage', pickedImage.path));
      }
      req.fields.addAll(body);
      var res = await req.send();
      final resBody = await res.stream.bytesToString();
      final jsonDecoded = jsonDecode(resBody);
      print(jsonDecoded);
      StatusModel status = StatusModel.fromJson(jsonDecoded);
      return status;
    } catch (e) {
      rethrow;
    }
  }
}
