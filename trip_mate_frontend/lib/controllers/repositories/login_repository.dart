// ignore_for_file: unused_import

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trip_mate_frontend/components/toast.dart';
import 'package:trip_mate_frontend/models/status_model.dart';
import '../../models/user.dart';

import '../../screens/admin/admin_home.dart';
import '../../screens/home_page.dart';
import '../../utils/constants.dart';
import 'package:http/http.dart' as http;

import '../../utils/functions.dart';

class LoginRepository {
  Future<StatusModel> login(
      {required String email, required String password}) async {
    try {
      var apiUrl = "$baseURL/api/auth/login";
      final Map<String, dynamic> data = {
        'email': email,
        'password': password,
      };
      var response = await http.post(
        Uri.parse(apiUrl),
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode(data),
      );
      Map<String, dynamic> jsonDecoded = jsonDecode(response.body);
      print(jsonDecoded);
      StatusModel status = StatusModel.fromJson(jsonDecoded);
      return status;
    } catch (ex) {
      print(ex);
      rethrow;
    }
  }
}
