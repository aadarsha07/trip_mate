import 'dart:convert';

import 'package:trip_mate_frontend/models/all_users_model.dart';
import 'package:trip_mate_frontend/utils/constants.dart';
import 'package:http/http.dart' as http;

import '../../utils/functions.dart';

class GetAllUsersRepository {
  Future<List<AllUsers>> getAllUsers() async {
    String? token = await getToken();
    try {
      var url = '$baseURL/api/users';
      var response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      });
      var jsonDecoded = jsonDecode(response.body);
      List<AllUsers> allUsers = [];
      for (var user in jsonDecoded) {
        allUsers.add(AllUsers.fromJson(user));
      }
      // print(allUsers);
      return allUsers;
    } catch (e) {
      rethrow;
    }
  }
}
