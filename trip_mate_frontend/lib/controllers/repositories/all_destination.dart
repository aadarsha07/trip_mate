// ignore_for_file: unused_import

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../../models/destinations.dart';
import '../../utils/constants.dart';
import '../../utils/functions.dart';

class AllDestinationRepository {
  Future<DestinationModel> fetchDestinations() async {
    token = await getToken();
    userId = await getId();

    try {
      String apiUrl = '$baseURL/api/destinations';
      final response = await http.get(
        Uri.parse(apiUrl),
        headers: {
          'Accept': '*/*',
          'User-Agent': 'Thunder Client (https://www.thunderclient.com)',
          'Authorization': 'Bearer $token',
        },
      );
      dynamic jsonDecoded = jsonDecode(response.body);
      DestinationModel destinations = DestinationModel.fromJson(jsonDecoded);
      return destinations;
    } catch (e) {
      rethrow;
    }
  }

  Future<List<Destination>> fetchPendingDestinations() async {
    try {
      String? userId = await getId();
      String? token = await getToken();

      String addedDestinationsUrl = '$baseURL/api/users/$userId/destinations';

      final addedDestinationsResponse = await http.get(
        Uri.parse(addedDestinationsUrl),
        headers: {
          'Accept': '*/*',
          'User-Agent': 'Thunder Client (https://www.thunderclient.com)',
          'Authorization': 'Bearer $token',
        },
      );

      if (addedDestinationsResponse.statusCode == 200) {
        final List<Map<String, dynamic>> addedDestinationsData =
            List<Map<String, dynamic>>.from(
                jsonDecode(addedDestinationsResponse.body));

        final List<Destination> pendingDestinations = addedDestinationsData
            .where((destinationData) {
              final List<Member> membersData = List<Member>.from(
                destinationData['members']
                    .map((memberJson) => Member.fromJson(memberJson)),
              );
              final hasPendingMember =
                  membersData.any((member) => member.status == 'Pending');
              return hasPendingMember;
            })
            .map((item) => Destination.fromJson(item))
            .toList();

        print(pendingDestinations);

        return pendingDestinations;
      } else {
        return [];
      }
    } catch (e) {
      // Handle exceptions
      rethrow;
    }
  }
}
