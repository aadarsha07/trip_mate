import 'dart:convert';

import 'package:image_picker/image_picker.dart';
import 'package:trip_mate_frontend/models/status_model.dart';
import 'package:trip_mate_frontend/utils/constants.dart';
import 'package:http/http.dart' as http;

class AddPopularPlacesRepository {
  Future<StatusModel> addPopularPlaces(
      String name, String description, XFile? image) async {
    try {
      var url = '$baseURL/api/admin/popularPlaces';
      var body = {'name': name, 'subTitle': description};
      var response = http.MultipartRequest('POST', Uri.parse(url));
      response.fields.addAll(body);

      response.files
          .add(await http.MultipartFile.fromPath('image', image!.path));

      var res = await response.send();
      final resBody = await res.stream.bytesToString();
      final jsonDecoded = jsonDecode(resBody);
      print(jsonDecoded);
      StatusModel status = StatusModel.fromJson(jsonDecoded);
      return status;
    } catch (e) {
      rethrow;
    }
  }

 
}
