// ignore_for_file: unused_import

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:trip_mate_frontend/models/status_model.dart';

import '../../models/destinations.dart';

import '../../utils/constants.dart';
import 'package:http/http.dart' as http;

import '../../utils/functions.dart';

class MyTripsRepository {
  Future<List<Destination>> myTrips(String userId) async {
    token = await getToken();
    String addedDestinationsUrl = '$baseURL/api/users/$userId/destinations';
    String joinedDestinationsUrl =
        '$baseURL/api/users/destinations/joined/$userId';

    // Fetch destinations added by the user
    final addedDestinationsResponse = await http.get(
      Uri.parse(addedDestinationsUrl),
      headers: {
        'Accept': '*/*',
        'User-Agent': 'Thunder Client (https://www.thunderclient.com)',
        'Authorization': 'Bearer $token',
      },
    );
    // Fetch destinations where the user is a member
    final joinedDestinationsResponse = await http.get(
      Uri.parse(joinedDestinationsUrl),
      headers: {
        'Accept': '*/*',
        'User-Agent': 'Thunder Client (https://www.thunderclient.com)',
        'Authorization': 'Bearer $token',
      },
    );
    if (addedDestinationsResponse.statusCode == 200 &&
        joinedDestinationsResponse.statusCode == 200) {
      final List<dynamic> addedDestinationsData =
          jsonDecode(addedDestinationsResponse.body);
      final List<dynamic> joinedDestinationsData =
          jsonDecode(joinedDestinationsResponse.body);

      final List<Destination> addedDestinations = addedDestinationsData
          .map((item) => Destination.fromJson(item))
          .toList();
      final List<Destination> joinedDestinations = joinedDestinationsData
          .map((item) => Destination.fromJson(item))
          .toList();

      final List<Destination> acceptedJoinedDestinations =
          joinedDestinations.where((destination) {
        final List<Member>? members = destination.members;
        final bool hasAcceptedMember =
            members!.any((member) => member.status == 'Accepted');
        return hasAcceptedMember;
      }).toList();

      return [...addedDestinations, ...acceptedJoinedDestinations];
    } else {
      return [];
    }
  }

  Future<StatusModel> deleteTrip(String id) async {
    try {
      String? token = await getToken();

      var headersList = {
        'Accept': '*/*',
        'User-Agent': 'Thunder Client (https://www.thunderclient.com)',
        'Authorization': 'Bearer $token'
      };
      var url = Uri.parse('$baseURL/api/destinations/$id');

      var response = await http.delete(url, headers: headersList);
      var jsonDecoded = jsonDecode(response.body);
      print(jsonDecoded);
      StatusModel status = StatusModel.fromJson(jsonDecoded);
      return status;
    } catch (e) {
      rethrow;
    }
  }
}
