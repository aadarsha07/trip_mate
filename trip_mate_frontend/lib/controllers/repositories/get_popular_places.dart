import 'package:image_picker/image_picker.dart';

import '../../models/status_model.dart';
import '../../utils/constants.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class GetPopularPlacesRepository {
  Future<List> getPopularPlaces() async {
    try {
      var url = Uri.parse('$baseURL/api/admin/popularPlaces');
      var response = await http.get(url);
      final resBody = jsonDecode(response.body);
      print(resBody);
      print('popular places');
      return resBody;
    } catch (e) {
      rethrow;
    }
  }

  Future<StatusModel> deletePopularPlaces(String placeId) async {
    try {
      var url = '$baseURL/api/admin/popularPlaces/$placeId';
      var response = await http.delete(Uri.parse(url));
      var jsonDecoded = jsonDecode(response.body);
      print(jsonDecoded);
      StatusModel status = StatusModel.fromJson(jsonDecoded);
      return status;
    } catch (e) {
      rethrow;
    }
  }

  Future<StatusModel> updatePopularPlaces(
    String placeId,
    String name,
    String description,
    XFile? image,
  ) async {
    try {
      var url = '$baseURL/api/admin/popularPlaces/$placeId';
      var body = {
        'name': name,
        'subTitle': description,
      };
      print(url);
      var response = http.MultipartRequest('PUT', Uri.parse(url));

      response.fields.addAll(body);
      if (image != null) {
        response.files
            .add(await http.MultipartFile.fromPath('image', image.path));
      }

      var res = await response.send();
      final resBody = await res.stream.bytesToString();
      final jsonDecoded = jsonDecode(resBody);

      print(jsonDecoded);
      StatusModel status = StatusModel.fromJson(jsonDecoded);
      return status;
    } catch (e) {
      rethrow;
    }
  }
}
