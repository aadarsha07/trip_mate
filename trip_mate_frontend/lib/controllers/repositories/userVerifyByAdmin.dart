import 'dart:convert';

import 'package:trip_mate_frontend/models/status_model.dart';
import 'package:http/http.dart' as http;
import 'package:trip_mate_frontend/utils/constants.dart';

class UserVerifyByAdmin {
  Future<StatusModel> verifyUser(String userId) async {
    try {
      var url = '$baseURL/api/admin/verifyUser/$userId';
      var response = await http.post(Uri.parse(url));
      var jsonDecoded = jsonDecode(response.body);
      StatusModel status = StatusModel.fromJson(jsonDecoded);
      return status;
    } catch (e) {
      rethrow;
    }
  }

  Future<StatusModel> unverifyUser(String userId) async {
    try {
      var url = '$baseURL/api/admin/unverifyUser/$userId';
      var response = await http.post(Uri.parse(url));
      var jsonDecoded = jsonDecode(response.body);
      StatusModel status = StatusModel.fromJson(jsonDecoded);
      return status;
    } catch (e) {
      rethrow;
    }
  }
}
