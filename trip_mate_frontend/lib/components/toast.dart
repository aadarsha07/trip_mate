import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../utils/constants.dart';

void showSuccessToast(String message, {Color? color, isCenter = false}) {
  Fluttertoast.showToast(
    msg: message,
    toastLength: Toast.LENGTH_LONG,
    gravity: isCenter ? ToastGravity.CENTER : ToastGravity.BOTTOM,
    backgroundColor: color ?? Colors.green,
    textColor: Colors.white,
    fontSize: 16.0,
  );
}

void showFailureToast(String message, {Color? color}) {
  Fluttertoast.showToast(
    msg: message,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
    backgroundColor: color ?? Colors.redAccent,
    textColor: Colors.white,
    fontSize: 16.0,
  );
}

void showSnackBar(String message, BuildContext context, bool isSuccess) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(message),
      backgroundColor: isSuccess ? Colors.green.shade300 : Colors.red.shade300,
      behavior: SnackBarBehavior.floating,
      duration: const Duration(
        milliseconds: 2500,
      ),
      action: SnackBarAction(
        label: 'Dismiss',
        disabledTextColor: kLightColor,
        textColor: kLightColor,
        onPressed: () {
          ScaffoldMessenger.of(context).removeCurrentSnackBar();
        },
      ),
    ),
//
  );
}
