import 'package:flutter/material.dart';

// ignore: must_be_immutable
class TextFieldComponent extends StatefulWidget {
  String fieldName;
  FormFieldValidator? validator;

  String? Function(String?)? handleValidation;
  String? hintText;
  IconButton? suffixIcon;
  bool? obscure;
  bool readOnly;
  Icon? prefixIcon;

  final TextInputType? keyboardType;
  int? maxLines;
  bool? enabled;

  TextEditingController? controller;
  void Function()? handleTap;
  TextFieldComponent(
      {super.key,
      this.enabled = true,
      this.keyboardType,
      required this.fieldName,
      this.validator,
      this.hintText,
      this.suffixIcon,
      this.maxLines,
      this.obscure = false,
      this.controller,
      this.handleTap,
      this.prefixIcon,
      this.handleValidation,
      this.readOnly = false});

  @override
  State<TextFieldComponent> createState() => _TextFieldComponentState();
}

class _TextFieldComponentState extends State<TextFieldComponent> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            widget.fieldName,
            style: const TextStyle(
              fontSize: 18,
            ),
          ),
          TextFormField(
            enabled: widget.enabled,
            maxLines: widget.maxLines,
            readOnly: widget.readOnly,
            controller: widget.controller,
            keyboardType: widget.keyboardType,
            validator: widget.handleValidation,
            onTap: widget.handleTap,
            obscureText: widget.obscure!,
            decoration: InputDecoration(
              border: const OutlineInputBorder(),
              contentPadding: const EdgeInsets.all(15),
              hintText: widget.hintText,
              suffixIcon: widget.suffixIcon,
              prefixIcon: widget.prefixIcon,
            ),
          )
        ],
      ),
    );
  }
}
