// ignore_for_file: no_leading_underscores_for_local_identifiers, duplicate_ignore, unused_element, must_be_immutable

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:trip_mate_frontend/utils/constants.dart';

class ProfileImageBox extends StatefulWidget {
  dynamic profileImages;
  final Function(List<XFile?> pickedImages) onImagesPicked;
  ProfileImageBox({Key? key, required this.onImagesPicked, this.profileImages})
      : super(key: key);

  @override
  State<ProfileImageBox> createState() => _ProfileImageBoxState();
}

class _ProfileImageBoxState extends State<ProfileImageBox> {
  final ImagePicker _picker = ImagePicker();
  List<XFile?> pickedImage = [];
  // List<XFile?> pickedImage = [
  //   null,
  //   null,
  //   null,
  // ];

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Stack(
          children: [
            SizedBox(
              width: 200,
              height: 300,
              child: (pickedImage.isNotEmpty && pickedImage[0] != null)
                  ? Container(
                      width: 150,
                      height: 150,
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: FileImage(
                            File(pickedImage[0]!.path),
                          ),
                        ),
                      ),
                    )
                  : widget.profileImages.isNotEmpty &&
                          widget.profileImages[0] != null
                      ? Container(
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(
                                    '$baseURL/${widget.profileImages[0]}')),
                          ),
                        )
                      : Container(
                          width: 150,
                          height: 150,
                          decoration: const BoxDecoration(
                            shape: BoxShape.rectangle,
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: AssetImage("assets/images/noimage.jpg"),
                            ),
                          ),
                          child: const Center(
                            child: Text("Choose an image"),
                          ),
                        ),
            ),
            Positioned(
              top: 0,
              right: 0,
              child: GestureDetector(
                onTap: () {
                  // ignore: no_leading_underscores_for_local_identifiers, unused_element
                  _pickImage() async {
                    final XFile? picked = await _picker.pickImage(
                      source: ImageSource.gallery,
                    );
                    setState(() {
                      pickedImage[0] = picked;
                    });
                    widget.onImagesPicked(pickedImage);
                  }
                },
                child: Container(
                  height: 36,
                  width: 36,
                  decoration: const BoxDecoration(
                    color: Color(0xFF7D5EFF),
                    shape: BoxShape.circle,
                  ),
                  child: Icon(
                    Icons.add,
                    size: 30,
                    color: pickedImage.isNotEmpty ? Colors.white : Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
        Column(
          children: [
            Stack(
              children: [
                SizedBox(
                  width: 150,
                  height: 140,
                  child: (pickedImage.length > 1 && pickedImage[1] != null)
                      ? Container(
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: FileImage(
                                File(pickedImage[1]!.path),
                              ),
                            ),
                          ),
                        )
                      : widget.profileImages.length > 1 &&
                              widget.profileImages[1] != null
                          ? Container(
                              width: 150,
                              height: 150,
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                    '$baseURL/${widget.profileImages[1]}',
                                  ),
                                ),
                              ),
                            )
                          : Container(
                              width: 150,
                              height: 150,
                              decoration: const BoxDecoration(
                                shape: BoxShape.rectangle,
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image:
                                      AssetImage("assets/images/noimage.jpg"),
                                ),
                              ),
                              child: const Center(
                                child: Text("Choose an image"),
                              ),
                            ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: GestureDetector(
                    onTap: () {
                      // ignore: no_leading_underscores_for_local_identifiers, unused_element
                      _pickImage() async {
                        final XFile? picked = await _picker.pickImage(
                          source: ImageSource.gallery,
                        );
                        setState(() {
                          pickedImage[1] = picked;
                        });
                        widget.onImagesPicked(pickedImage);
                      }
                    },
                    child: Container(
                      height: 36,
                      width: 36,
                      decoration: const BoxDecoration(
                        color: Color(0xFF7D5EFF),
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        Icons.add,
                        size: 30,
                        color: pickedImage.isNotEmpty
                            ? Colors.white
                            : Colors.black,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Stack(
              children: [
                SizedBox(
                  width: 150,
                  height: 140,
                  child: (pickedImage.length > 2 && pickedImage[2] != null)
                      ? Container(
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              image: FileImage(
                                File(pickedImage[2]!.path),
                              ),
                            ),
                          ),
                        )
                      : widget.profileImages.length > 2 &&
                              widget.profileImages[2] != null
                          ? Container(
                              width: 150,
                              height: 150,
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                    '$baseURL/${widget.profileImages[2]}',
                                  ),
                                ),
                              ),
                            )
                          : Container(
                              width: 150,
                              height: 150,
                              decoration: const BoxDecoration(
                                shape: BoxShape.rectangle,
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image:
                                      AssetImage("assets/images/noimage.jpg"),
                                ),
                              ),
                              child: const Center(
                                child: Text("Choose an image"),
                              ),
                            ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: GestureDetector(
                    onTap: () {
                      _pickImage() async {
                        final XFile? picked = await _picker.pickImage(
                          source: ImageSource.gallery,
                        );
                        setState(() {
                          pickedImage[2] = picked;
                        });
                        widget.onImagesPicked(pickedImage);
                      }
                    },
                    child: Container(
                      height: 36,
                      width: 36,
                      decoration: const BoxDecoration(
                        color: Color(0xFF7D5EFF),
                        shape: BoxShape.circle,
                      ),
                      child: Icon(
                        Icons.add,
                        size: 30,
                        color: pickedImage.isNotEmpty
                            ? Colors.white
                            : Colors.black,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
