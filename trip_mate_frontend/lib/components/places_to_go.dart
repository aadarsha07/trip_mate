import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:trip_mate_frontend/components/shimmers/places_to_go_shimmer.dart';
import 'package:trip_mate_frontend/controllers/bloc/popular_places/popular_places_bloc.dart';
import 'package:trip_mate_frontend/screens/global/destinations/add_destination.dart';
import 'package:trip_mate_frontend/utils/constants.dart';

class NearbyPlaces extends StatefulWidget {
  const NearbyPlaces({Key? key}) : super(key: key);

  @override
  State<NearbyPlaces> createState() => _NearbyPlacesState();
}

class _NearbyPlacesState extends State<NearbyPlaces> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PopularPlacesBloc, PopularPlacesState>(
      builder: (context, state) {
        if (state is PopularPlacesInitial) {
          BlocProvider.of<PopularPlacesBloc>(context)
              .add(PopularPlacesInitialEvent());
        } else if (state is PopularPlacesLoaded) {
          return Column(
            children: List.generate(state.popularPlaces.length, (index) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: SizedBox(
                  height: 135,
                  width: double.maxFinite,
                  child: Card(
                    elevation: 0.4,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: InkWell(
                      borderRadius: BorderRadius.circular(12),
                      onTap: () {
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //       builder: (context) => DestinationPage(
                        //         image: nearbyPlaces[index].image,
                        //         title: nearbyPlaces[index].title,
                        //       ),
                        //     ));
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(12),
                              child: FadeInImage.assetNetwork(
                                placeholder: 'assets/images/noimage.jpg',
                                image:
                                    '$baseURL/${state.popularPlaces[index]['image']}',
                                height: double.maxFinite,
                                width: 130,
                                fit: BoxFit.cover,
                              ),
                            ),
                            const SizedBox(width: 10),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    state.popularPlaces[index]['name'],
                                    overflow: TextOverflow.ellipsis,
                                    style: const TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Text(state.popularPlaces[index]['subTitle'],
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        fontSize: 12,
                                        color: Colors.grey,
                                      )),
                                  const Spacer(),
                                  Row(
                                    children: [
                                      const Spacer(),
                                      IconButton(
                                          onPressed: () {
                                            Navigator.of(context).push(
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        AddDestination(
                                                          name: state
                                                                  .popularPlaces[
                                                              index]['name'],
                                                        )));
                                          },
                                          icon: const Icon(
                                            Icons.add,
                                            size: 40,
                                            color: Color(0xff8f294f),
                                          ))
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              );
            }),
          );
        }
        return const Column(
          children: [
            PlacesShimmer(),
            PlacesShimmer(),
            PlacesShimmer(),
          ],
        );
      },
    );
  }
}
