import 'package:flutter/material.dart';
import 'package:trip_mate_frontend/utils/size_config.dart';

class MessageBubble extends StatelessWidget {
  final String text;
  final bool isMe;

  const MessageBubble({
    super.key,
    required this.text,
    required this.isMe,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 8),
      child: Column(
        crossAxisAlignment:
            isMe ? CrossAxisAlignment.start : CrossAxisAlignment.end,
        children: [
          Material(
            borderRadius: BorderRadius.only(
              topLeft:
                  isMe ? const Radius.circular(0) : const Radius.circular(16),
              topRight:
                  isMe ? const Radius.circular(16) : const Radius.circular(0),
              bottomLeft: const Radius.circular(16),
              bottomRight: const Radius.circular(16),
            ),
            elevation: 2,
            color:
                isMe ? const Color.fromARGB(255, 210, 208, 208) : Colors.blue,
            child: Container(
              width: SizeConfig(context).deviceWidth() / 2.5,
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: Text(
                text,
                style: TextStyle(
                    color: isMe ? Colors.black : Colors.white, fontSize: 20),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
