import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:trip_mate_frontend/components/date_format.dart';
import 'package:trip_mate_frontend/controllers/bloc/destination/destinations_bloc.dart';
import 'package:trip_mate_frontend/models/destinations.dart';
import 'package:trip_mate_frontend/screens/global/destinations/edit_destinations.dart';
import 'package:trip_mate_frontend/screens/global/profile/traveler_profile_page.dart';
import 'package:trip_mate_frontend/utils/constants.dart';
import 'package:trip_mate_frontend/utils/size_config.dart';

import '../utils/functions.dart';

// ignore: must_be_immutable
class MyTripsDetail extends StatefulWidget {
  Destination destination;

  MyTripsDetail({super.key, required this.destination});
  @override
  State<MyTripsDetail> createState() => _MyTripsDetailState();
}

class _MyTripsDetailState extends State<MyTripsDetail> {
  String calculateAge(String? userDateOfBirth) {
    if (userDateOfBirth == null) {
      return 'N/A';
    }

    DateTime today = DateTime.now();
    DateTime dob = DateFormat("yyyy-MM-dd").parse(userDateOfBirth);

    int age = today.year - dob.year;
    if (today.month < dob.month ||
        (today.month == dob.month && today.day < dob.day)) {
      age--;
    }

    return age.toString();
  }

  @override
  void initState() {
    super.initState();
    token = getToken();
    userId = getId();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Container(
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.location_pin,
                    color: Colors.orange,
                  ),
                  Text(
                    widget.destination.name!,
                    style: TextStyle(
                        fontSize: SizeConfig(context).titleSize(),
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.calendar_month,
                    color: Colors.grey,
                    size: 32,
                  ),
                  Text(
                    ' ${getFormattedDateRange(widget.destination.startDate!, widget.destination.endDate!)}',
                    style: const TextStyle(
                        fontWeight: FontWeight.w400, fontSize: 17),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: TabBar(
                  tabs: [
                    Tab(
                      child: Text(
                        "Trip Details",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: SizeConfig(context).nameSize(),
                          fontWeight: FontWeight.w400,
                          fontFamily: 'Poppins',
                        ),
                      ),
                    ),
                    Tab(
                      child: Text(
                        "Members",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: SizeConfig(context).nameSize(),
                            fontWeight: FontWeight.w400,
                            fontFamily: 'Poppins'),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Container(
                  height: 300,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    gradient: const LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color(0xFFD9D9D9),
                        Color(0x00D9D9D9),
                        Color(0x00D9D9D9),
                        Color(0x00D9D9D9),
                        Color(0x00D9D9D9),
                        Color(0xFFD9D9D9),
                      ],
                      stops: [0, 0.3229, 0.5937, 0.7812, 0.8385, 1],
                    ),
                  ),
                  child: TabBarView(children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20.0, top: 5, right: 5),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          widget.destination.addedBy!.userName!,
                                          style: TextStyle(
                                              fontSize: SizeConfig(context)
                                                  .nameSize(),
                                              fontWeight: FontWeight.w700),
                                        ),
                                        widget.destination.addedBy!.isVerified!
                                            ? const Icon(
                                                Icons.verified,
                                                color: Colors.blue,
                                                size: 14,
                                              )
                                            : const SizedBox()
                                      ],
                                    ),
                                  ],
                                ),
                                const Divider(
                                  height: 0,
                                  thickness: 2,
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'Description',
                                  style: TextStyle(
                                      fontSize: SizeConfig(context).nameSize(),
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  widget.destination.description!,
                                  style: TextStyle(
                                      fontSize:
                                          SizeConfig(context).largeTextSize(),
                                      fontWeight: FontWeight.w400),
                                ),
                                const Divider(
                                  height: 5,
                                  thickness: 2,
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  "I'm Looking For",
                                  style: TextStyle(
                                      fontSize: SizeConfig(context).nameSize(),
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  widget.destination.peopleLooking!,
                                  style: TextStyle(
                                      fontSize:
                                          SizeConfig(context).largeTextSize(),
                                      fontWeight: FontWeight.w400),
                                ),
                                const Divider(
                                  height: 5,
                                  thickness: 2,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    SingleChildScrollView(
                      child: Column(
                        children: [
                          for (int i = 0;
                              i < widget.destination.members!.length;
                              i++)
                            widget.destination.members![i].status == 'Pending'
                                ? const SizedBox()
                                : Column(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      TravelerProfilePage(
                                                        id: widget.destination
                                                            .members![i].userId,
                                                      )));
                                        },
                                        child: Row(
                                          children: [
                                            Container(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  widget.destination.members![i]
                                                              .memberProfileImage !=
                                                          null
                                                      ? ClipOval(
                                                          child: Image.network(
                                                            '$baseURL/${widget.destination.members![i].memberProfileImage}',
                                                            width: SizeConfig(
                                                                            context)
                                                                        .deviceHeight() <
                                                                    800
                                                                ? 60
                                                                : 90,
                                                            height: SizeConfig(
                                                                            context)
                                                                        .deviceHeight() <
                                                                    800
                                                                ? 60
                                                                : 90,
                                                            fit: BoxFit.cover,
                                                          ),
                                                        )
                                                      : ClipOval(
                                                          child: Image.asset(
                                                            'assets/images/noimage.jpg',
                                                            width: SizeConfig(
                                                                            context)
                                                                        .deviceHeight() <
                                                                    800
                                                                ? 60
                                                                : 90,
                                                            height: SizeConfig(
                                                                            context)
                                                                        .deviceHeight() <
                                                                    800
                                                                ? 60
                                                                : 90,
                                                            fit: BoxFit.cover,
                                                          ),
                                                        ),
                                                  const SizedBox(width: 20),
                                                  Text(
                                                    '${widget.destination.members![i].userName},  ${calculateAge(widget.destination.members![i].memberDateOfBirth.toString())}',
                                                    style: TextStyle(
                                                        fontSize: SizeConfig(
                                                                context)
                                                            .largeTextSize(),
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  const SizedBox(width: 10),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      const Divider(
                                        height: 5,
                                        thickness: 2,
                                      )
                                    ],
                                  )
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton(
                        onPressed: () async {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => EditDestinations(
                                  destination: widget.destination)));
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color(0xFFD9D9D9),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 30, vertical: 10),
                        ),
                        child: Text(
                          'Edit',
                          style: TextStyle(
                            fontSize: SizeConfig(context).nameSize() - 1,
                            fontWeight: FontWeight.w700,
                            color: const Color(0xFF7D5EFF),
                          ),
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () async {
                          BlocProvider.of<DestinationsBloc>(context).add(
                            DeleteDestinationEvent(
                              destinationId: widget.destination.id!,
                            ),
                          );
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: const Color(0xFFD9D9D9),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 30, vertical: 10),
                        ),
                        child: Text(
                          'Delete',
                          style: TextStyle(
                            fontSize: SizeConfig(context).nameSize() - 1,
                            fontWeight: FontWeight.w700,
                            color: const Color(0xFF7D5EFF),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
