import 'package:intl/intl.dart';

String formatDate(DateTime date) {
  final day = DateFormat('d').format(date);
  final month = DateFormat('MMM').format(date);
  // ignore: unused_local_variable
  final year = DateFormat('y').format(date);

  final daySuffix = _getDaySuffix(int.parse(day));

  return '$day$daySuffix $month';
}

String getFormattedDateRange(DateTime startDate, DateTime endDate) {
  final formattedStartDate = formatDate(startDate);
  final formattedEndDate = formatDate(endDate);

  final year = DateFormat('y').format(endDate);

  return '$formattedStartDate - $formattedEndDate, $year';
}

String _getDaySuffix(int day) {
  if (day >= 11 && day <= 13) {
    return 'th';
  }

  switch (day % 10) {
    case 1:
      return 'st';
    case 2:
      return 'nd';
    case 3:
      return 'rd';
    default:
      return 'th';
  }
}
