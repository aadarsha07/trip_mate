import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:trip_mate_frontend/components/recommendation_detail.dart';
import 'package:trip_mate_frontend/components/shimmers/recommendation_shimmer.dart';
import 'package:trip_mate_frontend/controllers/bloc/recommendation/recommendation_bloc.dart';

import '../Utils/constants.dart';
import '../models/recommendationModel.dart';

class RecommendedPlaces extends StatefulWidget {
  String? id;
  RecommendedPlaces({Key? key, this.id}) : super(key: key);

  @override
  State<RecommendedPlaces> createState() => _RecommendedPlacesState();
}

class _RecommendedPlacesState extends State<RecommendedPlaces> {
  Future<void> _showDestinationDetails(List<Destination> destination) async {
    await showModalBottomSheet(
      useSafeArea: true,
      context: context,
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return SingleChildScrollView(
            child: RecommendationDetails(destination: destination[0]));
      },
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecommendationBloc, RecommendationState>(
      builder: (context, state) {
        if (state is RecommendationInitial) {
          BlocProvider.of<RecommendationBloc>(context)
              .add(RecommendationInitialEvent());
        }
        if (state is RecommendationLoading) {
          return const RecommendShimmer();
        } else if (state is RecommendationLoaded) {
          final RecommendationModel recommendations = state.recommendations;
          final List<RecommendedUsersWithDestinations> recommendationPlace =
              recommendations.recommendedUsersWithDestinations!;
          for (var element in recommendationPlace) {
            element.destinations!.removeWhere(
                (item) => item.startDate!.isBefore(DateTime.now()));
          }

// Remove empty or null destinations
          recommendationPlace.removeWhere((item) =>
              item.destinations == null || item.destinations!.isEmpty);
          //remove where destination addedby id == userId
          recommendationPlace.removeWhere(
              (item) => item.destinations![0].addedBy!.userId == widget.id);

          //remove where destination addedby id == userId

          print('place');

          return SizedBox(
            height: 235,
            child: ListView.separated(
                physics: const BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return SizedBox(
                    width: 200,
                    child: Card(
                      elevation: 0.4,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(12),
                        ),
                      ),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(12),
                        onTap: () {
                          _showDestinationDetails(
                              recommendationPlace[index].destinations!);
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                          child: Column(
                            children: [
                              Expanded(
                                flex: 3,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(12),
                                  child: FadeInImage.assetNetwork(
                                    placeholder: 'assets/images/noimage.jpg',
                                    image:
                                        '$baseURL/${recommendationPlace[index].destinations![0].addedBy!.userProfileImage}',
                                    // '${widget.recommendationPlace![index].destinations![0].addedBy!.userProfileImage}',
                                    width: double.maxFinite,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(8, 8, 8, 0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            recommendationPlace[index]
                                                .destinations![0]
                                                .addedBy!
                                                .userName!,
                                            style: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          recommendationPlace[index]
                                                  .destinations![0]
                                                  .addedBy!
                                                  .isVerified!
                                              ? Icon(
                                                  Icons.verified,
                                                  color: Colors.blue,
                                                  size: 14,
                                                )
                                              : const SizedBox(),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          const Icon(
                                            Icons.location_on,
                                            size: 15,
                                            color: Colors.orange,
                                          ),
                                          Flexible(
                                            child: Text(
                                              recommendationPlace[index]
                                                  .destinations![0]
                                                  .name!,
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                              style: const TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.grey),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          const Icon(
                                            Icons.calendar_today,
                                            size: 15,
                                          ),
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          Text(
                                            DateFormat.d().format(
                                                recommendationPlace[index]
                                                    .destinations![0]
                                                    .startDate!),
                                            style:
                                                const TextStyle(fontSize: 11),
                                          ),
                                          Text(
                                            DateFormat.MMM().format(
                                                recommendationPlace[index]
                                                    .destinations![0]
                                                    .startDate!),
                                            style:
                                                const TextStyle(fontSize: 11),
                                          ),
                                          const Text('-'),
                                          Text(
                                            DateFormat.d().format(
                                                recommendationPlace[index]
                                                    .destinations![0]
                                                    .endDate!),
                                            style:
                                                const TextStyle(fontSize: 11),
                                          ),
                                          Text(
                                            DateFormat.MMM().format(
                                                recommendationPlace[index]
                                                    .destinations![0]
                                                    .endDate!),
                                            style:
                                                const TextStyle(fontSize: 11),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
                separatorBuilder: (context, index) => const Padding(
                      padding: EdgeInsets.only(right: 10),
                    ),
                itemCount: recommendationPlace.length),
          );
        }
        return const RecommendShimmer();
      },
    );
  }
}
