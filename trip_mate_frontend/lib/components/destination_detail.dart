import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';
import 'package:trip_mate_frontend/components/date_format.dart';
import 'package:trip_mate_frontend/controllers/repositories/join_destination.dart';
import 'package:trip_mate_frontend/models/destinations.dart';
import 'package:trip_mate_frontend/screens/global/profile/traveler_profile_page.dart';
import 'package:trip_mate_frontend/utils/constants.dart';
import 'package:trip_mate_frontend/utils/size_config.dart';

// ignore: must_be_immutable
class DestinationDetails extends StatefulWidget {
  Destination destination;
  DestinationDetails({super.key, required this.destination});
  @override
  State<DestinationDetails> createState() => _DestinationDetailsState();
}

class _DestinationDetailsState extends State<DestinationDetails> {
  String calculateAge(String? userDateOfBirth) {
    if (userDateOfBirth == null) {
      return 'N/A';
    }

    DateTime today = DateTime.now();
    DateTime dob = DateFormat("yyyy-MM-dd").parse(userDateOfBirth);

    int age = today.year - dob.year;
    if (today.month < dob.month ||
        (today.month == dob.month && today.day < dob.day)) {
      age--;
    }

    return age.toString();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Container(
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              // SizedBox(
              //   height: SizeConfig(context).containerHeight(),
              //   width: double.maxFinite,
              //   child: Stack(
              //     fit: StackFit.expand,
              //     children: [
              //       Container(
              //           decoration: BoxDecoration(
              //             borderRadius: const BorderRadius.vertical(
              //               bottom: Radius.circular(20),
              //             ),
              //             boxShadow: [
              //               BoxShadow(
              //                 color: Colors.black.withOpacity(0.4),
              //                 spreadRadius: 0,
              //                 blurRadius: 20,
              //                 offset: const Offset(0, 10),
              //               ),
              //             ],
              //           ),
              //           child: widget.destination.placeImage == null
              //               ? Image.asset(
              //                   'assets/images/noimage.jpg',
              //                   fit: BoxFit.fitWidth,
              //                 )
              //               : Image.network(
              //                   '$baseURL/${widget.destination.placeImage}')),
              //       Positioned(
              //         top: 10,
              //         left: 10,
              //         child: Container(
              //           height: 50,
              //           width: 50,
              //           decoration: const BoxDecoration(
              //               shape: BoxShape.circle, color: Colors.white),
              //           child: Center(
              //             child: IconButton(
              //                 onPressed: () {
              //                   Navigator.pop(context);
              //                 },
              //                 icon: const Icon(
              //                   Icons.clear,
              //                   color: Colors.black,
              //                   size: 30,
              //                 )),
              //           ),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.location_on,
                    color: Colors.orange,
                    size: 22,
                  ),
                  Text(
                    widget.destination.name!,
                    style: TextStyle(
                        fontSize: SizeConfig(context).titleSize(),
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  const Icon(
                    Icons.calendar_month,
                    color: Colors.grey,
                    size: 32,
                  ),
                  Text(
                    ' ${getFormattedDateRange(widget.destination.startDate!, widget.destination.endDate!)}',
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: const TextStyle(
                        fontWeight: FontWeight.w400, fontSize: 16),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: TabBar(
                  tabs: [
                    Tab(
                      child: Text(
                        "Trip Details",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: SizeConfig(context).nameSize(),
                          fontWeight: FontWeight.w400,
                          fontFamily: 'Poppins',
                        ),
                      ),
                    ),
                    Tab(
                      child: Text(
                        "Members",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: SizeConfig(context).nameSize(),
                            fontWeight: FontWeight.w400,
                            fontFamily: 'Poppins'),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Container(
                  height: 300,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    gradient: const LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color(0xFFD9D9D9),
                        Color(0x00D9D9D9),
                        Color(0x00D9D9D9),
                        Color(0x00D9D9D9),
                        Color(0x00D9D9D9),
                        Color(0xFFD9D9D9),
                      ],
                      stops: [0, 0.3229, 0.5937, 0.7812, 0.8385, 1],
                    ),
                  ),
                  child: TabBarView(children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20.0, top: 5, right: 5),
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          widget.destination.addedBy!.userName!,
                                          style: TextStyle(
                                              fontSize: SizeConfig(context)
                                                  .nameSize(),
                                              fontWeight: FontWeight.w700),
                                        ),
                                        widget.destination.addedBy!.isVerified!
                                            ? const Icon(
                                                Icons.verified,
                                                color: Colors.blue,
                                                size: 14,
                                              )
                                            : const SizedBox(),
                                      ],
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Get.to(() => TravelerProfilePage(
                                            id: widget
                                                .destination.addedBy!.userId!));
                                      },
                                      child: Text('View Profile',
                                          style: TextStyle(
                                              fontSize: SizeConfig(context)
                                                      .nameSize() -
                                                  1,
                                              fontWeight: FontWeight.w700,
                                              color: Colors.green)),
                                    ),
                                  ],
                                ),
                                const Divider(
                                  height: 0,
                                  thickness: 2,
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'Description',
                                  style: TextStyle(
                                      fontSize: SizeConfig(context).nameSize(),
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  widget.destination.description!,
                                  style: TextStyle(
                                      fontSize:
                                          SizeConfig(context).largeTextSize(),
                                      fontWeight: FontWeight.w400),
                                ),
                                const Divider(
                                  height: 5,
                                  thickness: 2,
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  "I'm Looking For",
                                  style: TextStyle(
                                      fontSize: SizeConfig(context).nameSize(),
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  widget.destination.peopleLooking!,
                                  style: TextStyle(
                                      fontSize:
                                          SizeConfig(context).largeTextSize(),
                                      fontWeight: FontWeight.w400),
                                ),
                                const Divider(
                                  height: 5,
                                  thickness: 2,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    SingleChildScrollView(
                      child: Column(
                        children: [
                          for (int i = 0;
                              i < widget.destination.members!.length;
                              i++)
                            widget.destination.members![i].status == 'Pending'
                                ? SizedBox()
                                : Column(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      TravelerProfilePage(
                                                        id: widget.destination
                                                            .members![i].userId,
                                                      )));
                                        },
                                        child: Row(
                                          children: [
                                            Container(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  ClipOval(
                                                    child: Image.network(
                                                      '$baseURL/${widget.destination.members![i].memberProfileImage}',
                                                      width: SizeConfig(context)
                                                                  .deviceHeight() <
                                                              800
                                                          ? 60
                                                          : 90,
                                                      height: SizeConfig(
                                                                      context)
                                                                  .deviceHeight() <
                                                              800
                                                          ? 60
                                                          : 90,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                  const SizedBox(width: 20),
                                                  Text(
                                                    '${widget.destination.members![i].userName},  ${calculateAge(widget.destination.members![i].memberDateOfBirth.toString())}',
                                                    style: TextStyle(
                                                        fontSize: SizeConfig(
                                                                context)
                                                            .largeTextSize(),
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  const SizedBox(width: 10),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      const Divider(
                                        height: 5,
                                        thickness: 2,
                                      )
                                    ],
                                  )
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: Center(
                  child: ElevatedButton(
                    onPressed: () async {
                      await JoinDestinationRepository().joinDestination(
                          context, token, widget.destination.id!);
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color(0xFFD9D9D9),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 10),
                    ),
                    child: Text(
                      'Join Trip',
                      style: TextStyle(
                        fontSize: SizeConfig(context).nameSize() - 1,
                        fontWeight: FontWeight.w700,
                        color: const Color(0xFF7D5EFF),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
