import 'package:flutter/material.dart';

class Interest {
  final String name;
  final IconData icon;

  Interest({required this.name, required this.icon});
}
