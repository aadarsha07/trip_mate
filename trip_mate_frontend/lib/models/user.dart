class User {
  String? id;
  bool? isVerified;
  String? firstName;
  String? lastName;
  String? email;
  String? password;
  String? gender;
  DateTime? dateOfBirth;
  String? profileImage;
  String? role;
  String? height;
  String? weight;
  bool? isEmailVerified;
  String? qualification;
  String? city;
  String? state;
  String? maritalStatus;
  List<String>? interests;
  List<String>? languages;
  int? v;
  String? token;
  String? aboutMe;
  String? drinker;
  String? lookingFor;
  String? smoker;

  User({
    this.id,
    this.isVerified,
    this.firstName,
    this.lastName,
    this.email,
    this.password,
    this.gender,
    this.isEmailVerified,
    this.dateOfBirth,
    this.profileImage,
    this.role,
    this.height,
    this.weight,
    this.qualification,
    this.city,
    this.state,
    this.maritalStatus,
    this.interests,
    this.languages,
    this.v,
    this.token,
    this.aboutMe,
    this.drinker,
    this.lookingFor,
    this.smoker,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['_id'],
      isVerified: json['isVerified'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      email: json['email'],
      password: json['password'],
      gender: json['gender'],
      isEmailVerified: json['isEmailVerified'],
      dateOfBirth: json['dateOfBirth'] != null
          ? DateTime.parse(json['dateOfBirth'])
          : null,
      profileImage: json['profileImage'],
      role: json['role'],
      height: json['height'],
      weight: json['weight'],
      qualification: json['qualification'],
      city: json['city'],
      state: json['state'],
      maritalStatus: json['maritalStatus'],
      interests: json['interests'] != null
          ? List<String>.from(json['interests'])
          : null,
      languages: json['languages'] != null
          ? List<String>.from(json['languages'])
          : null,
      v: json['__v'],
      token: json['token'],
      aboutMe: json['aboutMe'],
      drinker: json['drinker'],
      lookingFor: json['lookingFor'],
      smoker: json['smoker'],
    );
  }
  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'isVerified': isVerified,
      'firstName': firstName,
      'lastName': lastName,
      'email': email,
      'password': password,
      'gender': gender,
      'isEmailVerified': isEmailVerified,
      'dateOfBirth': dateOfBirth?.toIso8601String(),
      'profileImage': profileImage,
      'role': role,
      'height': height,
      'weight': weight,
      'qualification': qualification,
      'city': city,
      'state': state,
      'maritalStatus': maritalStatus,
      'interests': interests,
      'languages': languages,
      '__v': v,
      'token': token,
      'aboutMe': aboutMe,
      'drinker': drinker,
      'lookingFor': lookingFor,
      'smoker': smoker,
    };
  }
}
