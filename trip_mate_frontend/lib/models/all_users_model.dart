class AllUsers {
  String? sId;
  bool? isVerified;
  String? firstName;
  String? lastName;
  String? email;
  String? password;
  String? gender;
  String? dateOfBirth;
  String? profileImage;
  String? role;
  String? height;
  String? weight;
  String? qualification;
  String? city;
  String? state;
  String? maritalStatus;
  List<String>? interests;
  List<String>? languages;
  int? iV;
  String? token;
  String? aboutMe;
  String? drinker;
  String? lookingFor;
  String? smoker;
  bool? isEmailVerified;
  String? otp;

  AllUsers(
      {this.sId,
      this.isVerified,
      this.firstName,
      this.lastName,
      this.email,
      this.password,
      this.gender,
      this.dateOfBirth,
      this.profileImage,
      this.role,
      this.height,
      this.weight,
      this.qualification,
      this.city,
      this.state,
      this.maritalStatus,
      this.interests,
      this.languages,
      this.iV,
      this.token,
      this.aboutMe,
      this.drinker,
      this.lookingFor,
      this.smoker,
      this.isEmailVerified,
      this.otp});

  AllUsers.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    isVerified = json['isVerified'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    email = json['email'];
    password = json['password'];
    gender = json['gender'];
    dateOfBirth = json['dateOfBirth'];
    profileImage = json['profileImage'];
    role = json['role'];
    height = json['height'];
    weight = json['weight'];
    qualification = json['qualification'];
    city = json['city'];
    state = json['state'];
    maritalStatus = json['maritalStatus'];
    interests = json['interests'].cast<String>();
    languages = json['languages'].cast<String>();
    iV = json['__v'];
    token = json['token'];
    aboutMe = json['aboutMe'];
    drinker = json['drinker'];
    lookingFor = json['lookingFor'];
    smoker = json['smoker'];
    isEmailVerified = json['isEmailVerified'];
    otp = json['otp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['_id'] = sId;
    data['isVerified'] = isVerified;
    data['firstName'] = firstName;
    data['lastName'] = lastName;
    data['email'] = email;
    data['password'] = password;
    data['gender'] = gender;
    data['dateOfBirth'] = dateOfBirth;
    data['profileImage'] = profileImage;
    data['role'] = role;
    data['height'] = height;
    data['weight'] = weight;
    data['qualification'] = qualification;
    data['city'] = city;
    data['state'] = state;
    data['maritalStatus'] = maritalStatus;
    data['interests'] = interests;
    data['languages'] = languages;
    data['__v'] = iV;
    data['token'] = token;
    data['aboutMe'] = aboutMe;
    data['drinker'] = drinker;
    data['lookingFor'] = lookingFor;
    data['smoker'] = smoker;
    data['isEmailVerified'] = isEmailVerified;
    data['otp'] = otp;
    return data;
  }
}
