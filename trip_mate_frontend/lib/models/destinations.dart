class DestinationModel {
  String? status;
  List<Destination>? destinations;

  DestinationModel({
    this.status,
    this.destinations,
  });

  factory DestinationModel.fromJson(Map<String, dynamic> json) {
    List<dynamic> destinationsJson = json['destinations'];
    List<Destination> destinations =
        destinationsJson.map((e) => Destination.fromJson(e)).toList();

    return DestinationModel(
      status: json['status'],
      destinations: destinations,
    );
  }
}

class Destination {
  AddedBy? addedBy;
  String? id;
  String? name;
  String? peopleLooking;
  String? description;
  DateTime? startDate;
  DateTime? endDate;
  List<Member>? members;
  dynamic placeImage;
  int? v;

  Destination({
    this.addedBy,
    this.id,
    this.name,
    this.peopleLooking,
    this.description,
    this.startDate,
    this.endDate,
    this.members,
    this.placeImage,
    this.v,
  });

  factory Destination.fromJson(Map<String, dynamic> json) {
    return Destination(
      addedBy: AddedBy.fromJson(json['addedBy']),
      id: json['_id'],
      name: json['name'],
      peopleLooking: json['peopleLooking'],
      description: json['description'],
      placeImage: json['placeImage'],
      startDate: DateTime.parse(json['startDate']),
      endDate: DateTime.parse(json['endDate']),
      members: List<Member>.from(
          json['members'].map((member) => Member.fromJson(member))),
      v: json['__v'],
    );
  }
}

class AddedBy {
  String? userId;
  String? userName;
  String? userProfileImage;
  bool? isVerified;

  AddedBy({
    this.userId,
    this.userName,
    this.userProfileImage,
    this.isVerified,
  });

  factory AddedBy.fromJson(Map<String, dynamic> json) {
    return AddedBy(
      userId: json['userId'],
      userName: json['userName'],
      userProfileImage: json['userProfileImage'],
      isVerified: json['isVerified'],
    );
  }
}

class Member {
  String userId;
  String userName;
  String status;
  DateTime? memberDateOfBirth;
  dynamic memberProfileImage;
  bool? isVerified;

  Member(
      {required this.userId,
      required this.userName,
      required this.status,
      this.memberDateOfBirth,
      this.isVerified,
      this.memberProfileImage});
  factory Member.fromJson(Map<String, dynamic> json) {
    return Member(
      userId: json['userId'],
      userName: json['userName'],
      status: json['status'],
      memberDateOfBirth: json['memberDateOfBirth'] != null
          ? DateTime.parse(json['memberDateOfBirth'])
          : null,
      memberProfileImage: json['memberProfileImage'],
      isVerified: json['isVerified'],
    );
  }
}
