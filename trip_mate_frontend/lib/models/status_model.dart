class StatusModel {
  String? status;
  String? message;
  String? token;
  String? id;
  String? role;
  String? email;
  bool? isEmailVerified;
  String? otp;

  StatusModel(
      {this.status,
      this.message,
      this.token,
      this.otp,
      this.id,
      this.role,
      this.email,
      this.isEmailVerified});

  factory StatusModel.fromJson(Map<String, dynamic> json) {
    return StatusModel(
      status: json['status'],
      message: json['message'],
      token: json['token'],
      id: json['userId'],
      role: json['role'],
      email: json['email'],
      otp: json['otp'],
      isEmailVerified: json['isEmailVerified'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    data['message'] = message;
    data['token'] = token;
    data['userId'] = id;
    data['role'] = role;
    data['email'] = email;
    data['otp'] = otp;
    data['isEmailVerified'] = isEmailVerified;
    return data;
  }
}
