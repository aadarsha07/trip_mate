class VerificationModel {
  final String? status;
  final String? message;
  final ApiData? data;

  VerificationModel({
    this.status,
    this.message,
    this.data,
  });

  factory VerificationModel.fromJson(Map<String, dynamic> json) {
    return VerificationModel(
      status: json['status'],
      message: json['message'],
      data: json['data'] != null ? ApiData.fromJson(json['data']) : null,
    );
  }
}

class ApiData {
  final String? userId;
  final String? email;

  ApiData({
    this.userId,
    this.email,
  });

  factory ApiData.fromJson(Map<String, dynamic> json) {
    return ApiData(
      userId: json['userId'],
      email: json['email'],
    );
  }
}
